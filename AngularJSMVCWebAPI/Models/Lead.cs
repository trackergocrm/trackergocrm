﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularJSMVCWebAPI.Models
{
     
 public class Lead {

    public int Id { get; set; }  
    public string Description  { get; set; }  
    public string Title { get; set; }  
    public string Salesman  { get; set; }  
    public string CreateDate  { get; set; }     
    public string[] Uploads { get; set; }   
    public string Account { get; set; }  
    public string From  { get; set; }  
    public string To  { get; set; }  
    public int AccountId { get; set; }  
    public int FromId  { get; set; }  
    public Boolean Read { get; set; }   
    public string Subject  { get; set; }  
    public string Status { get; set; }   
    public string Saleman  { get; set; }  
    public string ContactPhone  { get; set; }  
    public string PONum { get; set; }   
    public decimal Amount { get; set; }   
    public int SalesStageId { get; set; }  
    public string SalesStage  { get; set; }  
    public string Topics  { get; set; }  
    public Boolean UnRead  { get; set; }  
    public int LeadSourceId { get; set; }   
    public string LeadSource  { get; set; }
    public int SalesManId { get; set; }   
    public string InvoiceNum  { get; set; }  
    public string InvoiceDate { get; set; }  
    public string LostDate { get; set; }   
    public decimal PotentialRevenue { get; set; }
    public int ReferredById { get; set; }
    public int Units { get; set; }
    public int UnitType { get; set; }
    public int TemplateId { get; set; }   
    public Boolean HotLead { get; set; }
    public Boolean InActive { get; set; }   

 

    }

}

 
