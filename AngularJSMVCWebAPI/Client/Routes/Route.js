﻿// script.js

// create the module and name it  
// also include ngRoute for all our routing needs
// var scotchApp = angular.module('scotchApp', ['ngRoute']);

// configure our routes
module.config(function ($routeProvider) {
    $routeProvider
        // route for the home page
        .when('/', {
            templateUrl: '../Views/main.htm',
            controller: 'MainController'
        })
        .when('/Main', {
            templateUrl: '../Views/main.htm',
            controller: 'LeadsController'
        })
        // route for the about page
        .when('/Leads', {
            templateUrl: '../Views/leads.htm',
            controller: 'LeadsController'
        })
        .otherwise
        ({
            redirectTo: 'www.microsoft.com'
        });

});

 