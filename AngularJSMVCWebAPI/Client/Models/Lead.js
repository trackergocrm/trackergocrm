﻿
// Id, Description, Title, Salesman, CreateDate, Uploads, Account, From, 
// To, AccountId, FromId, Read, Subject, Status, Saleman, Saleman, ContactPhone, PONum, Amount, SalesStageId,
// SalesStage, Topics, UnRead, LeadSourceId, LeadSource, SalesManId, 
// InvoiceNum, InvoiceDate, LostDate, PotentialRevenue, ReferredById, Units, UnitType, TemplateId, HotLead, InActive

var Lead = function ()
{
    this.Id = 0;
    this.Description = "";
    this.Title = "";
    this.Salesman = "";
    this.CreateDate = "";   
    this.Uploads = [];
    this.Account = "";
    this.From = "";
    this.To = "";
    this.AccountId = 0;
    this.FromId = 0;
    this.Read = false;
    this.Subject = "";
    this.Status = "";
    this.Saleman = "";
    this.ContactPhone = "";
    this.PONum = "";
    this.Amount = 0.00;
    this.SalesStageId = 0;
    this.SalesStage = "";
    this.Topics = "";
    this.UnRead = false;
    this.LeadSourceId = 0;
    this.LeadSource = "";
    this.SalesManId = 0;
    this.InvoiceNum = "";
    this.InvoiceDate = "";
    this.LostDate = "";
    this.PotentialRevenue = 0.00;
    this.ReferredById = 0;
    this.Units = 0;
    this.UnitType = 0;
    this.TemplateId = 0;
    this.HotLead = false;
    this.InActive = false;
};
 

