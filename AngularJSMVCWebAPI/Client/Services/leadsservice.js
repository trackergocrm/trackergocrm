﻿

module.service('LeadsService', function ($http,  $q) {
           
            //simply search contacts list for given id
            //and returns the contact object if found
            this.getall = function () {

                var deferred = $q.defer();

                $http.get("/api/Leads/")
                  .then(function (response) {
                      return deferred.resolve(response);
                  }),
                  function (response) {
                     alert(response.Message);
                  };
                   
                 return deferred.promise;

            }

            this.delete = function (id) {
              
               var deferred = $q.defer();

                var config = { headers: { 'Content-Type': 'application/json; charset=UTF-8', 'accept': 'application/json' } }

                $http.delete("/api/Leads/" + id, config)
                 .then(function (response) {
                     return deferred.resolve(response);
                 }),
                 function (response) {
                     alert(response.Message);
                 };

                 return deferred.promise;

           }

            this.insert = function (instance) {

                var jsonParse = JSON.parse(JSON.stringify(instance));

                var config = { headers: { 'Content-Type': 'application/json; charset=UTF-8', 'accept': 'application/json' } }

                var deferred = $q.defer();

                $http.post("/api/Leads/", jsonParse, config)
                 .then(function (response) {
                     return deferred.resolve(response);
                 }),
                 function (response) {
                     alert(response.Message);
                 };

                return deferred.promise;

            }

            this.update = function (instance) {
                 
                var jsonParse = JSON.parse(JSON.stringify(instance));

                var config = { headers: { 'Content-Type': 'application/json; charset=UTF-8', 'accept': 'application/json' } }

                var deferred = $q.defer();

                $http.put("/api/Leads/", jsonParse, config)
                 .then(function (response) {
                     return deferred.resolve(response);
                 }),
                 function (response)
                 {
                     alert(response.Message);
                 };

                return deferred.promise;

           }
           
        });