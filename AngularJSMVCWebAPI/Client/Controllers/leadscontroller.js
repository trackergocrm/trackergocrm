﻿
// Controller Definition: LeadsController
/// <reference path="../Models/Lead.js" />

module.controller('LeadsController', function (LeadsService) {
           
    var vm = this;
    var service = LeadsService;
    //var temp=new PricingData[]{new PricingData(1,2,3,4,5),new PricingData(1,2,3,4,5)};

    vm.Leads = [];

    vm.editlead;
    vm.message = "";

    vm.ShowLeadDetail = false;
    vm.ShowLeadGrid = true;

    vm.showleaddetail = function (lead)
    {
        vm.ShowLeadDetail = true;
        vm.ShowLeadGrid = false;

        txtId.value = lead.Id;
        txtDescription.value = lead.Description;
        txtTitle.value = lead.Title;
        txtSalesman.value = lead.Salesman;
        txtCreateDate.value = lead.CreateDate;
        //txtUploads.value = lead.CreateDate
        txtAccount.value = lead.Account;
        txtFrom.value = lead.From;
        txtTo.value = lead.To;
        txtAccountId.value = lead.AccountId;
        txtFromId.value = lead.FromId;
        chkRead.checked = lead.Read;
        txtSubject.value = lead.Subject;
        txtStatus.value = lead.Status;
        txtSaleman.value = lead.Saleman;
        txtContactPhone.value = lead.ContactPhone;
        txtPONum.value = lead.PONum;
        txtAmount.value = lead.Amount;
        txtSalesStageId.value = lead.SalesStageId;
        txtSalesStage.value = lead.SalesStage;
        txtTopics.value = lead.Topics;
        chkUnRead.checked = lead.UnRead;
        txtLeadSourceId.value = lead.LeadSourceId;
        txtLeadSource.value = lead.LeadSource;
        txtSalesManId.value = lead.SalesManId;
        txtInvoiceNum.value = lead.InvoiceNum;
        txtInvoiceDate.value = lead.InvoiceDate;
        txtLostDate.value = lead.LostDate;
        txtPotentialRevenue.value = lead.PotentialRevenue;
        txtReferredById.value = lead.ReferredById;
        txtUnits.value = lead.Units;
        txtUnitType.value = lead.UnitType;
        txtTemplateId.value = lead.TemplateId;
        chkHotLead.checked = lead.HotLead;
        chkInActive.checked = lead.InActive;

    }

    vm.hideleaddetail = function () {
        vm.ShowLeadDetail = false;
        vm.ShowLeadGrid = true;
    }

    vm.getall = function ()
    {

        vm.message = "";

        service.getall()
        .then(function (response)
        {
            vm.Leads = response.data;
        }),
        function (response) {
            alert(response.Message);
        };
    }

    vm.delete = function (id) 
    {
        if (confirm("Delete this lead?"))
        {
            vm.message = "";

            service.delete(id)
            .then(function (response)
            {
                var len = vm.Leads.length;

                for (var i = 0; i < len; i++)
                {


                    if ((vm.Leads[i] != null) && (vm.Leads[i].Id == id))
                    {
                        vm.Leads.splice(i,1);
                    }
                }

                vm.message = "Record has been deleted successfully!";

            }),
            function (response)
            {
                alert(response.Message);
            };
        }
    }

    vm.insert = function (editlead)
    {
        vm.message = "";

        service.insert(editlead).
           then(function (response) 
           {                

               vm.Leads.push(editlead);
               vm.editlead = null;
               //alert("Record has been added successfully!");
               vm.message = "Record has been added successfully!";
                
           }),
           function (response) {
               alert(response.Message);
           };
    }
			
    vm.update = function (lead)
    {
        vm.message = "";

        service.update(lead).
            then(function (response)
            {
                
                //var index = vm.Leads.indexOf(lead);
                //_.findIndex(users, function (chr) {
                //    return chr.user == 'barney';
                //});

                var len = vm.Leads.length;

                for (var i = 0; i < len; i++)
                {
                    if (vm.Leads[i].Id == lead.Id)
                    {
                        vm.Leads[i] = lead;
                    }
                }

                //alert("Record has been updated successfully!");
                vm.message = "Record has been updated successfully!";
            }),
            function(response)
        {
            alert(response.Message);
        };
    }
	        
})