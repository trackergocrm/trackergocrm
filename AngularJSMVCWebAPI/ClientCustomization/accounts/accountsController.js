module.controller('AccountsController', function ($scope, $rootScope, Upload, $timeout, AccountsService, FlashService, CRUDService) {
    var vm = this;
    $scope.showiconadd = true;
    $scope.showiconsearch = true;
    $scope.activeiconadd = false;
    $scope.activeiconsearch = false;
    $scope.activeTab = 'accountsFeed';
    $scope.subActiveTab = 'details';
    vm.Account = null;
    vm.LastAccount;
    vm.showAddForm = false;
    vm.isEditForm = false;
    vm.currentaccountid = -1;
    vm.event_detail = true;
    vm.message = null;
    vm.showadd = true;
    vm.showedit = true;
    vm.showsave = false;
    vm.showdelete = true;
    vm.showcancel = false;
    vm.showback = false;
    vm.addNewEvent = false;
    vm.newActivity_Text = "";
    vm.newNotes_Text = "";
	vm.searchVisible = false;
    $rootScope.dashboardTitle = "Accounts Feed";
    $rootScope.dashboardMsg = "Overview of all the Accounts";
    vm.newEvent = {
        isRecurrence: false
    };
    $scope.availableSearchParams = [
        { key: "Subject", name: "Subject", placeholder: "Subject..." },
        { key: "Contact", name: "Contact", placeholder: "Contact..." },
        { key: "Phone", name: "Phone", placeholder: "Phone..." },
        { key: "emailAddress", name: "E-Mail", placeholder: "E-Mail..." },
        { key: "Date", name: "Date", placeholder: "Date...", type: "date" }
       //{ key: "SelectBox", name: "SelectBox", placeholder: "SelectBox...", type: "select", suggestedValues: ['test1', 'test2', 'test3'] }
    ];

    $scope.eventsSearchParams = [
        { key: "Search Param 1", name: "Search Param 1", placeholder: "Search Param 1..." },
        { key: "Search Param 2", name: "Search Param 2", placeholder: "Search Param 2..." }
    ];

    $scope.notesSearchParams = [
        { key: "Search Param 1", name: "Search Param 1", placeholder: "Search Param 1..." },
        { key: "Search Param 2", name: "Search Param 2", placeholder: "Search Param 2..." }
    ];
	
	$scope.startSearch = function ()
    {

        $scope.searchVisible = true;
    }

    $scope.hideSearch = function () {

        $scope.searchVisible = false;
    }

    vm.buttonvisible = function (showadd, showedit, showsave, showdelete, showcancel, showback) {

        vm.showadd = showadd;
        vm.showedit = showedit;
        vm.showsave = showsave;
        vm.showdelete = showdelete;
        vm.showcancel = showcancel;
        vm.showback = showback;
    };

    $scope.changeWithOutSubTabs = function (iconName) {

        $scope.activeiconadd = false;
        $scope.activeiconsearch = false;

        if (iconName === 'add') {
            $scope.activeiconadd = true;
        }

        if (iconName === 'search') {
            $scope.activeiconsearch = true;
        }

    };

    $scope.changeSubTabs = function (tabName) {
        $scope.subActiveTab = tabName;
        if (tabName === 'details') {
            $rootScope.dashboardTitle = "Accounts Details";
            //vm.buttonvisible(true, true, false, true, false, false);
            vm.buttonvisible(false, true, false, true, false, false);
        }
        else if (tabName === 'events') {
            vm.isEditForm = true;
            vm.buttonvisible(true, false, false, false, false, false);
        }
        else {
            vm.isEditForm = true;
            vm.buttonvisible(false, false, false, false, false, false);
           
        }

        if (tabName === 'posts') {
            $rootScope.dashboardTitle = "Activities Details";
        }
        if (tabName === 'events') {
            vm.showCalendarReadOnly = false;
            vm.showCalendarEdit = false;
            vm.event_detail = true;
            $rootScope.dashboardTitle = "Events Details";
        }
        if (tabName === 'notes') {
            $rootScope.dashboardTitle = "Notes Details";
        }
        if (tabName === 'uploads') {
            $rootScope.dashboardTitle = "Uploads Details";
        }
        if (tabName === 'replies') {
            $rootScope.dashboardTitle = "Replies Details";
        }
    };

    vm.getall = function () {
        AccountsService.getall()
        .then(function (response) {
            vm.Accounts = response;
            //console.log(response);
            // console.log(JSON.stringify(response));
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getall();

    vm.getActivities = function () {
        AccountsService.getActivityType()
        .then(function (response) {
            vm.ActivityType = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getActivities();

    vm.getAcountName = function () {
        AccountsService.getAcountName()
        .then(function (response) {
            vm.AcountName = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getAcountName();

    vm.getLeadContact = function () {
        AccountsService.getLeadContact()
        .then(function (response) {
            vm.LeadContact = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getLeadContact();

    vm.getLeadSource = function () {
        AccountsService.getLeadSource()
        .then(function (response) {
            vm.LeadSource = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getLeadSource();

    vm.getSalesStage = function () {
        AccountsService.getSalesStage()
        .then(function (response) {
            vm.SalesStage = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getSalesStage();

    vm.getSalesman = function () {
        AccountsService.getSalesman()
        .then(function (response) {
            vm.Salesman = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getSalesman();

    vm.getReferredBy = function () {
        AccountsService.getReferredBy()
        .then(function (response) {
            vm.ReferredBy = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getReferredBy();



    $scope.$watch('files', function () {
        // $scope.upload($scope.files);
    });
    $scope.$watch('file', function () {
        if ($scope.file != null) {
            $scope.files = [$scope.file];
        }
    });
    vm.deleteActivities = function (id) {
        for (var i = 0; i < vm.Account.Activities.length; i++) {
            if ((vm.Account.Activities != null) && (vm.Account.Activities[i].ActivityID == id)) {
                vm.Account.Activities.splice(i, 1);
                vm.save();
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
    };
    vm.deleteEvents = function (id) {
        for (var i = 0; i < vm.Account.Events.length; i++) {
            if ((vm.Account.Events != null) && (vm.Account.Events[i].EventsID == id)) {
                vm.Account.Events.splice(i, 1);
                vm.save();
                break;
            }
        }
        vm.showCalendarReadOnly = false;
        vm.showCalendarEdit = false;
        vm.event_detail = true;
        FlashService.Success("Deleted Successfully");
    };
    vm.deleteNotes = function (id) {
        for (var i = 0; i < vm.Account.Notes.length; i++) {
            if ((vm.Account.Notes != null) && (vm.Account.Notes[i].NotesID == id)) {
                vm.Account.Notes.splice(i, 1);
                vm.save();
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
    };
    vm.deleteUploads = function (id) {
        for (var i = 0; i < vm.Account.Uploads.length; i++) {
            if ((vm.Account.Uploads != null) && (vm.Account.Uploads[i].UploadsID == id)) {
                vm.Account.Uploads.splice(i, 1);
                vm.save();
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
    };
    vm.editEvents = function (id) {
        vm.addNewEvent = !vm.addNewEvent;
        for (var i = 0; i < vm.Account.Events.length; i++) {
            if ((vm.Account.Events != null) && (vm.Account.Events[i].EventsID == id)) {
                vm.newEvent = vm.Account.Events[i];
                break;
            }
        }
    };
    // upload on file select or drop
    vm.upload = function (file) {
        CRUDService.addUpload(vm.Account, "Accounts", file).then(function (response) {
            //console.log("response", response);
            vm.Accounts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.files = [];
            //console.log("vm.files", vm.files);
            FlashService.Success("Upload Saved Successfully!");
        });
        /* Upload.upload({
            url: 'upload/url',
            data: { file: file, 'username': $scope.username }
        }).then(function (resp) {
            // console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
        }, function (resp) {
            // console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            // console.log('progress: ' + progressPercentage + '% ');
        });*/
    };


    vm.email = function () {
        alert('email called');
    };
    vm.call = function () {
        alert('Call function called');
    };



    vm.cancel = function () {
        vm.showAddForm = false;
        if (vm.currentaccountid) {
            vm.filteraccounts(vm.currentaccountid);
            vm.buttonvisible(true, true, false, true, false, false);
        } else {
            $scope.activeTab = 'accountsFeed';
        }
        
    }

    vm.addNewActivity = function () {
       /* var newActivity = {};
        newActivity.ActivityID = vm.Account.Activities[vm.Account.Activities.length - 1].ActivityID + 1;
        newActivity.Name = 'Manish G';
        newActivity.Text = vm.newActivity_Text;
        newActivity.ProfilePic = 'img/profileimg.png';
        newActivity.EditActivities = false;
        vm.Account.Activities.push(newActivity);

        AccountsService.update(vm.Account).then(function (response) {
            vm.Accounts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newActivity_Text = "";
            FlashService.Success("Activity Saved Successfully!");
        });*/
        CRUDService.addNewActivity(vm.newActivity_Text, vm.Account, "Accounts").then(function (response) {
            //console.log("response", response);
            vm.Accounts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newActivity_Text = "";
            FlashService.Success("Activity Saved Successfully!");
        });
    };
    vm.addNewReply = function () {
        /*var newReplyAdd = {};
        newReplyAdd.RepliesID = vm.Account.Replies[vm.Account.Replies.length - 1].RepliesID + 1;
        newReplyAdd.Comment = vm.reply.Text
        newReplyAdd.ProfilePic = "img/profileimg.png";
        newReplyAdd.Recepients = vm.reply.Recepients;
        newReplyAdd.CreationDate = "2016-08-16";
        newReplyAdd.ExcludeProspect = vm.reply.excludeProspect;
        newReplyAdd.OtherReplies = [];
        vm.Account.Replies.push(newReplyAdd);

        AccountsService.update(vm.Account).then(function (response) {
            vm.Accounts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.reply = {};
            FlashService.Success("Replies Saved Successfully!");
        });*/
        CRUDService.addNewReply(vm.reply, vm.Account, "Accounts").then(function (response) {
            //console.log("response", response);
            vm.Accounts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.reply = {};
            FlashService.Success("Replies Saved Successfully!");
        });
    };
    vm.addsubreply = function (index) {
       /* var subReplyAdd = {};
        if (vm.Account.Replies[index].OtherReplies.length == undefined || vm.Account.Replies[index].OtherReplies.length == 0) {
            subReplyAdd.OtherRepliesID = 1;
        }
        else {
            subReplyAdd.OtherRepliesID = vm.Account.Replies[index].OtherReplies[vm.Account.Replies[index].OtherReplies.length - 1].OtherRepliesID + 1;
        }
        subReplyAdd.Comment = vm.subreply.Text
        subReplyAdd.ProfilePic = "img/profileimg.png";
        subReplyAdd.Recepients = vm.subreply.Recepients;
        subReplyAdd.CreationDate = "2016-08-16";
        subReplyAdd.ExcludeProspect = false;
        vm.Account.Replies[index].OtherReplies.push(subReplyAdd);
        

        AccountsService.update(vm.Account).then(function (response) {
            vm.Accounts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.subreply = {};
            vm.subreply.Recepients = '';
            FlashService.Success("Replies Saved Successfully!");
        });*/
        CRUDService.addsubreply(index, vm.subreply, vm.Account, "Accounts").then(function (response) {
            //console.log("response", response);
            vm.Accounts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.subreply = {};
            vm.subreply.Recepients = '';
            FlashService.Success("Replies Saved Successfully!");
        });
    };
    vm.addNewEvents = function () {
        /*var newEvents = vm.newEvent;
        if (vm.newEvent.EventsID == undefined) {
            newEvents.EventsID = vm.Account.Events[vm.Account.Events.length - 1].EventsID + 1;
        }

        newEvents.EditActivities = false;
        vm.Account.Events.push(newEvents);

        AccountsService.update(vm.Account).then(function (response) {
            vm.Accounts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newEvent = {};
            FlashService.Success("Events Saved Successfully!");
        });*/
        CRUDService.addNewEvents(vm.newEvent, vm.isEditEventForm, vm.Account, "Accounts").then(function (response) {
            //console.log("response", response);
            vm.Accounts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newEvent = {};
            FlashService.Success("Events Saved Successfully!");
        });
       /* vm.showCalendarEdit = false;
        vm.event_detail = true;
        vm.isEditEventForm == false;
        vm.buttonvisible(true, false, false, false, false, false);*/
        if (vm.isEditEventForm == true) {
            vm.showCalendarReadOnly = true;
            vm.event_detail = false;
            vm.isEditEventForm = false;
            vm.buttonvisible(true, false, true, true, false, false);
        } else {
            vm.event_detail = true;
            vm.buttonvisible(true, false, false, false, false, false);
        }

        vm.showCalendarEdit = false;
    };
    vm.addNewNotes = function () {
       /* var newNotes = {};
        newNotes.NotesID = vm.Account.Notes[vm.Account.Notes.length - 1].NotesID + 1;
        newNotes.Name = 'Manish G';
        newNotes.Text = vm.newNotes_Text;
        newNotes.ProfilePic = 'img/profileimg.png';
        newNotes.EditActivities = false;
        vm.Account.Notes.push(newNotes);

        AccountsService.update(vm.Account).then(function (response) {
            vm.Accounts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newNotes_Text = "";
            FlashService.Success("Notes Saved Successfully!");
        });*/
        CRUDService.addNewNotes(vm.newNotes_Text, vm.Account, "Accounts").then(function (response) {
            //console.log("response", response);
            vm.Accounts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newNotes_Text = "";
            FlashService.Success("Notes Saved Successfully!");
        });
    };
    vm.save = function () {
       /*vm.showAddForm = false;
        
        if (vm.isEditForm == true) {
            AccountsService.update(vm.Account).then(function (response) {
                vm.Accounts = response;
                if ($scope.subActiveTab == 'details') {
                    vm.buttonvisible(false, true, false, true, false, false);
                }
                FlashService.Success("Saved Successfully!");
            });
        }
        else {
            AccountsService.insert(vm.Account).then(function (response) {
                vm.Accounts = response;
                if ($scope.subActiveTab == 'details') {
                    vm.buttonvisible(false, true, false, true, false, false);
                    $scope.activeTab = 'accountsFeed';
                }
                FlashService.Success("Saved Successfully!");
            });
        }*/
        vm.showAddForm = false;

        if (vm.isEditForm == true) {
            vm.Account.Company = vm.Account.CompanyId;
            CRUDService.update(vm.Account, "Accounts").then(function (response) {
                vm.Accounts = response;
                if ($scope.subActiveTab == 'details') {
                    //vm.buttonvisible(false, true, false, true, false, false);
                    vm.buttonvisible(true, true, false, true, false, false);
                }
                FlashService.Success("Saved Successfully!");
            });
        }
        else {
            vm.Account.Id = vm.Accounts.length + 1;
            vm.Account.Company = vm.Account.CompanyId;
            vm.Account.Replies = [];
            vm.Account.Uploads = [];
            vm.Account.Notes = [];
            vm.Account.Events = [];
            vm.Account.Activities = [];
            CRUDService.insert(vm.Account, "Accounts").then(function (response) {
                vm.Accounts = response;
                if ($scope.subActiveTab == 'details') {
                    //vm.buttonvisible(false, true, false, true, false, false);
                    vm.buttonvisible(true, true, false, true, false, false);
                    $scope.activeTab = 'accountsFeed';
                }
                FlashService.Success("Saved Successfully!");
            });
        }
    }

    vm.add = function () {
            vm.LastAccount = vm.Account;
            vm.Account = {};
            vm.showAddForm = true;
        //vm.buttonvisible(false, true, true, false, true, false);
            vm.buttonvisible(false, false, true, false, true, false);
    }
    vm.addevent = function () {
        //console.log(" vm.Account", vm.Account);
        vm.newEvent = {
            isRecurrence: false
        };
        vm.showCalendarEdit = true;
        vm.event_detail = false;
        vm.buttonvisible(false, false, true, true, false, false);
    }
    vm.delete = function () {
        /*if (confirm("Delete this account?")) {

            AccountsService.delete(vm.currentaccountid).then(function (response) {
                vm.Accounts = response;
                if ($scope.subActiveTab == 'details') {
                    vm.buttonvisible(false, true, false, true, false, false);
                }
            });
            FlashService.Success("Deleted Successfully!");
        }
        $scope.activeTab = 'accountsFeed';
        vm.buttonvisible(false, true, false, true, false, false);*/
        if (confirm("Delete this account?")) {

            CRUDService.delete(vm.currentaccountid, "Accounts").then(function (response) {
                vm.Accounts = response;
                if ($scope.subActiveTab == 'details') {
                    vm.buttonvisible(false, true, false, true, false, false);
                }
            });
            vm.Accounts = [];
            vm.Accounts = CRUDService.getStorageAccounts();
            FlashService.Success("Deleted Successfully!");
        }
        //$scope.activeTab = 'accountsFeed';
        $scope.activeTab = 'accountsDetail';
        vm.buttonvisible(false, true, false, true, false, false);
    };

    vm.edit = function () {
        vm.showAddForm = true;
        vm.isEditForm = true;
        vm.filteraccounts(vm.currentaccountid);
        vm.buttonvisible(false, false, true, false, true, false);
    }
    vm.editevent = function () {
        vm.showCalendarReadOnly = false;
        vm.showCalendarEdit = true;
        vm.isEditEventForm = true;
        vm.filterevents(vm.currenteventid);
        //console.log("vm.newEvent", vm.newEvent);
        vm.buttonvisible(false, false, true, true, false, false);
    }
    vm.filteraccounts = function (id) {
        vm.Account = vm.Accounts.filter(function (v) {
            return v.Id === id; // Filter out the appropriate one
        })[0];
        
        //console.log(vm.Account);
        if (vm.Account && vm.Account.hasOwnProperty('CreationDate') && vm.Account.CreationDate != "") {
            vm.Account.CreationDate = new Date(vm.Account.CreationDate);
        }
    }
    vm.filterevents = function (id) {
        //console.log("vm.Account", vm.Account);
        //console.log("vm.Account.Events", vm.Account.Events);
        vm.Calendar = vm.Account.Events.filter(function (v) {
            return v.EventsID === id; // Filter out the appropriate one
        })[0];
        vm.newEvent = vm.Calendar;
    }
    vm.showaccountdetail = function (id, event) {
        $scope.activeTab = 'accountsDetail';
        $scope.subActiveTab = 'details'
        vm.filteraccounts(id);
        vm.currentaccountid = id;
        vm.showAddForm = false;

        //vm.buttonvisible(true, true, false, true, false, false);
        vm.buttonvisible(false, true, false, true, false, false);
    }
    vm.showeventdetail = function (id, event) {
        vm.filterevents(id);
        vm.currenteventid = id;
        //console.log(" vm.filterevents(id)", vm.filterevents(id));
        //console.log(" vm.currenteventid", vm.currenteventid);
        vm.showCalendarReadOnly = true;
        vm.event_detail = false;
        vm.buttonvisible(true, true, false, true, false, false);
    }
    vm.close = function () {
        vm.showCalendarReadOnly = true;
        vm.showCalendarEdit = false;
        vm.event_detail = false;
        vm.buttonvisible(true, true, false, true, false, false);
    }
    vm.addNewAccount = function (id, event) {
        $scope.activeTab = 'accountsDetail';
        $scope.subActiveTab = 'details'
        vm.filteraccounts(id);
        vm.currentaccountid = id;
        vm.LastAccount = vm.Account;
        vm.Account = {};
        vm.showAddForm = true;
        //vm.buttonvisible(false, true, true, false, true, false);
        vm.buttonvisible(false, false, true, false, true, false);
    }

    $scope.searchObject = {};
    $scope.$on('advanced-searchbox:addedSearchParam', function (event, searchParameter) {
        ///
    });

    $scope.$on('advanced-searchbox:removedSearchParam', function (event, searchParameter) {
        /// Need to call ajax if tags is removed
        //console.log(searchParameter.key, " is removed", searchParameter);
    });

    $scope.$on('advanced-searchbox:removedAllSearchParam', function (event) {
        ///
    });

    $scope.$on('advanced-searchbox:enteredEditMode', function (event, searchParameter) {
        ///
    });

    $scope.$on('advanced-searchbox:leavedEditMode', function (event, searchParameter) {
        ///
    });

    $scope.$on('advanced-searchbox:modelUpdated', function (event, model) {
        ///
    });

    $scope.$on('advanced-searchbox:searchButtonClicked', function (event, model) {
        ///Get results with filters after user clicking submit button
        $scope.searchObject = model;
        model.type = "Accounts";
        model.object = vm.Accounts;
        //console.log("model", model);
        CRUDService.searchvalue(vm.Accounts, model.type, model).then(function (response) {
            //console.log("response", response);
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
        });

    });

});