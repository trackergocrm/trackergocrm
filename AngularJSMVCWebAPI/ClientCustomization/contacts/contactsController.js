﻿module.controller('ContactsController', function ($scope, $rootScope, Upload, $timeout, ContactsService, FlashService, CRUDService) {

    var vm = this;
    $scope.showiconadd = true;
    $scope.showiconsearch = true;
    $scope.showiconphonee = true;
    $scope.activeiconadd = false;
    $scope.activeiconsearch = false;
    $scope.activeiconphonee = false;
    $scope.activeTab = 'contactsFeed';
    $scope.subActiveTab = 'details';
    vm.Contact = null;
    vm.LastContact;
    vm.showAddForm = false;
    vm.isEditForm = false;
    vm.currentcontactid = -1;
    vm.event_detail = true;
    vm.message = null;
    vm.showadd = true;
    vm.showedit = true;
    vm.showsave = false;
    vm.showdelete = true;
    vm.showcancel = false;
    vm.showback = false;
    vm.addNewEvent = false;
    vm.newActivity_Text = "";
    vm.newNotes_Text = "";
    $rootScope.dashboardTitle = "Contacts Feed";
    $rootScope.dashboardMsg = "Overview of all the Contacts";
    vm.newEvent = {
        isRecurrence: false
    };
    $scope.availableSearchParams = [
        { key: "Subject", name: "Subject", placeholder: "Subject..." },
        { key: "Contact", name: "Contact", placeholder: "Contact..." },
        { key: "Phone", name: "Phone", placeholder: "Phone..." },
        { key: "emailAddress", name: "E-Mail", placeholder: "E-Mail..." },
        { key: "Date", name: "Date", placeholder: "Date...", type: "date" }
       //{ key: "SelectBox", name: "SelectBox", placeholder: "SelectBox...", type: "select", suggestedValues: ['test1', 'test2', 'test3'] }
    ];

    $scope.eventsSearchParams = [
        { key: "Search Param 1", name: "Search Param 1", placeholder: "Search Param 1..." },
        { key: "Search Param 2", name: "Search Param 2", placeholder: "Search Param 2..." }
    ];

    $scope.notesSearchParams = [
        { key: "Search Param 1", name: "Search Param 1", placeholder: "Search Param 1..." },
        { key: "Search Param 2", name: "Search Param 2", placeholder: "Search Param 2..." }
    ];
    $scope.startSearch = function () {

        $scope.searchVisible = true;
    }

    $scope.hideSearch = function () {

        $scope.searchVisible = false;
    }
    vm.buttonvisible = function (showadd, showedit, showsave, showdelete, showcancel, showback) {

        vm.showadd = showadd;
        vm.showedit = showedit;
        vm.showsave = showsave;
        vm.showdelete = showdelete;
        vm.showcancel = showcancel;
        vm.showback = showback;
    };

    $scope.changeWithOutSubTabs = function (iconName) {

        $scope.activeiconadd = false;
        $scope.activeiconsearch = false;
        $scope.activeiconphonee = false;

        if (iconName === 'add') {
            $scope.activeiconadd = true;
        }

        if (iconName === 'search') {
            $scope.activeiconsearch = true;
        }
        if (iconName === 'phonee') {
            $scope.activeiconphonee = true;
        }

    };



    $scope.changeSubTabs = function (tabName) {
        $scope.subActiveTab = tabName;
        if (tabName === 'details') {
            $rootScope.dashboardTitle = "Contacts Details";
            //vm.buttonvisible(true, true, false, true, false, false);
            vm.buttonvisible(false, true, false, true, false, false);
        }
        else if (tabName === 'events') {
            vm.isEditForm = true;
            vm.buttonvisible(true, false, false, false, false, false);
        }
        else {
            vm.isEditForm = true;
            vm.buttonvisible(false, false, false, false, false, false);

        }

        if (tabName === 'posts') {
            $rootScope.dashboardTitle = "Activities Details";
        }
        if (tabName === 'events') {
            vm.showCalendarReadOnly = false;
            vm.showCalendarEdit = false;
            vm.event_detail = true;
            $rootScope.dashboardTitle = "Events Details";
        }
        if (tabName === 'notes') {
            $rootScope.dashboardTitle = "Notes Details";
        }
        if (tabName === 'uploads') {
            $rootScope.dashboardTitle = "Uploads Details";
        }
        if (tabName === 'replies') {
            $rootScope.dashboardTitle = "Replies Details";
        }
    };

    vm.getall = function () {
        ContactsService.getall()
        .then(function (response) {
            vm.Contacts = response;
            //console.log(response)
            //console.log("vm.Contacts", vm.Contacts);
            // console.log(JSON.stringify(response));
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getall();

    vm.getActivities = function () {
        ContactsService.getActivityType()
        .then(function (response) {
            vm.ActivityType = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getActivities();

    vm.getAcountName = function () {
        ContactsService.getAcountName()
        .then(function (response) {
            vm.AcountName = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getAcountName();

    vm.getLeadContact = function () {
        ContactsService.getLeadContact()
        .then(function (response) {
            vm.LeadContact = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getLeadContact();

    vm.getLeadSource = function () {
        ContactsService.getLeadSource()
        .then(function (response) {
            vm.LeadSource = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getLeadSource();

    vm.getSalesStage = function () {
        ContactsService.getSalesStage()
        .then(function (response) {
            vm.SalesStage = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getSalesStage();

    vm.getSalesman = function () {
        ContactsService.getSalesman()
        .then(function (response) {
            vm.Salesman = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getSalesman();

    vm.getReferredBy = function () {
        ContactsService.getReferredBy()
        .then(function (response) {
            vm.ReferredBy = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getReferredBy();



    $scope.$watch('files', function () {
        // $scope.upload($scope.files);
    });
    $scope.$watch('file', function () {
        if ($scope.file != null) {
            $scope.files = [$scope.file];
        }
    });
    vm.deleteActivities = function (id) {
        for (var i = 0; i < vm.Contact.Activities.length; i++) {
            if ((vm.Contact.Activities != null) && (vm.Contact.Activities[i].ActivityID == id)) {
                vm.Contact.Activities.splice(i, 1);
                vm.save();
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
    };
    vm.deleteEvents = function (id) {
        for (var i = 0; i < vm.Contact.Events.length; i++) {
            if ((vm.Contact.Events != null) && (vm.Contact.Events[i].EventsID == id)) {
                vm.Contact.Events.splice(i, 1);
                vm.save();
                break;
            }
        }
        vm.showCalendarReadOnly = false;
        vm.showCalendarEdit = false;
        vm.event_detail = true;
        FlashService.Success("Deleted Successfully");
    };
    vm.deleteNotes = function (id) {
        for (var i = 0; i < vm.Contact.Notes.length; i++) {
            if ((vm.Contact.Notes != null) && (vm.Contact.Notes[i].NotesID == id)) {
                vm.Contact.Notes.splice(i, 1);
                vm.save();
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
    };
    vm.deleteUploads = function (id) {
        for (var i = 0; i < vm.Contact.Uploads.length; i++) {
            if ((vm.Contact.Uploads != null) && (vm.Contact.Uploads[i].UploadsID == id)) {
                vm.Contact.Uploads.splice(i, 1);
                vm.save();
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
    };
    vm.editEvents = function (id) {
        vm.addNewEvent = !vm.addNewEvent;
        for (var i = 0; i < vm.Contact.Events.length; i++) {
            if ((vm.Contact.Events != null) && (vm.Contact.Events[i].EventsID == id)) {
                vm.newEvent = vm.Contact.Events[i];
                break;
            }
        }
    };
    // upload on file select or drop
    vm.upload = function (file) {
        //console.log("file", file);
        CRUDService.addUpload(vm.Contact, "Contacts", file).then(function (response) {
            //console.log("response", response);
            vm.Contacts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.files = [];
            //console.log("vm.files", vm.files);
            FlashService.Success("Upload Saved Successfully!");
        });
        /* Upload.upload({
            url: 'upload/url',
            data: { file: file, 'username': $scope.username }
        }).then(function (resp) {
            // console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
        }, function (resp) {
            // console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            // console.log('progress: ' + progressPercentage + '% ');
        });*/
    };


    vm.email = function () {
        alert('email called');
    };
    vm.call = function () {
        alert('Call function called');
    };



    vm.cancel = function () {
        vm.showAddForm = false;
        if (vm.currentcontactid) {
            vm.filtercontacts(vm.currentcontactid);
            vm.buttonvisible(true, true, false, true, false, false);
        } else {
            $scope.activeTab = 'contactsFeed';
        }
        
    }

    vm.addNewActivity = function () {
        /* var newActivity = {};
         if (vm.Contact.Activities.length == 0) {
             newActivity.ActivityID = 1;
         }
         else {
             newActivity.ActivityID = vm.Contact.Activities[vm.Contact.Activities.length - 1].ActivityID + 1;
         }
         newActivity.Name = 'Manish G';
         newActivity.Text = vm.newActivity_Text;
         newActivity.ProfilePic = 'img/profileimg.png';
         newActivity.EditActivities = false;
         vm.Contact.Activities.push(newActivity);
 
         CRUDService.update(vm.Contact, "Contacts").then(function (response) {
             vm.Contacts = response;
             if ($scope.subActiveTab == 'details') {
                 vm.buttonvisible(false, true, false, true, false, false);
             }
             vm.newActivity_Text = "";
             FlashService.Success("Activity Saved Successfully!");
         });*/
        CRUDService.addNewActivity(vm.newActivity_Text, vm.Contact, "Contacts").then(function (response) {
            //console.log("response", response);
            vm.Contacts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newActivity_Text = "";
            FlashService.Success("Activity Saved Successfully!");
        });
    };
    vm.addNewReply = function () {
        /* var newReplyAdd = {};
         if (vm.Contact.Replies.length == 0) {
             newReplyAdd.RepliesID = 1;
         }
         else {
             newReplyAdd.RepliesID = vm.Contact.Replies[vm.Contact.Replies.length - 1].RepliesID + 1;
         }
         newReplyAdd.Comment = vm.reply.Text
         newReplyAdd.ProfilePic = "img/profileimg.png";
         newReplyAdd.Recepients = vm.reply.Recepients;
         newReplyAdd.CreationDate = "2016-08-16";
         newReplyAdd.ExcludeProspect = vm.reply.excludeProspect;
         newReplyAdd.OtherReplies = [];
         vm.Contact.Replies.push(newReplyAdd);
 
         CRUDService.update(vm.Contact, "Contacts").then(function (response) {
             vm.Contacts = response;
             if ($scope.subActiveTab == 'details') {
                 vm.buttonvisible(false, true, false, true, false, false);
             }
             vm.reply = {};
             FlashService.Success("Replies Saved Successfully!");
         });*/

        CRUDService.addNewReply(vm.reply, vm.Contact, "Contacts").then(function (response) {
            //console.log("response", response);
            vm.Contacts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.reply = {};
            FlashService.Success("Replies Saved Successfully!");
        });
    };
    vm.addsubreply = function (index) {
        /* var subReplyAdd = {};
         if (vm.Contact.Replies[index].OtherReplies.length == undefined || vm.Contact.Replies[index].OtherReplies.length == 0) {
             subReplyAdd.OtherRepliesID = 1;
         }
         else {
             subReplyAdd.OtherRepliesID = vm.Contact.Replies[index].OtherReplies[vm.Contact.Replies[index].OtherReplies.length - 1].OtherRepliesID + 1;
         }
         subReplyAdd.Comment = vm.subreply.Text
         subReplyAdd.ProfilePic = "img/profileimg.png";
         subReplyAdd.Recepients = vm.subreply.Recepients;
         subReplyAdd.CreationDate = "2016-08-16";
         subReplyAdd.ExcludeProspect = false;
         vm.Contact.Replies[index].OtherReplies.push(subReplyAdd);
 
 
         CRUDService.update(vm.Contact, "Contacts").then(function (response) {
             vm.Contacts = response;
             if ($scope.subActiveTab == 'details') {
                 vm.buttonvisible(false, true, false, true, false, false);
             }
             vm.subreply = {};
             vm.subreply.Recepients = '';
             FlashService.Success("Replies Saved Successfully!");
         });*/
        CRUDService.addsubreply(index, vm.subreply, vm.Contact, "Contacts").then(function (response) {
            //console.log("response", response);
            vm.Contacts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.subreply = {};
            vm.subreply.Recepients = '';
            FlashService.Success("Replies Saved Successfully!");
        });
    };

    vm.addNewEvents = function () {
        /* var newEvents = vm.newEvent;
         if (vm.isEditEventForm == true) {
             CRUDService.update(vm.Contact, "Contacts").then(function (response) {
                 vm.Contacts = response;
                 if ($scope.subActiveTab == 'details') {
                     vm.buttonvisible(false, true, false, true, false, false);
                 }
                 vm.newEvent = {};
                 FlashService.Success("Events Saved Successfully!");
             });
         }
         else {
             if (vm.Contact.Events.length == 0) {
                 newEvents.EventsID = 1;
             }
             else {
                 newEvents.EventsID = vm.Contact.Events[vm.Contact.Events.length - 1].EventsID + 1;
             }
             console.log("vm.Account", vm.Account);
             console.log("newEvents", newEvents);
             newEvents.EditEvents = false;
             vm.Contact.Events.push(newEvents);
 
             CRUDService.update(vm.Contact, "Contacts").then(function (response) {
                 vm.Contacts = response;
                 if ($scope.subActiveTab == 'details') {
                     vm.buttonvisible(false, true, false, true, false, false);
                 }
                 vm.newEvent = {};
                 FlashService.Success("Events Saved Successfully!");
             });
         }
         vm.showCalendarEdit = false;
         vm.event_detail = true;
         vm.isEditEventForm == false;
         vm.buttonvisible(true, false, false, false, false, false);*/
        CRUDService.addNewEvents(vm.newEvent, vm.isEditEventForm, vm.Contact, "Contacts").then(function (response) {
            //console.log("response", response);
            vm.Contacts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newEvent = {};
            FlashService.Success("Events Saved Successfully!");
        });
        /*vm.showCalendarEdit = false;
        vm.event_detail = true;
        vm.isEditEventForm == false;
        vm.buttonvisible(true, false, false, false, false, false);*/
        if (vm.isEditEventForm == true) {
            vm.showCalendarReadOnly = true;
            vm.event_detail = false;
            vm.isEditEventForm = false;
            vm.buttonvisible(true, false, true, true, false, false);
        } else {
            vm.event_detail = true;
            vm.buttonvisible(true, false, false, false, false, false);
        }

        vm.showCalendarEdit = false;
        vm.buttonvisible(true, false, true, true, false, false);

    };





    vm.addNewNotes = function () {
        /* var newNotes = {};
         if (vm.Contact.Notes.length == 0) {
             newNotes.NotesID = 1;
         } else {
         newNotes.NotesID = vm.Contact.Notes[vm.Contact.Notes.length - 1].NotesID + 1;
          }
         newNotes.Name = 'Manish G';
         newNotes.Text = vm.newNotes_Text;
         newNotes.ProfilePic = 'img/profileimg.png';
         newNotes.EditNotes = false;
         vm.Contact.Notes.push(newNotes);
 
         CRUDService.update(vm.Contact, "Contacts").then(function (response) {
             vm.Contacts = response;
             if ($scope.subActiveTab == 'details') {
                 vm.buttonvisible(false, true, false, true, false, false);
             }
             vm.newNotes_Text = "";
             FlashService.Success("Notes Saved Successfully!");
         });*/

        CRUDService.addNewNotes(vm.newNotes_Text, vm.Contact, "Contacts").then(function (response) {
            //console.log("response", response);
            vm.Contacts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newNotes_Text = "";
            FlashService.Success("Notes Saved Successfully!");
        });
    };
    vm.save = function () {
        vm.showAddForm = false;
        vm.Contact.Company = vm.Contact.CompanyId;
        vm.Contact.Mobile = vm.Contact.Phone;
        if (vm.isEditForm == true) {
            CRUDService.update(vm.Contact, "Contacts").then(function (response) {
                vm.Contacts = response;
                if ($scope.subActiveTab == 'details') {
                    // vm.buttonvisible(false, true, false, true, false, false);
                    vm.buttonvisible(true, true, false, true, false, false);
                }
                FlashService.Success("Saved Successfully!");
            });
        }
        else {
            vm.Contact.Id = vm.Contacts.length + 1;
            vm.Contact.Replies = [];
            vm.Contact.Uploads = [];
            vm.Contact.Notes = [];
            vm.Contact.Events = [];
            vm.Contact.Activities = [];
            vm.Contact.ContactId = "";
            CRUDService.insert(vm.Contact, "Contacts").then(function (response) {
                vm.Contacts = response;
                if ($scope.subActiveTab == 'details') {
                    //vm.buttonvisible(false, true, false, true, false, false);
                    vm.buttonvisible(true, true, false, true, false, false);
                    $scope.activeTab = 'contactsFeed';
                }
                vm.Contacts = [];
                vm.Contacts = CRUDService.getStorageContacts();
                FlashService.Success("Saved Successfully!");
            });
        }
    }

    vm.add = function () {
        vm.LastContact = vm.Contact;
        vm.Contact = {};
        vm.showAddForm = true;
        //vm.buttonvisible(false, true, true, false, true, false);
        vm.buttonvisible(false, false, true, false, true, false);
    }
    vm.addevent = function () {
        //console.log(" vm.Account", vm.Account);
        vm.newEvent = {
            isRecurrence: false
        };
        vm.showCalendarEdit = true;
        vm.event_detail = false;
        vm.buttonvisible(false, false, true, true, false, false);
    }
    vm.delete = function () {
        if (confirm("Delete this contact?")) {

            CRUDService.delete(vm.currentcontactid, "Contacts").then(function (response) {
                vm.Contacts = response;
                if ($scope.subActiveTab == 'details') {
                    vm.buttonvisible(false, true, false, true, false, false);
                }
            });
            FlashService.Success("Deleted Successfully!");
        }
        //$scope.activeTab = 'contactsFeed';
        $scope.activeTab = 'contactsDetail';
        vm.buttonvisible(false, true, false, true, false, false);
    };

    vm.edit = function () {
        vm.showAddForm = true;
        vm.isEditForm = true;
        vm.filtercontacts(vm.currentcontactid);
        vm.buttonvisible(false, false, true, false, true, false);
    }
    vm.editevent = function () {
        vm.showCalendarReadOnly = false;
        vm.showCalendarEdit = true;
        vm.isEditEventForm = true;
        vm.filterevents(vm.currenteventid);
        //console.log("vm.newEvent", vm.newEvent);
        vm.buttonvisible(false, false, true, true, false, false);
    }
    vm.filtercontacts = function (id) {
        vm.Contact = vm.Contacts.filter(function (v) {
            return v.Id === id; // Filter out the appropriate one
        })[0];
        if (vm.Contact && vm.Contact.hasOwnProperty('CreationDate') && vm.Contact.CreationDate != "") {
            vm.Contact.CreationDate = new Date(vm.Contact.CreationDate);
        }
       
    }
    vm.filterevents = function (id) {
        //console.log("vm.Contact", vm.Contact);
        //console.log("vm.Contact.Events", vm.Contact.Events);
        vm.Calendar = vm.Contact.Events.filter(function (v) {
            return v.EventsID === id; // Filter out the appropriate one
        })[0];
        vm.newEvent = vm.Calendar;
    }
    vm.showcontactdetail = function (id, event) {
        $scope.activeTab = 'contactsDetail';
        $scope.subActiveTab = 'details'
        vm.filtercontacts(id);
        vm.currentcontactid = id;
        vm.showAddForm = false;

        //vm.buttonvisible(true, true, false, true, false, false);
        vm.buttonvisible(false, true, false, true, false, false);
    }
    vm.showeventdetail = function (id, event) {
        //console.log("id", id);
        //console.log("event", event);
        vm.filterevents(id);
        vm.currenteventid = id;
        vm.showCalendarReadOnly = true;
        vm.event_detail = false;
        vm.buttonvisible(true, true, false, true, false, false);
    }
    vm.close = function () {
        vm.showCalendarReadOnly = true;
        vm.showCalendarEdit = false;
        vm.event_detail = false;
        vm.buttonvisible(true, true, false, true, false, false);
    }
    vm.addNewContact = function (id, event) {
        $scope.activeTab = 'contactsDetail';
        $scope.subActiveTab = 'details'
        vm.filtercontacts(id);
        vm.currentcontactid = id;
        vm.LastContact = vm.Contact;
        vm.Contact = {};
        vm.showAddForm = true;
        //vm.buttonvisible(false, true, true, false, true, false);
        vm.buttonvisible(false, false, true, false, true, false);

    }

    $scope.searchObject = {};
    $scope.$on('advanced-searchbox:addedSearchParam', function (event, searchParameter) {
        ///
    });

    $scope.$on('advanced-searchbox:removedSearchParam', function (event, searchParameter) {
        /// Need to call ajax if tags is removed
        //console.log(searchParameter.key, " is removed", searchParameter);
    });

    $scope.$on('advanced-searchbox:removedAllSearchParam', function (event) {
        ///
    });

    $scope.$on('advanced-searchbox:enteredEditMode', function (event, searchParameter) {
        ///
    });

    $scope.$on('advanced-searchbox:leavedEditMode', function (event, searchParameter) {
        ///
    });

    $scope.$on('advanced-searchbox:modelUpdated', function (event, model) {
        ///
    });

    $scope.$on('advanced-searchbox:searchButtonClicked', function (event, model) {
        ///Get results with filters after user clicking submit button
        $scope.searchObject = model;
        model.type = "Contacts";
        model.object = vm.Contacts;
        //console.log("model", model);
        CRUDService.searchvalue(vm.Contacts, model.type, model).then(function (response) {
            //console.log("response", response);
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
        });

    });

});