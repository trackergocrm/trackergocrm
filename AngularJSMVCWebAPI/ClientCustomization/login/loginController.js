﻿module.controller('LoginController', function ($rootScope, $scope, $location, AuthenticationService, FlashService, UserService) {

    var vm = this;
    $rootScope.showHeaderFooter = false;

    vm.login = login;
    vm.user = {
        "email": 'a@a.com',
        "password": 1
    };
    (function initController() {
        // reset login status
        AuthenticationService.ClearCredentials();
        UserService.Create(vm.user);
    })();

    function login() {
        vm.dataLoading = true;
        AuthenticationService.Login(vm.username, vm.password, function (response) {
            if (response.success) {
                AuthenticationService.SetCredentials(vm.username, vm.password);
                $rootScope.showHeaderFooter = true;
                $location.path('/leads');
            } else {
                FlashService.Error(response.message);
                vm.dataLoading = false;
            }
        });
    };

});