﻿module.controller('LeadsController', function ($scope, $rootScope, Upload, $timeout, LeadsService, FlashService,$filter, CRUDService) {

    var vm = this;
    $scope.showiconadd = true;
    $scope.showiconsearch = true;
    $scope.activeiconadd = false;
    $scope.activeiconsearch = false;
    vm.event_detail = true;
    vm.showiconadd = true;
    vm.showiconsearch = true;
    vm.activeiconadd = false;
    vm.activeiconsearch = false;
    $scope.activeTab = 'leadsFeed';
    $scope.subActiveTab = 'details';
    vm.Lead = null;
    vm.LastLead;
    vm.showAddForm = false;
    vm.isEditForm = false;
    vm.currentleadid = -1;
    vm.message = null;
    vm.showadd = true;
    vm.showedit = true;
    vm.showsave = false;
    vm.showdelete = true;
    vm.showcancel = false;
    vm.showback = false;
    vm.addNewEvent = false;
    vm.newActivity_Text = "";
    vm.newNotes_Text = "";
    vm.searchVisible = false;
    $rootScope.dashboardTitle = "Leads Feed";
    $rootScope.dashboardMsg = "Overview of all the Leads";
    vm.newEvent = {
        isRecurrence: false
    };
    $scope.availableSearchParams = [
        { key: "Subject", name: "Subject", placeholder: "Subject..." },
        { key: "Contact", name: "Contact", placeholder: "Contact..." },
        { key: "Phone", name: "Phone", placeholder: "Phone..." },
        { key: "emailAddress", name: "E-Mail", placeholder: "E-Mail..." },
        { key: "Date", name: "Date", placeholder: "Date...", type: "date" }
       //{ key: "SelectBox", name: "SelectBox", placeholder: "SelectBox...", type: "select", suggestedValues: ['test1', 'test2', 'test3'] }
    ];

    $scope.eventsSearchParams = [
        { key: "Search Param 1", name: "Search Param 1", placeholder: "Search Param 1..." },
        { key: "Search Param 2", name: "Search Param 2", placeholder: "Search Param 2..." }
    ];

    $scope.notesSearchParams = [
        { key: "Search Param 1", name: "Search Param 1", placeholder: "Search Param 1..." },
        { key: "Search Param 2", name: "Search Param 2", placeholder: "Search Param 2..." }
    ];


    $scope.startSearch = function ()
    {

        $scope.searchVisible = true;
    }

    $scope.hideSearch = function () {

        $scope.searchVisible = false;
    }




    vm.buttonvisible = function (showadd, showedit, showsave, showdelete, showcancel, showback) {

        vm.showadd = showadd;
        vm.showedit = showedit;
        vm.showsave = showsave;
        vm.showdelete = showdelete;
        vm.showcancel = showcancel;
        vm.showback = showback;
    };

    $scope.changeWithOutSubTabs = function (iconName) {

        $scope.activeiconadd = false;
        $scope.activeiconsearch = false;

        if (iconName === 'add') {
            $scope.activeiconadd = true;
        }

        if (iconName === 'search') {
            $scope.activeiconsearch = true;
        }

    };

   /* $scope.changeWithOutSubTabs = function (iconName)
    {

        activeiconadd = false;
        activeiconsearch = false;

        if (iconName === 'add') {
            activeiconadd = true;
        }

        if (iconName === 'search') {
            activeiconsearch = true;
        }

    };*/

    $scope.changeSubTabs = function (tabName) {
        $scope.subActiveTab = tabName;
        if (tabName === 'details') {
            $rootScope.dashboardTitle = "Leads Details";
            //vm.buttonvisible(true, true, false, true, false, false);
            vm.buttonvisible(false, true, false, true, false, false);
        }
        else if (tabName === 'events') {
            vm.isEditForm = true;
            vm.buttonvisible(true, false, false, false, false, false);
        }
        else {
            vm.isEditForm = true;
            vm.buttonvisible(false, false, false, false, false, false);
           
        }

        if (tabName === 'posts') {
            $rootScope.dashboardTitle = "Activities Details";
        }
        if (tabName === 'events') {
            vm.showCalendarReadOnly = false;
            vm.showCalendarEdit = false;
            vm.event_detail = true;
            $rootScope.dashboardTitle = "Events Details";
        }
        if (tabName === 'notes') {
            $rootScope.dashboardTitle = "Notes Details";
        }
        if (tabName === 'uploads') {
            $rootScope.dashboardTitle = "Uploads Details";
        }
        if (tabName === 'replies') {
            $rootScope.dashboardTitle = "Replies Details";
        }
    };

    vm.getall = function () {
        LeadsService.getall()
        .then(function (response) {
            vm.Leads = response;
            // console.log(JSON.stringify(response));
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getall();

    vm.getActivities = function () {
        LeadsService.getActivityType()
        .then(function (response) {
            vm.ActivityType = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getActivities();

    vm.getAcountName = function () {
        LeadsService.getAcountName()
        .then(function (response) {
            vm.AcountName = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getAcountName();

    vm.getLeadContact = function () {
        LeadsService.getLeadContact()
        .then(function (response) {
            vm.LeadContact = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getLeadContact();

    vm.getLeadSource = function () {
        LeadsService.getLeadSource()
        .then(function (response) {
            vm.LeadSource = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getLeadSource();

    vm.getSalesStage = function () {
        LeadsService.getSalesStage()
        .then(function (response) {
            vm.SalesStage = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getSalesStage();

    vm.getSalesman = function () {
        LeadsService.getSalesman()
        .then(function (response) {
            vm.Salesman = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getSalesman();

    vm.getReferredBy = function () {
        LeadsService.getReferredBy()
        .then(function (response) {
            vm.ReferredBy = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getReferredBy();


    $scope.$watch('vm.files', function () {
        // $scope.upload($scope.files);
        //console.log("vm.files", vm.files);
        //vm.upload(vm.files);
    });
    $scope.$watch('file', function () {
        if ($scope.file != null) {
            vm.files = [$scope.file];
        }
    });
    vm.deleteActivities = function (id) {
        for (var i = 0; i < vm.Lead.Activities.length; i++) {
            if ((vm.Lead.Activities != null) && (vm.Lead.Activities[i].ActivityID == id)) {
                vm.Lead.Activities.splice(i, 1);
                vm.save();
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
    };
    vm.deleteEvents = function (id) {
        for (var i = 0; i < vm.Lead.Events.length; i++) {
            if ((vm.Lead.Events != null) && (vm.Lead.Events[i].EventsID == id)) {
                vm.Lead.Events.splice(i, 1);
                vm.save();
                break;
            }
        }
        vm.showCalendarReadOnly = false;
        vm.showCalendarEdit = false;
        vm.event_detail = true;
        FlashService.Success("Deleted Successfully");
    };
    vm.deleteNotes = function (id) {
        for (var i = 0; i < vm.Lead.Notes.length; i++) {
            if ((vm.Lead.Notes != null) && (vm.Lead.Notes[i].NotesID == id)) {
                vm.Lead.Notes.splice(i, 1);
                vm.save();
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
    };
    vm.deleteUploads = function (id) {
        for (var i = 0; i < vm.Lead.Uploads.length; i++) {
            if ((vm.Lead.Uploads != null) && (vm.Lead.Uploads[i].UploadsID == id)) {
                vm.Lead.Uploads.splice(i, 1);
                vm.save();
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
    };
    vm.editEvents = function (id) {
        vm.addNewEvent = !vm.addNewEvent;
        for (var i = 0; i < vm.Lead.Events.length; i++) {
            if ((vm.Lead.Events != null) && (vm.Lead.Events[i].EventsID == id)) {
                vm.newEvent = vm.Lead.Events[i];
                break;
            }
        }
    };
    // upload on file select or drop
    vm.upload = function (file) {
        //console.log("file", file);
        CRUDService.addUpload(vm.Lead, "Leads", file).then(function (response) {
            //console.log("response", response);
            vm.Leads = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.files = [];
            //console.log("vm.files", vm.files);
            FlashService.Success("Upload Saved Successfully!");
        });
        /*Upload.upload({
            url: 'upload/url',
            data: { file: file, 'username': $scope.username }
        }).then(function (resp) {
            // console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
        }, function (resp) {
            // console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            // console.log('progress: ' + progressPercentage + '% ');
       });*/
    };


    vm.email = function () {
        alert('email called');
    };
    vm.call = function () {
        alert('Call function called');
    };



    vm.cancel = function () {
        vm.showAddForm = false;
        if (vm.currentleadid) {
            vm.filterleads(vm.currentleadid);
            vm.buttonvisible(true, true, false, true, false, false);
        } else {
            $scope.activeTab = 'leadsFeed';
        }
        
    }

    vm.addNewActivity = function () {
       /* var newActivity = {};
        newActivity.ActivityID = vm.Lead.Activities[vm.Lead.Activities.length - 1].ActivityID + 1;
        newActivity.Name = 'Manish G';
        newActivity.Text = vm.newActivity_Text;
        newActivity.ProfilePic = 'img/profileimg.png';
        newActivity.EditActivities = false;
        vm.Lead.Activities.push(newActivity);

        LeadsService.update(vm.Lead).then(function (response) {
            vm.Leads = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newActivity_Text = "";
            FlashService.Success("Activity Saved Successfully!");
        });*/
        CRUDService.addNewActivity(vm.newActivity_Text, vm.Lead, "Leads").then(function (response) {
            //console.log("response", response);
            vm.Leads = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newActivity_Text = "";
            FlashService.Success("Activity Saved Successfully!");
        });
    };
    vm.addNewReply = function () {
       /* var newReplyAdd = {};
        newReplyAdd.RepliesID = vm.Lead.Replies[vm.Lead.Replies.length - 1].RepliesID + 1;
        newReplyAdd.Comment = vm.reply.Text
        newReplyAdd.ProfilePic = "img/profileimg.png";
        newReplyAdd.Recepients = vm.reply.Recepients;
        newReplyAdd.CreationDate = "2016-08-16";
        newReplyAdd.ExcludeProspect = vm.reply.excludeProspect;
        newReplyAdd.OtherReplies = [];
        vm.Lead.Replies.push(newReplyAdd);

        LeadsService.update(vm.Lead).then(function (response) {
            vm.Leads = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.reply = {};
            FlashService.Success("Replies Saved Successfully!");
        });*/
        CRUDService.addNewReply(vm.reply, vm.Lead, "Leads").then(function (response) {
            //console.log("response", response);
            vm.Leads = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.reply = {};
            FlashService.Success("Replies Saved Successfully!");
        });
    };
    vm.addsubreply = function (index) {
       /* var subReplyAdd = {};
        if (vm.Lead.Replies[index].OtherReplies.length == undefined || vm.Lead.Replies[index].OtherReplies.length == 0) {
            subReplyAdd.OtherRepliesID = 1;
        }
        else {
            subReplyAdd.OtherRepliesID = vm.Lead.Replies[index].OtherReplies[vm.Lead.Replies[index].OtherReplies.length - 1].OtherRepliesID + 1;
        }
        subReplyAdd.Comment = vm.subreply.Text
        subReplyAdd.ProfilePic = "img/profileimg.png";
        subReplyAdd.Recepients = vm.subreply.Recepients;
        subReplyAdd.CreationDate = "2016-08-16";
        subReplyAdd.ExcludeProspect = false;
        vm.Lead.Replies[index].OtherReplies.push(subReplyAdd);
        

        LeadsService.update(vm.Lead).then(function (response) {
            vm.Leads = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.subreply = {};
            vm.subreply.Recepients = '';
            FlashService.Success("Replies Saved Successfully!");
        });*/
        CRUDService.addsubreply(index, vm.subreply, vm.Lead, "Leads").then(function (response) {
            //console.log("response", response);
            vm.Leads = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.subreply = {};
            vm.subreply.Recepients = '';
            FlashService.Success("Replies Saved Successfully!");
        });
    };
    vm.addNewEvents = function () {
       /* var newEvents = vm.newEvent;
        if (vm.newEvent.EventsID == undefined) {
            newEvents.EventsID = vm.Lead.Events[vm.Lead.Events.length - 1].EventsID + 1;
        }

        newEvents.EditActivities = false;
        vm.Lead.Events.push(newEvents);

        LeadsService.update(vm.Lead).then(function (response) {
            vm.Leads = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newEvent = {};
            FlashService.Success("Events Saved Successfully!");
        });*/
        CRUDService.addNewEvents(vm.newEvent, vm.isEditEventForm, vm.Lead, "Leads").then(function (response) {
            //console.log("response", response);
            vm.Leads = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newEvent = {};
            FlashService.Success("Events Saved Successfully!");
        });
        if (vm.isEditEventForm == true) {
            vm.showCalendarReadOnly = true;
            vm.event_detail = false;
            vm.isEditEventForm = false;
            vm.buttonvisible(true, false, true, true, false, false);
        } else {
            vm.event_detail = true;
            vm.buttonvisible(true, false, false, false, false, false);
        }

        vm.showCalendarEdit = false;
        

       /* vm.showCalendarEdit = false;
        vm.event_detail = true;
        vm.isEditEventForm == false;
        vm.buttonvisible(true, false, false, false, false, false);*/
    };
    vm.addNewNotes = function () {
       /*var newNotes = {};
        newNotes.NotesID = vm.Lead.Notes[vm.Lead.Notes.length - 1].NotesID + 1;
        newNotes.Name = 'Manish G';
        newNotes.Text = vm.newNotes_Text;
        newNotes.ProfilePic = 'img/profileimg.png';
        newNotes.EditActivities = false;
        vm.Lead.Notes.push(newNotes);

        LeadsService.update(vm.Lead).then(function (response) {
            vm.Leads = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newNotes_Text = "";
            FlashService.Success("Notes Saved Successfully!");
        });*/
        CRUDService.addNewNotes(vm.newNotes_Text, vm.Lead, "Leads").then(function (response) {
            //console.log("response", response);
            vm.Leads = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newNotes_Text = "";
            FlashService.Success("Notes Saved Successfully!");
        });
    };
    vm.save = function () {
        vm.showAddForm = false;
        vm.Lead.Company = vm.Lead.CompanyId;
        vm.Lead.LeadSource = vm.Lead.LeadSourceId;
        vm.Lead.ReferredBy = vm.Lead.ReferredById;
        vm.Lead.SalesStage = vm.Lead.SalesStageId;
        vm.Lead.Salesman = vm.Lead.SalesManId;
        vm.Lead.LeadContact = vm.Lead.LeadContactId;
        if (vm.isEditForm == true) {
            CRUDService.update(vm.Lead, "Leads").then(function (response) {
                vm.Leads = response;
                //console.log("vm.Leads", vm.Leads);
                if ($scope.subActiveTab == 'details') {
                    //vm.buttonvisible(false, true, false, true, false, false);
                    vm.buttonvisible(true, true, false, true, false, false);
                }
                FlashService.Success("Saved Successfully!");
            });
        }
        else {

            vm.Leads = CRUDService.getStorageLeads();
            vm.Lead.Id = vm.Leads.length + 1;
            vm.Lead.CreationDate = new Date(vm.Lead.CreationDate);
            vm.Lead.Phone = "";
            vm.Lead.Email = "";
            vm.Lead.Revenue = "";
            vm.Lead.PotentialRevenue = "";
            vm.Lead.Active = "";
            vm.Lead.Title = "Lead";
            vm.Lead.Activities = [];
            vm.Lead.Events = [];
            vm.Lead.Notes = [];
            vm.Lead.Uploads = [];
            vm.Lead.Replies = [];
            CRUDService.insert(vm.Lead, "Leads").then(function (response) {
                vm.Leads = response;
                if ($scope.subActiveTab == 'details') {
                    //vm.buttonvisible(false, true, false, true, false, false);
                    vm.buttonvisible(true, true, false, true, false, false);
                    $scope.activeTab = 'leadsFeed';
                }
                vm.Leads = [];
                vm.Leads = CRUDService.getStorageLeads();
                //console.log(vm.Leads);
                FlashService.Success("Saved Successfully!");
            });
        }
    }

    vm.add = function () {
            vm.LastLead = vm.Lead;
            vm.Lead = {};
            vm.showAddForm = true;
        //vm.buttonvisible(false, true, true, false, true, false);
            vm.buttonvisible(false, false, true, false, true, false);
    }
    vm.addevent = function () {
        //console.log(" vm.Lead", vm.Lead);
        vm.newEvent = {
            isRecurrence: false
        };
        vm.showCalendarEdit = true;
        vm.event_detail = false;
        vm.buttonvisible(false, false, true, true, false, false);
    }
    vm.delete = function () {
       /* if (confirm("Delete this lead?")) {

            LeadsService.delete(vm.currentleadid).then(function (response) {
                vm.Leads = response;
                if ($scope.subActiveTab == 'details') {
                    vm.buttonvisible(false, true, false, true, false, false);
                }
            });
            FlashService.Success("Deleted Successfully!");
        }
        $scope.activeTab = 'leadsFeed';
        vm.buttonvisible(false, true, false, true, false, false);*/
        if (confirm("Delete this lead?")) {

            CRUDService.delete(vm.currentleadid, "Leads").then(function (response) {
                vm.Leads = response;
                //console.log("vm.Leads", vm.Leads);
                if ($scope.subActiveTab == 'details') {
                    vm.buttonvisible(false, true, false, true, false, false);
                }
            });
            vm.Leads = [];
            vm.Leads = CRUDService.getStorageLeads();
            FlashService.Success("Deleted Successfully!");
        }
        // $scope.activeTab = 'leadsFeed';
        $scope.activeTab = 'leadsDetail';
        vm.buttonvisible(false, true, false, true, false, false);
    };

    vm.edit = function () {
        vm.showAddForm = true;
        vm.isEditForm = true;
        vm.filterleads(vm.currentleadid);
        vm.buttonvisible(false, false, true, false, true, false);
    }

    vm.editevent = function () {
        vm.showCalendarReadOnly = false;
        vm.showCalendarEdit = true;
        vm.isEditEventForm = true;
        vm.filterevents(vm.currenteventid);
        //console.log("vm.newEvent", vm.newEvent);
        vm.buttonvisible(false, false, true, true, false, false);
    }

    vm.filterleads = function (id) {
        vm.Lead = vm.Leads.filter(function (v) {
            return v.Id === id; // Filter out the appropriate one
        })[0];
        if (vm.Lead && vm.Lead.hasOwnProperty('CreationDate') && vm.Lead.CreationDate != "") {
            vm.Lead.CreationDate = new Date(vm.Lead.CreationDate);
        }
        
    }
    vm.filterevents = function (id) {
        //console.log("vm.Lead", vm.Lead);
        //console.log("vm.Lead.Events", vm.Lead.Events);
        vm.Calendar = vm.Lead.Events.filter(function (v) {
            return v.EventsID === id; // Filter out the appropriate one
        })[0];
        vm.newEvent = vm.Calendar;
    }
    vm.showleaddetail = function (id, event) {
        $scope.activeTab = 'leadsDetail';
        $scope.subActiveTab = 'details'
        vm.filterleads(id);
        vm.currentleadid = id;
        vm.showAddForm = false;

        //vm.buttonvisible(true, true, false, true, false, false);
        vm.buttonvisible(false, true, false, true, false, false);
    }
    vm.showeventdetail = function (id, event) {
        vm.filterevents(id);
        vm.currenteventid = id;
        //console.log(" vm.filterevents(id)", vm.filterevents(id));
        //console.log(" vm.currenteventid", vm.currenteventid);
        vm.showCalendarReadOnly = true;
        vm.event_detail = false;
        vm.buttonvisible(true, true, false, true, false, false);
    }
    vm.close = function () {
        vm.showCalendarReadOnly = true;
        vm.showCalendarEdit = false;
        vm.event_detail = false;
        vm.buttonvisible(true, true, false, true, false, false);
    }
    vm.addNewLead = function (id, event) {
        $scope.activeTab = 'leadsDetail';
        $scope.subActiveTab = 'details'
        vm.filterleads(id);
        vm.currentleadid = id;
        vm.LastLead = vm.Lead;
        vm.Lead = {};
        vm.showAddForm = true;
        //vm.buttonvisible(false, true, true, false, true, false);
        vm.buttonvisible(false, false, true, false, true, false);
    }

    $scope.searchObject = {};
    $scope.$on('advanced-searchbox:addedSearchParam', function (event, searchParameter) {
        ///
    });

    $scope.$on('advanced-searchbox:removedSearchParam', function (event, searchParameter) {
        /// Need to call ajax if tags is removed
        //console.log(searchParameter.key, " is removed", searchParameter);
    });

    $scope.$on('advanced-searchbox:removedAllSearchParam', function (event) {
        ///
    });

    $scope.$on('advanced-searchbox:enteredEditMode', function (event, searchParameter) {
        ///
    });

    $scope.$on('advanced-searchbox:leavedEditMode', function (event, searchParameter) {
        ///
    });

    $scope.$on('advanced-searchbox:modelUpdated', function (event, model) {
        ///
    });

    $scope.$on('advanced-searchbox:searchButtonClicked', function (event, model) {
        ///Get results with filters after user clicking submit button
        $scope.searchObject = model;
        model.type = "Leads";
        model.object = vm.Leads;
        //console.log("model", model);
        CRUDService.searchvalue(vm.Leads, model.type, model).then(function (response) {
            //console.log("response", response);
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
        });

    });

});