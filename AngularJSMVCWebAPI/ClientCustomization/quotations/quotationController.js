﻿module.controller('QuotationsController', function ($scope, $rootScope, Upload, $timeout, QuotationsService, FlashService, CRUDService) {
	
    var vm = this;
    $scope.showiconadd = true;
    $scope.showiconsearch = true;
    $scope.activeiconadd = false;
    $scope.activeiconsearch = false;
    $scope.activeTab = 'quotationsFeed';
    $scope.subActiveTab = 'details';
    vm.Quotation = null;
    vm.LastQuotation;
    vm.showAddForm = false;
    vm.isEditForm = false;
    vm.currentquotationid = -1;
    vm.event_detail = true;
    vm.message = null;
    vm.showadd = true;
    vm.showedit = true;
    vm.showsave = false;
    vm.showdelete = true;
    vm.showitemsave = false;
    vm.showitemcancel = false;
    
    vm.showcancel = false;
    vm.showback = false;
    vm.addNewEvent = false;
    vm.newActivity_Text = "";
    vm.newNotes_Text = "";
	vm.searchVisible = false;
    $rootScope.dashboardTitle = "Quotation Feed";
    $rootScope.dashboardMsg = "Overview of all the Quotation";
    vm.newEvent = {
        isRecurrence: false
    };
    vm.deleteItem = false;
    vm.readvalue = false;
    $scope.availableSearchParams = [
        { key: "Subject", name: "Subject", placeholder: "Subject..." },
        { key: "Contact", name: "Contact", placeholder: "Contact..." },
        { key: "Phone", name: "Phone", placeholder: "Phone..." },
        { key: "emailAddress", name: "E-Mail", placeholder: "E-Mail..." },
        { key: "Date", name: "Date", placeholder: "Date...", type: "date" }
       //{ key: "SelectBox", name: "SelectBox", placeholder: "SelectBox...", type: "select", suggestedValues: ['test1', 'test2', 'test3'] }
    ];

    $scope.eventsSearchParams = [
        { key: "Search Param 1", name: "Search Param 1", placeholder: "Search Param 1..." },
        { key: "Search Param 2", name: "Search Param 2", placeholder: "Search Param 2..." }
    ];

    $scope.notesSearchParams = [
        { key: "Search Param 1", name: "Search Param 1", placeholder: "Search Param 1..." },
        { key: "Search Param 2", name: "Search Param 2", placeholder: "Search Param 2..." }
    ];
	
	$scope.startSearch = function ()
    {

        $scope.searchVisible = true;
    }

    $scope.hideSearch = function () {

        $scope.searchVisible = false;
    }

    vm.buttonvisible = function (showadd, showedit, showsave, showdelete, showcancel, showback) {

        vm.showadd = showadd;
        vm.showedit = showedit;
        vm.showsave = showsave;
        vm.showdelete = showdelete;
        vm.showcancel = showcancel;
        vm.showback = showback;
    };

    $scope.changeWithOutSubTabs = function (iconName) {

        $scope.activeiconadd = false;
        $scope.activeiconsearch = false;

        if (iconName === 'add') {
            $scope.activeiconadd = true;
        }

        if (iconName === 'search') {
            $scope.activeiconsearch = true;
        }

    };

    $scope.changeSubTabs = function (tabName) {
        $scope.subActiveTab = tabName;
        if (tabName === 'details') {
            $rootScope.dashboardTitle = "Quotation Details";
            //vm.buttonvisible(true, true, false, true, false, false);
            vm.buttonvisible(false, true, false, true, false, false);
        }
        else if (tabName === 'events') {
            vm.isEditForm = true;
            vm.buttonvisible(true, false, false, false, false, false);
        }
        else {
            vm.isEditForm = true;
            vm.buttonvisible(false, false, false, false, false, false);
           
        }

        if (tabName === 'posts') {
            $rootScope.dashboardTitle = "Activities Details";
        }
        if (tabName === 'events') {
            vm.showCalendarReadOnly = false;
            vm.showCalendarEdit = false;
            vm.event_detail = true;
            $rootScope.dashboardTitle = "Events Details";
        }
        if (tabName === 'notes') {
            $rootScope.dashboardTitle = "Notes Details";
        }
        if (tabName === 'uploads') {
            $rootScope.dashboardTitle = "Uploads Details";
        }
        if (tabName === 'replies') {
            $rootScope.dashboardTitle = "Replies Details";
        }
    };

    vm.getall = function () {
        QuotationsService.getall()
        .then(function (response) {
            vm.Quotations = response;
            // console.log(JSON.stringify(response));
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getall();

    vm.getActivities = function () {
        QuotationsService.getActivityType()
        .then(function (response) {
            vm.ActivityType = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getActivities();

    vm.getAcountName = function () {
        QuotationsService.getAcountName()
        .then(function (response) {
            vm.AcountName = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getAcountName();

    vm.getLeadContact = function () {
        QuotationsService.getLeadContact()
        .then(function (response) {
            vm.LeadContact = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getLeadContact();

    vm.getLeadSource = function () {
        QuotationsService.getLeadSource()
        .then(function (response) {
            vm.LeadSource = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getLeadSource();

    vm.getSalesStage = function () {
        QuotationsService.getSalesStage()
        .then(function (response) {
            vm.SalesStage = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getSalesStage();

    vm.getSalesman = function () {
        QuotationsService.getSalesman()
        .then(function (response) {
            vm.Salesman = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getSalesman();

    vm.getReferredBy = function () {
        QuotationsService.getReferredBy()
        .then(function (response) {
            vm.ReferredBy = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getReferredBy();



    $scope.$watch('files', function () {
        // $scope.upload($scope.files);
    });
    $scope.$watch('file', function () {
        if ($scope.file != null) {
            $scope.files = [$scope.file];
        }
    });
    vm.deleteItems = function (id) {
        for (var i = 0; i < vm.Quotation.Items.length; i++) {
            if ((vm.Quotation.Items != null) && (vm.Quotation.Items[i].ItemId == id)) {
                vm.Quotation.Items.splice(i, 1);
                CRUDService.update(vm.Quotation, "Quotations").then(function (response) {
                    vm.Quotations = response;
                    //console.log("vm.Quotations", vm.Quotations);
                    if ($scope.subActiveTab == 'details') {
                        vm.buttonvisible(true, true, false, true, false, false);
                    }
                    FlashService.Success("Saved Successfully!");
                });
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
        $scope.activeTab = 'quotationsDetail';
        vm.buttonvisible(false, true, false, true, false, false);
    };
    vm.deleteActivities = function (id) {
        for (var i = 0; i < vm.Quotation.Activities.length; i++) {
            if ((vm.Quotation.Activities != null) && (vm.Quotation.Activities[i].ActivityID == id)) {
                vm.Quotation.Activities.splice(i, 1);
                vm.save();
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
    };
    vm.deleteEvents = function (id) {
        for (var i = 0; i < vm.Quotation.Events.length; i++) {
            if ((vm.Quotation.Events != null) && (vm.Quotation.Events[i].EventsID == id)) {
                vm.Quotation.Events.splice(i, 1);
                vm.save();
                break;
            }
        }
        vm.showCalendarReadOnly = false;
        vm.showCalendarEdit = false;
        vm.event_detail = true;
        FlashService.Success("Deleted Successfully");
    };
    vm.deleteNotes = function (id) {
        for (var i = 0; i < vm.Quotation.Notes.length; i++) {
            if ((vm.Quotation.Notes != null) && (vm.Quotation.Notes[i].NotesID == id)) {
                vm.Quotation.Notes.splice(i, 1);
                vm.save();
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
    };
    vm.deleteUploads = function (id) {
        for (var i = 0; i < vm.Quotation.Uploads.length; i++) {
            if ((vm.Quotation.Uploads != null) && (vm.Quotation.Uploads[i].UploadsID == id)) {
                vm.Quotation.Uploads.splice(i, 1);
                vm.save();
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
    };
    vm.editEvents = function (id) {
        vm.addNewEvent = !vm.addNewEvent;
        for (var i = 0; i < vm.Quotation.Events.length; i++) {
            if ((vm.Quotation.Events != null) && (vm.Quotation.Events[i].EventsID == id)) {
                vm.newEvent = vm.Quotation.Events[i];
                break;
            }
        }
    };
    // upload on file select or drop
    vm.upload = function (file) {
        CRUDService.addUpload(vm.Quotation, "Quotations", file).then(function (response) {
           // console.log("response", response);
            vm.Quotations = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.files = [];
            //console.log("vm.files", vm.files);
            FlashService.Success("Upload Saved Successfully!");
        });
        /* Upload.upload({
            url: 'upload/url',
            data: { file: file, 'username': $scope.username }
        }).then(function (resp) {
            // console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
        }, function (resp) {
            // console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            // console.log('progress: ' + progressPercentage + '% ');
        });*/
    };


    vm.email = function () {
        alert('email called');
    };
    vm.call = function () {
        alert('Call function called');
    };



    vm.cancel = function () {
        
        vm.showAddForm = false;
        if (vm.currentquotationid) {
            vm.filterquotations(vm.currentquotationid);
            vm.buttonvisible(true, true, false, true, false, false);
        } else {
            $scope.activeTab = "quotationsFeed";
        }
        
    }
    vm.cancelItem = function (index) {
        vm.Quotation.Items[index].readvalue = true;
        vm.readvalue = true;
        //vm.buttonvisible(true, true, false, true, false, false);
    }
    vm.undocancelItem = function (index) {
        vm.Quotation.Items[index].readvalue = false;
        //vm.buttonvisible(true, true, false, true, false, false);
    }
    vm.addNewActivity = function () {
        /*var newActivity = {};
        newActivity.ActivityID = vm.Quotation.Activities[vm.Quotation.Activities.length - 1].ActivityID + 1;
        newActivity.Name = 'Manish G';
        newActivity.Text = vm.newActivity_Text;
        newActivity.ProfilePic = 'img/profileimg.png';
        newActivity.EditActivities = false;
        vm.Quotation.Activities.push(newActivity);

        QuotationsService.update(vm.Quotation).then(function (response) {
            vm.Quotations = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newActivity_Text = "";
            FlashService.Success("Activity Saved Successfully!");
        });*/
        //console.log("vm.newActivity_Text", vm.newActivity_Text);
        //console.log("vm.Quotation", vm.Quotation);
        CRUDService.addNewActivity(vm.newActivity_Text, vm.Quotation, "Quotations").then(function (response) {
            //console.log("response", response);
            vm.Quotations = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newActivity_Text = "";
            FlashService.Success("Activity Saved Successfully!");
        });
    };
    vm.addNewReply = function () {
       /* var newReplyAdd = {};
        newReplyAdd.RepliesID = vm.Quotation.Replies[vm.Quotation.Replies.length - 1].RepliesID + 1;
        newReplyAdd.Comment = vm.reply.Text
        newReplyAdd.ProfilePic = "img/profileimg.png";
        newReplyAdd.Recepients = vm.reply.Recepients;
        newReplyAdd.CreationDate = "2016-08-16";
        newReplyAdd.ExcludeProspect = vm.reply.excludeProspect;
        newReplyAdd.OtherReplies = [];
        vm.Quotation.Replies.push(newReplyAdd);

        QuotationsService.update(vm.Quotation).then(function (response) {
            vm.Quotations = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.reply = {};
            FlashService.Success("Replies Saved Successfully!");
        });*/
        CRUDService.addNewReply(vm.reply, vm.Quotation, "Quotations").then(function (response) {
            //console.log("response", response);
            vm.Quotations = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.reply = {};
            FlashService.Success("Replies Saved Successfully!");
        });
    };
    vm.addsubreply = function (index) {
       /* var subReplyAdd = {};
        if (vm.Quotation.Replies[index].OtherReplies.length == undefined || vm.Quotation.Replies[index].OtherReplies.length == 0) {
            subReplyAdd.OtherRepliesID = 1;
        }
        else {
            subReplyAdd.OtherRepliesID = vm.Quotation.Replies[index].OtherReplies[vm.Quotation.Replies[index].OtherReplies.length - 1].OtherRepliesID + 1;
        }
        subReplyAdd.Comment = vm.subreply.Text
        subReplyAdd.ProfilePic = "img/profileimg.png";
        subReplyAdd.Recepients = vm.subreply.Recepients;
        subReplyAdd.CreationDate = "2016-08-16";
        subReplyAdd.ExcludeProspect = false;
        vm.Quotation.Replies[index].OtherReplies.push(subReplyAdd);
        

        QuotationsService.update(vm.Quotation).then(function (response) {
            vm.Quotations = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.subreply = {};
            vm.subreply.Recepients = '';
            FlashService.Success("Replies Saved Successfully!");
        });*/
        CRUDService.addsubreply(index, vm.subreply, vm.Quotation, "Quotations").then(function (response) {
            //console.log("response", response);
            vm.Quotations = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.subreply = {};
            vm.subreply.Recepients = '';
            FlashService.Success("Replies Saved Successfully!");
        });
    };
    vm.addNewEvents = function () {
       /* var newEvents = vm.newEvent;
        if (vm.newEvent.EventsID == undefined) {
            newEvents.EventsID = vm.Quotation.Events[vm.Quotation.Events.length - 1].EventsID + 1;
        }

        newEvents.EditActivities = false;
        vm.Quotation.Events.push(newEvents);

        QuotationsService.update(vm.Quotation).then(function (response) {
            vm.Quotations = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newEvent = {};
            FlashService.Success("Events Saved Successfully!");
        });*/
        CRUDService.addNewEvents(vm.newEvent, vm.isEditEventForm, vm.Quotation, "Quotations").then(function (response) {
            //console.log("response", response);
            vm.Quotations = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newEvent = {};
            FlashService.Success("Events Saved Successfully!");
        });
       /* vm.showCalendarEdit = false;
        vm.event_detail = true;
        vm.isEditEventForm == false;
        vm.buttonvisible(true, false, false, false, false, false);*/
        if (vm.isEditEventForm == true) {
            vm.showCalendarReadOnly = true;
            vm.event_detail = false;
            vm.isEditEventForm = false;
            vm.buttonvisible(true, false, true, true, false, false);
        } else {
            vm.event_detail = true;
            vm.buttonvisible(true, false, false, false, false, false);
        }

        vm.showCalendarEdit = false;
        vm.buttonvisible(true, false, true, true, false, false);

    };
    vm.addNewNotes = function () {
       /* var newNotes = {};
        newNotes.NotesID = vm.Quotation.Notes[vm.Quotation.Notes.length - 1].NotesID + 1;
        newNotes.Name = 'Manish G';
        newNotes.Text = vm.newNotes_Text;
        newNotes.ProfilePic = 'img/profileimg.png';
        newNotes.EditActivities = false;
        vm.Quotation.Notes.push(newNotes);

        QuotationsService.update(vm.Quotation).then(function (response) {
            vm.Quotations = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newNotes_Text = "";
            FlashService.Success("Notes Saved Successfully!");
        });*/
        CRUDService.addNewNotes(vm.newNotes_Text, vm.Quotation, "Quotations").then(function (response) {
            //console.log("response", response);
            vm.Quotations = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newNotes_Text = "";
            FlashService.Success("Notes Saved Successfully!");
        });
    };
    vm.save = function () {
       /*vm.showAddForm = false;
        
        if (vm.isEditForm == true) {
            QuotationsService.update(vm.Quotation).then(function (response) {
                vm.Quotations = response;
                if ($scope.subActiveTab == 'details') {
                    vm.buttonvisible(false, true, false, true, false, false);
                }
                FlashService.Success("Saved Successfully!");
            });
        }
        else {
            QuotationsService.insert(vm.Quotation).then(function (response) {
                vm.Quotations = response;
                if ($scope.subActiveTab == 'details') {
                    vm.buttonvisible(false, true, false, true, false, false);
                    $scope.activeTab = 'leadsFeed';
                }
                FlashService.Success("Saved Successfully!");
            });
        }*/
        vm.showAddForm = false;
        vm.Quotation.AccountId = vm.Quotation.AccountName;
        if (vm.isEditForm == true) {
            CRUDService.update(vm.Quotation, "Quotations").then(function (response) {
                vm.Quotations = response;
                //console.log("vm.Quotations", vm.Quotations);
                if ($scope.subActiveTab == 'details') {
                    // vm.buttonvisible(false, true, false, true, false, false);
                    vm.buttonvisible(true, true, false, true, false, false);
                }
                FlashService.Success("Saved Successfully!");
            });
        }
        else {
            vm.Quotation.Id = vm.Quotations.length + 1;
            vm.Quotation.CustomerId = "";
            vm.Quotation.PricingModelId = "";
            vm.Quotation.Items = [];
            vm.Quotation.Replies = [];
            vm.Quotation.Uploads = [];
            vm.Quotation.Notes = [];
            vm.Quotation.Events = [];
            vm.Quotation.Activities = [];
            CRUDService.insert(vm.Quotation, "Quotations").then(function (response) {
                vm.Quotations = response;
                if ($scope.subActiveTab == 'details') {
                    //vm.buttonvisible(false, true, false, true, false, false);
                    vm.buttonvisible(true, true, false, true, false, false);
                    $scope.activeTab = 'quotationsFeed';
                }
                vm.Quotations = [];
                vm.Quotations = CRUDService.getStorageQuotations();
                //console.log("vm.Quotations", vm.Quotations);
                FlashService.Success("Saved Successfully!");
            });
        }
        
    }

    vm.add = function () {
            
            vm.LastQuotation = vm.Quotation;
            vm.Quotation = {};
            vm.showAddForm = true;
        //vm.buttonvisible(false, true, true, false, true, false);
            vm.buttonvisible(false, false, true, false, false, true);
    }
    vm.addevent = function () {
        //console.log(" vm.Lead", vm.Lead);
        vm.newEvent = {
            isRecurrence: false
        };
        vm.showCalendarEdit = true;
        vm.event_detail = false;
        vm.buttonvisible(false, false, true, true, false, false);
    }
    vm.delete = function () {
        if (confirm("Delete this quotation?")) {

            QuotationsService.delete(vm.currentquotationid).then(function (response) {
                vm.Quotations = response;
                if ($scope.subActiveTab == 'details') {
                    vm.buttonvisible(false, true, false, true, false, false);
                }
            });
            FlashService.Success("Deleted Successfully!");
        }
        //$scope.activeTab = 'quotationsFeed';
        $scope.activeTab = 'quotationsDetail';
        vm.buttonvisible(false, true, false, true, false, false);
    };

    vm.edit = function () {
       
        vm.showAddForm = true;
        vm.isEditForm = true;
        vm.filterquotations(vm.currentquotationid);
        vm.showitemsave = true;
        vm.showitemcancel = true;
        vm.firstvalue = 1;
        vm.buttonvisible(false, false, true, false, true, false);
    }
    vm.editevent = function () {
        vm.showCalendarReadOnly = false;
        vm.showCalendarEdit = true;
        vm.isEditEventForm = true;
        vm.filterevents(vm.currenteventid);
        //console.log("vm.newEvent", vm.newEvent);
        vm.buttonvisible(false, false, true, true, false, false);
    }
    vm.filterquotations = function (id) {
        vm.Quotation = vm.Quotations.filter(function (v) {
            return v.Id === id; // Filter out the appropriate one
        })[0];
        if (vm.Quotation && vm.Quotation.hasOwnProperty('CreationDate') && vm.Quotation.CreationDate != "" && vm.Quotation.hasOwnProperty('EstimateDate') && vm.Quotation.EstimateDate != "" && vm.Quotation.hasOwnProperty('Phone') && vm.Quotation.Phone != "" && vm.Quotation.hasOwnProperty('ReferredNumber') && vm.Quotation.ReferredNumber != "") {
            vm.Quotation.CreationDate = new Date(vm.Quotation.CreationDate);
            vm.Quotation.EstimateDate = new Date(vm.Quotation.EstimateDate);
            vm.Quotation.Phone = parseInt(vm.Quotation.Phone);
            vm.Quotation.ReferredNumber = parseInt(vm.Quotation.ReferredNumber);
        }
        if (vm.Quotation && vm.Quotation.hasOwnProperty('Items') && vm.Quotation.Items != "") {
            //console.log("vm.Quotation.Items.length", vm.Quotation.Items.length);
            for (i = 0; i < vm.Quotation.Items.length; i++) {
                vm.Quotation.Items[i].readvalue = false;
            }
        }
        
    }
    vm.filterevents = function (id) {
        //console.log("vm.Quotation", vm.Quotation);
        //console.log("vm.Quotation.Events", vm.Quotation.Events);
        vm.Calendar = vm.Quotation.Events.filter(function (v) {
            return v.EventsID === id; // Filter out the appropriate one
        })[0];
        vm.newEvent = vm.Calendar;
    }
    vm.showquotationsdetail = function (id, event) {
		
        $scope.activeTab = 'quotationsDetail';
        $scope.subActiveTab = 'details'
        vm.filterquotations(id);
        vm.currentquotationid = id;
        vm.showAddForm = false;

        //vm.buttonvisible(true, true, false, true, false, false);
        vm.buttonvisible(false, true, false, true, false, false);
    }
    vm.showeventdetail = function (id, event) {
        vm.filterevents(id);
        vm.currenteventid = id;
        //console.log(" vm.filterevents(id)", vm.filterevents(id));
        //console.log(" vm.currenteventid", vm.currenteventid);
        vm.showCalendarReadOnly = true;
        vm.event_detail = false;
        vm.buttonvisible(true, true, false, true, false, false);
    }
    vm.close = function () {
        vm.showCalendarReadOnly = true;
        vm.showCalendarEdit = false;
        vm.event_detail = false;
        vm.buttonvisible(true, true, false, true, false, false);
    }
    vm.addNewQuotations = function (id, event) {
        $scope.activeTab = 'quotationsDetail';
        $scope.subActiveTab = 'details'
        vm.filterquotations(id);
        vm.currentquotationid = id;
        vm.LastQuotation = vm.Quotation;
        vm.Quotation = {};
        vm.showAddForm = true;
        vm.buttonvisible(false, false, true, false, true, false);
        //vm.buttonvisible(false, false, true, false, false, true);
    }

    $scope.searchObject = {};
    $scope.$on('advanced-searchbox:addedSearchParam', function (event, searchParameter) {
        ///
    });

    $scope.$on('advanced-searchbox:removedSearchParam', function (event, searchParameter) {
        /// Need to call ajax if tags is removed
        //console.log(searchParameter.key, " is removed", searchParameter);
    });

    $scope.$on('advanced-searchbox:removedAllSearchParam', function (event) {
        ///
    });

    $scope.$on('advanced-searchbox:enteredEditMode', function (event, searchParameter) {
        ///
    });

    $scope.$on('advanced-searchbox:leavedEditMode', function (event, searchParameter) {
        ///
    });

    $scope.$on('advanced-searchbox:modelUpdated', function (event, model) {
        ///
    });

    $scope.$on('advanced-searchbox:searchButtonClicked', function (event, model) {
        ///Get results with filters after user clicking submit button
        $scope.searchObject = model;
        model.type = "Quotations";
        model.object = vm.Quotations;
       // console.log("model", model);
        CRUDService.searchvalue(vm.Quotations, model.type, model).then(function (response) {
            //console.log("response", response);
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
        });

    });

});