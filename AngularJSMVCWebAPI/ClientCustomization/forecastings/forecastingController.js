﻿module.controller('forecastingController', function ($scope, $rootScope, Upload, $timeout, ForecastsService, FlashService, CRUDService) {

    var vm = this;
    $scope.showiconadd = true;
    $scope.showiconsearch = true;
    $scope.activeiconadd = false;
    $scope.activeiconsearch = false;
    $scope.activeTab = 'forecastsFeed';
    $scope.subActiveTab = 'details';
    vm.Forecast = null;
    vm.LastForecast;
    vm.showAddForm = false;
    vm.isEditForm = false;
    vm.currentforecastid = -1;
    vm.event_detail = true;
    vm.message = null;
    vm.showadd = true;
    vm.showedit = true;
    vm.showsave = false;
    vm.showdelete = true;
    vm.showcancel = false;
    vm.showitemsave = false;
    vm.showitemcancel = false;
   
    vm.showback = false;
    vm.addNewEvent = false;
    vm.newActivity_Text = "";
    vm.newNotes_Text = "";
	vm.searchVisible = false;
    $rootScope.dashboardTitle = "Forecasts Feed";
    $rootScope.dashboardMsg = "Overview of all the Forecasts";
    vm.newEvent = {
        isRecurrence: false
    };
    vm.deleteItem = false;
   
    $scope.availableSearchParams = [
        { key: "Subject", name: "Subject", placeholder: "Subject..." },
        { key: "Contact", name: "Contact", placeholder: "Contact..." },
        { key: "Phone", name: "Phone", placeholder: "Phone..." },
        { key: "emailAddress", name: "E-Mail", placeholder: "E-Mail..." },
        { key: "Date", name: "Date", placeholder: "Date...", type: "date" }
       //{ key: "SelectBox", name: "SelectBox", placeholder: "SelectBox...", type: "select", suggestedValues: ['test1', 'test2', 'test3'] }
    ];

    $scope.eventsSearchParams = [
        { key: "Search Param 1", name: "Search Param 1", placeholder: "Search Param 1..." },
        { key: "Search Param 2", name: "Search Param 2", placeholder: "Search Param 2..." }
    ];

    $scope.notesSearchParams = [
        { key: "Search Param 1", name: "Search Param 1", placeholder: "Search Param 1..." },
        { key: "Search Param 2", name: "Search Param 2", placeholder: "Search Param 2..." }
    ];

	
	$scope.startSearch = function ()
    {

        $scope.searchVisible = true;
    }

    $scope.hideSearch = function () {

        $scope.searchVisible = false;
    }
	
    vm.buttonvisible = function (showadd, showedit, showsave, showdelete, showcancel, showback) {

        vm.showadd = showadd;
        vm.showedit = showedit;
        vm.showsave = showsave;
        vm.showdelete = showdelete;
        vm.showcancel = showcancel;
        vm.showback = showback;
    };

    $scope.changeWithOutSubTabs = function (iconName) {

        $scope.activeiconadd = false;
        $scope.activeiconsearch = false;

        if (iconName === 'add') {
            $scope.activeiconadd = true;
        }

        if (iconName === 'search') {
            $scope.activeiconsearch = true;
        }

    };

    $scope.changeSubTabs = function (tabName) {
        $scope.subActiveTab = tabName;
        if (tabName === 'details') {
            $rootScope.dashboardTitle = "Forecasts Details";
            //vm.buttonvisible(true, true, false, true, false, false);
            vm.buttonvisible(false, true, false, true, false, false);
        }
        else if (tabName === 'events') {
            vm.isEditForm = true;
            vm.buttonvisible(true, false, false, false, false, false);
        }
        else {
            vm.isEditForm = true;
            vm.buttonvisible(false, false, false, false, false, false);
           
        }

        if (tabName === 'posts') {
            $rootScope.dashboardTitle = "Activities Details";
        }
        if (tabName === 'events') {
            vm.showCalendarReadOnly = false;
            vm.showCalendarEdit = false;
            vm.event_detail = true;
            $rootScope.dashboardTitle = "Events Details";
        }
        if (tabName === 'notes') {
            $rootScope.dashboardTitle = "Notes Details";
        }
        if (tabName === 'uploads') {
            $rootScope.dashboardTitle = "Uploads Details";
        }
        if (tabName === 'replies') {
            $rootScope.dashboardTitle = "Replies Details";
        }
    };

    vm.getall = function () {
        ForecastsService.getall()
        .then(function (response) {
            vm.Forecasts = response;
            //console.log(response);
            // console.log(JSON.stringify(response));
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getall();

    vm.getActivities = function () {
        ForecastsService.getActivityType()
        .then(function (response) {
            vm.ActivityType = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getActivities();

    vm.getAcountName = function () {
        ForecastsService.getAcountName()
        .then(function (response) {
            vm.AcountName = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getAcountName();

    vm.getLeadContact = function () {
        ForecastsService.getLeadContact()
        .then(function (response) {
            vm.LeadContact = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getLeadContact();

    vm.getLeadSource = function () {
        ForecastsService.getLeadSource()
        .then(function (response) {
            vm.LeadSource = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getLeadSource();

    vm.getSalesStage = function () {
        ForecastsService.getSalesStage()
        .then(function (response) {
            vm.SalesStage = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getSalesStage();

    vm.getSalesman = function () {
        ForecastsService.getSalesman()
        .then(function (response) {
            vm.Salesman = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getSalesman();

    vm.getReferredBy = function () {
        ForecastsService.getReferredBy()
        .then(function (response) {
            vm.ReferredBy = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getReferredBy();



    $scope.$watch('files', function () {
        // $scope.upload($scope.files);
    });
    $scope.$watch('file', function () {
        if ($scope.file != null) {
            $scope.files = [$scope.file];
        }
    });
    vm.deleteItems = function (id) {
        //console.log("id", id);
        for (var i = 0; i < vm.Forecast.Items.length; i++) {
            if ((vm.Forecast.Items != null) && (vm.Forecast.Items[i].ItemId == id)) {
                vm.Forecast.Items.splice(i, 1);
                CRUDService.update(vm.Forecast, "Forecasts").then(function (response) {
                    vm.Forecasts = response;
                    if ($scope.subActiveTab == 'details') {
                        vm.buttonvisible(true, true, false, true, false, false);
                    }
                    FlashService.Success("Updated Successfully!");
                });
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
        $scope.activeTab = 'forecastsDetail';
        vm.buttonvisible(false, true, false, true, false, false);
    };
    vm.deleteActivities = function (id) {
        for (var i = 0; i < vm.Forecast.Activities.length; i++) {
            if ((vm.Forecast.Activities != null) && (vm.Forecast.Activities[i].ActivityID == id)) {
                vm.Forecast.Activities.splice(i, 1);
                vm.save();
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
    };
    vm.deleteEvents = function (id) {
        for (var i = 0; i < vm.Forecast.Events.length; i++) {
            if ((vm.Forecast.Events != null) && (vm.Forecast.Events[i].EventsID == id)) {
                vm.Forecast.Events.splice(i, 1);
                vm.save();
                break;
            }
        }
        vm.showCalendarReadOnly = false;
        vm.showCalendarEdit = false;
        vm.event_detail = true;
        FlashService.Success("Deleted Successfully");
    };
    vm.deleteNotes = function (id) {
        for (var i = 0; i < vm.Forecast.Notes.length; i++) {
            if ((vm.Forecast.Notes != null) && (vm.Forecast.Notes[i].NotesID == id)) {
                vm.Forecast.Notes.splice(i, 1);
                vm.save();
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
    };
    vm.deleteUploads = function (id) {
        for (var i = 0; i < vm.Forecast.Uploads.length; i++) {
            if ((vm.Forecast.Uploads != null) && (vm.Forecast.Uploads[i].UploadsID == id)) {
                vm.Forecast.Uploads.splice(i, 1);
                vm.save();
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
    };
    vm.editEvents = function (id) {
        vm.addNewEvent = !vm.addNewEvent;
        for (var i = 0; i < vm.Forecast.Events.length; i++) {
            if ((vm.Forecast.Events != null) && (vm.Forecast.Events[i].EventsID == id)) {
                vm.newEvent = vm.Forecast.Events[i];
                break;
            }
        }
    };
    // upload on file select or drop
    vm.upload = function (file) {
        CRUDService.addUpload(vm.Forecast, "Forecasts", file).then(function (response) {
            //console.log("response", response);
            vm.Forecasts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.files = [];
            //console.log("vm.files", vm.files);
            FlashService.Success("Upload Saved Successfully!");
        });
        /*Upload.upload({
            url: 'upload/url',
            data: { file: file, 'username': $scope.username }
        }).then(function (resp) {
            // console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
        }, function (resp) {
            // console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            // console.log('progress: ' + progressPercentage + '% ');
        });*/
    };


    vm.email = function () {
        alert('email called');
    };
    vm.call = function () {
        alert('Call function called');
    };



    vm.cancel = function () {
        
        vm.showAddForm = false;
        if (vm.currentforecastid) {
            vm.filterforecasts(vm.currentforecastid);
            vm.buttonvisible(true, true, false, true, false, false);
        } else {
            $scope.activeTab = "forecastsFeed";
        }
        
        //vm.buttonvisible(true, true, false, true, false, false);
    }
    vm.cancelItem = function (index) {
        vm.Forecast.Items[index].readvalue = true;
        vm.readvalue = true;
        //vm.buttonvisible(true, true, false, true, false, false);
    }
    vm.undocancelItem = function (index) {
        vm.Forecast.Items[index].readvalue = false;
        //vm.buttonvisible(true, true, false, true, false, false);
    }
    vm.addNewActivity = function () {
       /*var newActivity = {};
        newActivity.ActivityID = vm.Forecast.Activities[vm.Forecast.Activities.length - 1].ActivityID + 1;
        newActivity.Name = 'Manish G';
        newActivity.Text = vm.newActivity_Text;
        newActivity.ProfilePic = 'img/profileimg.png';
        newActivity.EditActivities = false;
        vm.Forecast.Activities.push(newActivity);

        ForecastsService.update(vm.Forecast).then(function (response) {
            vm.Forecasts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newActivity_Text = "";
            FlashService.Success("Activity Saved Successfully!");
        });*/
        CRUDService.addNewActivity(vm.newActivity_Text, vm.Forecast, "Forecasts").then(function (response) {
            //console.log("response", response);
            vm.Forecasts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newActivity_Text = "";
            FlashService.Success("Activity Saved Successfully!");
        });
    };
    vm.addNewReply = function () {
       /*var newReplyAdd = {};
        newReplyAdd.RepliesID = vm.Forecast.Replies[vm.Forecast.Replies.length - 1].RepliesID + 1;
        newReplyAdd.Comment = vm.reply.Text
        newReplyAdd.ProfilePic = "img/profileimg.png";
        newReplyAdd.Recepients = vm.reply.Recepients;
        newReplyAdd.CreationDate = "2016-08-16";
        newReplyAdd.ExcludeProspect = vm.reply.excludeProspect;
        newReplyAdd.OtherReplies = [];
        vm.Forecast.Replies.push(newReplyAdd);

        ForecastsService.update(vm.Forecast).then(function (response) {
            vm.Forecasts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.reply = {};
            FlashService.Success("Replies Saved Successfully!");
        });*/
        CRUDService.addNewReply(vm.reply, vm.Forecast, "Forecasts").then(function (response) {
            //console.log("response", response);
            vm.Forecasts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.reply = {};
            FlashService.Success("Replies Saved Successfully!");
        });
    };
    vm.addsubreply = function (index) {
        /*var subReplyAdd = {};
        if (vm.Forecast.Replies[index].OtherReplies.length == undefined || vm.Forecast.Replies[index].OtherReplies.length == 0) {
            subReplyAdd.OtherRepliesID = 1;
        }
        else {
            subReplyAdd.OtherRepliesID = vm.Forecast.Replies[index].OtherReplies[vm.Forecast.Replies[index].OtherReplies.length - 1].OtherRepliesID + 1;
        }
        subReplyAdd.Comment = vm.subreply.Text
        subReplyAdd.ProfilePic = "img/profileimg.png";
        subReplyAdd.Recepients = vm.subreply.Recepients;
        subReplyAdd.CreationDate = "2016-08-16";
        subReplyAdd.ExcludeProspect = false;
        vm.Forecast.Replies[index].OtherReplies.push(subReplyAdd);
        

        ForecastsService.update(vm.Forecast).then(function (response) {
            vm.Forecasts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.subreply = {};
            vm.subreply.Recepients = '';
            FlashService.Success("Replies Saved Successfully!");
        });*/
        CRUDService.addsubreply(index, vm.subreply, vm.Forecast, "Forecasts").then(function (response) {
            //console.log("response", response);
            vm.Forecasts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.subreply = {};
            vm.subreply.Recepients = '';
            FlashService.Success("Replies Saved Successfully!");
        });
    };
    vm.addNewEvents = function () {
        /*var newEvents = vm.newEvent;
        if (vm.newEvent.EventsID == undefined) {
            newEvents.EventsID = vm.Forecast.Events[vm.Forecast.Events.length - 1].EventsID + 1;
        }

        newEvents.EditActivities = false;
        vm.Forecast.Events.push(newEvents);

        ForecastsService.update(vm.Forecast).then(function (response) {
            vm.Forecasts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newEvent = {};
            FlashService.Success("Events Saved Successfully!");
        });*/
        CRUDService.addNewEvents(vm.newEvent, vm.isEditEventForm, vm.Forecast, "Forecasts").then(function (response) {
            //console.log("response", response);
            vm.Forecasts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newEvent = {};
            FlashService.Success("Events Saved Successfully!");
        });
        if (vm.isEditEventForm == true) {
            vm.showCalendarReadOnly = true;
            vm.event_detail = false;
            vm.isEditEventForm = false;
            vm.buttonvisible(true, false, true, true, false, false);
        } else {
            vm.event_detail = true;
            vm.buttonvisible(true, false, false, false, false, false);
        }

        vm.showCalendarEdit = false;
        vm.buttonvisible(true, false, true, true, false, false);

    };
    vm.addNewNotes = function () {
       /*var newNotes = {};
        newNotes.NotesID = vm.Forecast.Notes[vm.Forecast.Notes.length - 1].NotesID + 1;
        newNotes.Name = 'Manish G';
        newNotes.Text = vm.newNotes_Text;
        newNotes.ProfilePic = 'img/profileimg.png';
        newNotes.EditActivities = false;
        vm.Forecast.Notes.push(newNotes);

        ForecastsService.update(vm.Forecast).then(function (response) {
            vm.Forecasts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newNotes_Text = "";
            FlashService.Success("Notes Saved Successfully!");
        });*/
        CRUDService.addNewNotes(vm.newNotes_Text, vm.Forecast, "Forecasts").then(function (response) {
            //console.log("response", response);
            vm.Forecasts = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newNotes_Text = "";
            FlashService.Success("Notes Saved Successfully!");
        });
    };
    vm.save = function () {
       /* vm.showAddForm = false;
        
        if (vm.isEditForm == true) {
            ForecastsService.update(vm.Forecast).then(function (response) {
                vm.Forecasts = response;
                if ($scope.subActiveTab == 'details') {
                    vm.buttonvisible(false, true, false, true, false, false);
                }
                FlashService.Success("Saved Successfully!");
            });
        }
        else {
            ForecastsService.insert(vm.Forecast).then(function (response) {
                vm.Forecasts = response;
                if ($scope.subActiveTab == 'details') {
                    vm.buttonvisible(false, true, false, true, false, false);
                    $scope.activeTab = 'forecastsFeed';
                }
                FlashService.Success("Saved Successfully!");
            });
        }*/
        vm.showAddForm = false;
        //console.log("vm.Forecast", vm.Forecast);
        if (vm.isEditForm == true) {
            CRUDService.update(vm.Forecast, "Forecasts").then(function (response) {
                vm.Forecasts = response;
                if ($scope.subActiveTab == 'details') {
                    //vm.buttonvisible(false, true, false, true, false, false);
                    vm.buttonvisible(true, true, false, true, false, false);
                }
                FlashService.Success("Saved Successfully!");
            });
        }
        else {
            vm.Forecast.Id = vm.Forecasts.length + 1;
            vm.Forecast.Activities = [];
            vm.Forecast.Events = [];
            vm.Forecast.Notes = [];
            vm.Forecast.Uploads = [];
            vm.Forecast.Replies = [];
            vm.Forecast.Items = [];
            CRUDService.insert(vm.Forecast, "Forecasts").then(function (response) {
                vm.Forecasts = response;
                if ($scope.subActiveTab == 'details') {
                    //vm.buttonvisible(false, true, false, true, false, false);
                    vm.buttonvisible(true, true, false, true, false, false);
                    $scope.activeTab = 'forecastsFeed';
                }
                vm.Forecasts = [];
                vm.Forecasts = CRUDService.getStorageForecasts();
                //console.log(vm.Forecasts);
                FlashService.Success("Saved Successfully!");
            });
        }
       
    }

    vm.add = function () {
           
            vm.LastForecast = vm.Forecast;
            vm.Forecast = {};
            vm.showAddForm = true;
        //vm.buttonvisible(false, true, true, false, true, false);
            vm.buttonvisible(false, false, true, false, false, true);
    }
    vm.addevent = function () {
        //console.log(" vm.Lead", vm.Lead);
        vm.newEvent = {
            isRecurrence: false
        };
        vm.showCalendarEdit = true;
        vm.event_detail = false;
        vm.buttonvisible(false, false, true, true, false, false);
    }
    vm.delete = function () {
        if (confirm("Delete this forecast?")) {

            ForecastsService.delete(vm.currentforecastid).then(function (response) {
                vm.Forecasts = response;
                if ($scope.subActiveTab == 'details') {
                    vm.buttonvisible(false, true, false, true, false, false);
                }
            });
            vm.Forecasts = [];
            vm.Forecasts = CRUDService.getStorageForecasts();
            FlashService.Success("Deleted Successfully!");
        }
        //$scope.activeTab = 'forecastsFeed';
        $scope.activeTab = 'forecastsDetail';
        vm.buttonvisible(false, true, false, true, false, false);
    };

    vm.edit = function () {
       
        vm.showAddForm = true;
        vm.isEditForm = true;
        vm.filterforecasts(vm.currentforecastid);
        vm.showitemsave = true;
        vm.showitemcancel = true;
        vm.firstvalue = 1;
        vm.buttonvisible(false, false, true, false, true, false);
    }
    vm.editevent = function () {
        vm.showCalendarReadOnly = false;
        vm.showCalendarEdit = true;
        vm.isEditEventForm = true;
        vm.filterevents(vm.currenteventid);
        //console.log("vm.newEvent", vm.newEvent);
        vm.buttonvisible(false, false, true, true, false, false);
    }
    vm.filterforecasts = function (id) {
        vm.Forecast = vm.Forecasts.filter(function (v) {
            return v.Id === id; // Filter out the appropriate one
        })[0];
        if (vm.Forecast && vm.Forecast.hasOwnProperty('CreationDate') && vm.Forecast.CreationDate != "") {
            vm.Forecast.CreationDate = new Date(vm.Forecast.CreationDate);
        }
        if( vm.Forecast && vm.Forecast.hasOwnProperty('StartDate') && vm.Forecast.StartDate != ""){
            vm.Forecast.StartDate = new Date(vm.Forecast.StartDate);
        }
        if (vm.Forecast  && vm.Forecast.hasOwnProperty('EndDate') && vm.Forecast.EndDate != "" ) {
            vm.Forecast.EndDate = new Date(vm.Forecast.EndDate);
        }
        if( vm.Forecast && vm.Forecast.hasOwnProperty('TargetPeriod') && vm.Forecast.TargetPeriod != "" ){
            vm.Forecast.TargetPeriod = new Date(vm.Forecast.TargetPeriod);
        }
        if (vm.Forecast  && vm.Forecast.hasOwnProperty('Phone') && vm.Forecast.Phone != "") {
            vm.Forecast.Phone = parseInt(vm.Forecast.Phone);
        }
        
        if (vm.Forecast && vm.Forecast.hasOwnProperty('Items') && vm.Forecast.Items != "") {
            //console.log("vm.Forecast.Items.length", vm.Forecast.Items.length);
            for (i = 0; i < vm.Forecast.Items.length; i++) {
                vm.Forecast.Items[i].readvalue = false;
            }
        }
        
    }
    vm.filterevents = function (id) {
       // console.log("vm.Forecast", vm.Forecast);
        //console.log("vm.Forecast.Events", vm.Forecast.Events);
        vm.Calendar = vm.Forecast.Events.filter(function (v) {
            return v.EventsID === id; // Filter out the appropriate one
        })[0];
        vm.newEvent = vm.Calendar;
    }
    vm.showforecastdetail = function (id, event) {
        $scope.activeTab = 'forecastsDetail';
        $scope.subActiveTab = 'details'
        vm.filterforecasts(id);
        vm.currentforecastid = id;
        vm.showAddForm = false;

        //vm.buttonvisible(true, true, false, true, false, false);
        vm.buttonvisible(false, true, false, true, false, false);
    }
    vm.showeventdetail = function (id, event) {
        vm.filterevents(id);
        vm.currenteventid = id;
        //console.log(" vm.filterevents(id)", vm.filterevents(id));
        //console.log(" vm.currenteventid", vm.currenteventid);
        vm.showCalendarReadOnly = true;
        vm.event_detail = false;
        vm.buttonvisible(true, true, false, true, false, false);
    }
    vm.close = function () {
        vm.showCalendarReadOnly = true;
        vm.showCalendarEdit = false;
        vm.event_detail = false;
        vm.buttonvisible(true, true, false, true, false, false);
    }
    vm.addNewForecast = function (id, event) {
        $scope.activeTab = 'forecastsDetail';
        $scope.subActiveTab = 'details'
        vm.filterforecasts(id);
        vm.currentforecastid = id;
        vm.LastForecast = vm.Forecast;
        vm.Forecast = {};
        vm.showAddForm = true;
        vm.buttonvisible(false, false, true, false, true, false);
        //vm.buttonvisible(false, false, true, false, false, true);
    }

    $scope.searchObject = {};
    $scope.$on('advanced-searchbox:addedSearchParam', function (event, searchParameter) {
        ///
    });

    $scope.$on('advanced-searchbox:removedSearchParam', function (event, searchParameter) {
        /// Need to call ajax if tags is removed
        //console.log(searchParameter.key, " is removed", searchParameter);
    });

    $scope.$on('advanced-searchbox:removedAllSearchParam', function (event) {
        ///
    });

    $scope.$on('advanced-searchbox:enteredEditMode', function (event, searchParameter) {
        ///
    });

    $scope.$on('advanced-searchbox:leavedEditMode', function (event, searchParameter) {
        ///
    });

    $scope.$on('advanced-searchbox:modelUpdated', function (event, model) {
        ///
    });

    $scope.$on('advanced-searchbox:searchButtonClicked', function (event, model) {
        ///Get results with filters after user clicking submit button
        $scope.searchObject = model;
        model.type = "Forecasts";
        model.object = vm.Forecasts;
        //console.log("model", model);
        CRUDService.searchvalue(vm.Forecasts, model.type, model).then(function (response) {
            //console.log("response", response);
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
        });

    });

});