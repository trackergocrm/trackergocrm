﻿module.controller('CalendarController', function ($rootScope,$scope, Upload, $timeout, CalenderService, FlashService) {


    $rootScope.dashboardTitle = "Calender Feed";
    $rootScope.dashboardMsg = "Overview of all the Calenders";



    var vm = this;
    vm.Calendar = {};

    //Date slider
    var today = new Date();
    vm.startOfWeek= moment().startOf('week');
    vm.endOfWeek = moment().endOf('week');
    var days = [];
    var day = vm.startOfWeek;

    vm.selectedDate;

    vm.showCalFeed = function () {

        vm.addNewEvent = false;

        vm.showCalendarFeed = true;

        vm.showCalendarSpinner = true;

        vm.showCalendarDetail = false;

        vm.showCalendarReadOnly = false;

        vm.showCalendarEdit = false;

        vm.showDetail = true;

        vm.showEdit = false;

        vm.showedit = false;

        vm.showadd = false;

        vm.showsave = false;

        vm.showcancel = false;

        vm.showback = false;

        vm.showdelete = true;

        vm.showaddevent = true;

        vm.addnew = false;

        vm.isYearActive = false;
        vm.newEvent = {
            isRecurrence: false
        };

    }
    

    vm.changeSelectedDate = function (date) {
        vm.showCalFeed();
        vm.selectedDate = date;
        vm.Calendars = [];
        CalenderService.getall().then(function (response) {
            vm.Calendars = [];
            for (var i = 0; i < response.length; i++) {
                if (date.getDate() == new Date(response[i].StartDate).getDate()) {
                    vm.Calendars.push(response[i])
                }
            }
            // console.log(JSON.stringify(response));
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    }

    while (day <= vm.endOfWeek) {
        var temp = day.toDate();
        if (today.getDate() == temp.getDate()) {
            vm.changeSelectedDate(today)
        }
        days.push({
            "object": temp,
        });
        day = day.clone().add(1, 'd');
    }

    vm.DateObjects = days;
    

    vm.decrementDate = function () {

        var yesterday = moment(vm.startOfWeek).subtract(1, 'day');
        var yesterday2 = moment(vm.endOfWeek).subtract(1, 'day');
        vm.startOfWeek = yesterday;
        vm.endOfWeek = yesterday2;
        
        vm.DateObjects.unshift({ 'object': yesterday.toDate() });
        vm.DateObjects.pop();
        vm.selectedDate = yesterday.toDate();

    }

    vm.incrementDate = function () {
        var tomorrow = moment(vm.endOfWeek).add(1, 'day');
        var tomorrow2 = moment(vm.startOfWeek).add(1, 'day');
        vm.endOfWeek = tomorrow;
        vm.startOfWeek = tomorrow2;
        vm.DateObjects.push({ 'object': tomorrow.toDate() });
        vm.DateObjects.shift();

        vm.selectedDate = tomorrow.toDate();
    }

    

    vm.SelectedYear = 2016;

    vm.Calendar.Years = ["2012", "2013", "2014", "2015", "2016"];

    vm.addNewEvent = false;

    vm.showCalendarFeed = true;

    vm.showCalendarSpinner = true;

    vm.showCalendarDetail = false;

    vm.showCalendarReadOnly = false;

    vm.showCalendarEdit = false;

    vm.showDetail = true;

    vm.showEdit = false;

    vm.showedit = false;

    vm.showadd = false;

    vm.showsave = false;
    
    vm.showcancel = false;

    vm.showback = false;

    vm.showdelete = true;

    vm.showaddevent = true;

    vm.addnew = false;

    vm.isYearActive = false;
    vm.newEvent = {
        isRecurrence: false
    };

    vm.getall = function () {
        CalenderService.getall()
        .then(function (response) {
            vm.Calendars = response;
       
             //console.log(JSON.stringify(response));
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    
    //vm.getall();

    /*vm.Calendar.Events =
    [
      { Name:"Name 1", Subject: "Subject 1", StartDate: "01-11-2016", EndDate: "01-11-2016", StartTime: "08:00 AM", EndTime: "10:00 AM", Description: "Description 1", EventType: "Event Type 1", EventSubType: "Event SubType 1", RecurrenceType: "Event Type 1", RecurrenceDays: "1" },
      { Name:"Name 2", Subject: "Subject 2", StartDate: "01-13-2016", EndDate: "01-13-2016", StartTime: "08:00 AM", EndTime: "10:00 AM", Description: "Description 2", EventType: "Event Type 2", EventSubType: "Event SubType 1", RecurrenceType: "Event Type 2", RecurrenceDays: "2" },
      { Name:"Name 3", Subject: "Subject 3", StartDate: "01-14-2016", EndDate: "01-14-2016", StartTime: "08:00 AM", EndTime: "10:00 AM", Description: "Description 3", EventType: "Event Type 3", EventSubType: "Event SubType 1", RecurrenceType: "Event Type 3", RecurrenceDays: "3" },
      { Name:"Name 4", Subject: "Subject 4", StartDate: "01-18-2016", EndDate: "01-18-2016", StartTime: "08:00 AM", EndTime: "10:00 AM", Description: "Description 4", EventType: "Event Type 4", EventSubType: "Event SubType 1", RecurrenceType: "Event Type 4", RecurrenceDays: "4" },
    ];*/

    vm.Init = function() {

    }
    vm.addNewEvents = function () {
        var newEvents = vm.newEvent;
        if (vm.isEditEventForm == true) {
            for (var i = 0; i < vm.Calendars.length; i++) {
                if ((vm.Calendars != null) && (vm.Calendars[i].EventsID == vm.currenteventid)) {
                    vm.Calendars[i] = vm.newEvent;
                    break;
                }
            }
            CalenderService.setStorageCalenders(vm.Calendars);
            vm.cancel();
            
            vm.isEditEventForm == false;
            vm.Calendars = [];
            CalenderService.getall().then(function (response) {
                vm.Calendars = [];
                for (var i = 0; i < response.length; i++) {
                    if (vm.selectedDate.getDate() == new Date(response[i].StartDate).getDate()) {
                        vm.Calendars.push(response[i])
                    }
                }
                // console.log(JSON.stringify(response));
            });
        }
        else {
            if (!vm.Calendars || (vm.Calendars && vm.Calendars.length == 0)) {
                newEvents.EventsID = 1;
                vm.Calendars = [];
            }
            else {
                newEvents.EventsID = ++(vm.Calendars[vm.Calendars.length - 1].EventsID);
            }
            newEvents.Title = newEvents.Subject;
            //console.log("newEvents", newEvents);
            //console.log("vm.Calendars", vm.Calendars);
            vm.Calendars = [];
            vm.Calendars = CalenderService.getStorageCalenders();
            vm.Calendars.push(newEvents);
            //console.log("vm.Calendars", vm.Calendars);
            CalenderService.setStorageCalenders(vm.Calendars);
            vm.cancel();
            vm.Calendars = [];
            CalenderService.getall().then(function (response) {
                vm.Calendars = [];
                for (var i = 0; i < response.length; i++) {
                    if (vm.selectedDate.getDate() == new Date(response[i].StartDate).getDate()) {
                        vm.Calendars.push(response[i])
                    }
                }
                // console.log(JSON.stringify(response));
            });
        }
    };
    vm.deleteEvents = function (id) {
        //console.log("id", id);
        //console.log("vm.Calendars.length", vm.Calendars.length);
        for (var i = 0; i < vm.Calendars.length; i++) {
            //console.log("vm.Calendars[i].EventsID", vm.Calendars[i].EventsID);
            if ((vm.Calendars != null) && (vm.Calendars[i].EventsID == id)) {
                vm.Calendars.splice(i, 1);
                CalenderService.setStorageCalenders(vm.Calendars);
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
    };
    vm.ShowCalendarDetail = function (id, a, event) {
        //console.log("id", id, "a", a, "event", event);
        vm.filterevents(id);
        vm.currenteventid = id;
        vm.Calendar = a;

        vm.showCalendarDetail = true;
        vm.addNewEvent = true;
        vm.showCalendarFeed = false;
        vm.showCalendarReadOnly = true;
        vm.showCalendarEdit = false;
        vm.showedit = true;
        vm.showadd = true;
        vm.showsave = false;
        vm.showcancel = false;
        vm.showback = true;
        vm.showdelete = true;
        vm.showCalendarSpinner = false;
        //vm.showaddevent = true;
        vm.showaddevent = false;

    }

    vm.CancelCalendarDetail = function () {

        vm.showCalendarDetail = false;
        vm.addNewEvent = false;
        vm.showCalendarFeed = true;
        vm.showCalendarReadOnly = false;
        vm.showCalendarEdit = false;
        vm.showedit = true;
        vm.showadd = true;
        vm.showsave = false;
        vm.showcancel = false;
        vm.showback = true;
        vm.showdelete = true;
        vm.showCalendarSpinner = false;
        vm.showaddevent = true;

    }

    vm.SetDay = function (year) {

        alert("Set Day");

        vm.SelectedYear = year;

        isYearActive = false;

    }

    vm.edit = function () {
        vm.isEditEventForm = true;
        vm.filterevents(vm.currenteventid);
        //console.log("vm.newEvent", vm.newEvent);
        vm.showCalendarDetail = true;
        vm.addNewEvent = false;
        vm.showCalendarFeed = false;
        vm.showCalendarReadOnly = false;
        vm.showCalendarEdit = true;
        vm.showedit = false;
        vm.showadd = false;
        vm.showsave = true;
        vm.showcancel = true;
        vm.showback = false;
        vm.showdelete = false;
        vm.showaddevent = false;    }

    vm.cancel = function () {

        if (vm.addnew)
        {
            vm.showCalendarDetail = false;
            vm.addNewEvent = true;
            vm.showCalendarFeed = true;
            vm.showCalendarReadOnly = false;
            vm.showCalendarEdit = false;
            vm.showedit = false;
            vm.showadd = false;
            vm.showsave = false;
            vm.showcancel = false;
            vm.showback = false;
            vm.showdelete = false;
            vm.showaddevent = true;
            vm.showCalendarSpinner = true;
        }
        else
        {
            vm.showCalendarDetail = true;
            vm.addNewEvent = true;
            vm.showCalendarFeed = false;
            vm.showCalendarReadOnly = true;
            vm.showCalendarEdit = false;
            vm.showedit = true;
            vm.showadd = true;
            vm.showsave = false;
            vm.showcancel = false;
            vm.showback = true;
            vm.showdelete = true;
            vm.showaddevent = true;
        }

    }
    vm.addevent = function () {
        //console.log(" vm.Calendars", vm.Calendars);
        vm.newEvent = {
            isRecurrence: false
        };
        vm.showCalendarEdit = true;
        vm.event_detail = false;

    }
    vm.filterevents = function (id) {
        //console.log("vm.Calendars", vm.Calendars);
        vm.Calendar = vm.Calendars.filter(function (v) {
            return v.EventsID === id; // Filter out the appropriate one
        })[0];
        
        if (vm.Calendar && vm.Calendar.hasOwnProperty('StartDate') && vm.Calendar.StartDate != '') {
            vm.Calendar.StartDate = new Date(vm.Calendar.StartDate);
        }
        if (vm.Calendar && vm.Calendar.hasOwnProperty('EndDate') && vm.Calendar.EndDate != '') {
            vm.Calendar.EndDate = new Date(vm.Calendar.EndDate);
        }
        if (vm.Calendar && vm.Calendar.hasOwnProperty('StartTime') && vm.Calendar.StartTime != '') {
            vm.Calendar.StartTime = new Date(vm.Calendar.StartTime);
        }
        if (vm.Calendar && vm.Calendar.hasOwnProperty('EndTime') && vm.Calendar.EndTime != '') {
            vm.Calendar.EndTime = new Date(vm.Calendar.EndTime);
        }
        vm.newEvent = vm.Calendar;
    }
    vm.back = function () {

        vm.showCalendarDetail = false;
        vm.addNewEvent = true;
        vm.showCalendarFeed = true;
        vm.showCalendarReadOnly = false;
        vm.showCalendarEdit = false;
        vm.showedit = false;
        vm.showadd = false;
        vm.showsave = false;
        vm.showcancel = false;
        vm.showback = false;
        vm.showdelete = false;
        vm.showCalendarSpinner = true;
        vm.showaddevent = true;

    }

    vm.add = function ()
    {
        vm.newEvent = {
            isRecurrence: false
        };
        vm.showCalendarDetail = true;
        vm.addNewEvent = false;
        vm.showCalendarFeed = false;
        vm.showCalendarReadOnly = false;
        vm.showCalendarEdit = true;
        vm.showedit = false;
        vm.showadd = false;
        vm.showsave = true;
        vm.showcancel = true;
        vm.showback = false;
        vm.showdelete = false;
        vm.showaddevent = false;
        vm.addnew = true;
    }

    vm.Init();

});

module.directive('isolate', function () {

    return {

        link: function (scope, element, attrs) {

            scope.isYearActive = false;

            element.click(function () {

                scope.isYearActive = true;

                $(element).addClass("yearactive");

                alert("GGGGGG");

            });
        }
        ,
        scope: true
    }

});

