﻿module.controller('SidebarController', function ($scope, $route) {
    $scope.showMenu = false;
    $scope.homemenubk = false;
    $scope.calendarmenubk = false;
    $scope.leadsmenubk = true;
    $scope.accountsmenubk = false;
    $scope.contactsmenubk = false;
    $scope.dashboardmenubk = false;
    $scope.configmenubk = false;
    $scope.productsmenubk = false;
    $scope.pricemenubk = false;
    $scope.quotationsmenubk = false;
    $scope.forecastingmenubk = false;

    $scope.flipshowMenu = function (menuitem) {

        $scope.homemenubk = false;
        $scope.calendarmenubk = false;
        $scope.leadsmenubk = false;
        $scope.accountsmenubk = false;
        $scope.contactsmenubk = false;
        $scope.dashboardmenubk = false;
        $scope.configmenubk = false;
        $scope.productsmenubk = false;
        $scope.pricemenubk = false;
        $scope.quotationsmenubk = false;
        $scope.forecastingmenubk = false;

        if (menuitem === 'Home')
        {
            $scope.homemenubk = true;
        }

        if (menuitem === 'Calendar')
        {
            $scope.calendarmenubk = true;
        }

        if (menuitem === 'Leads')
        {
            $scope.leadsmenubk = true;
        }

        if (menuitem === 'Accounts')
        {
            $scope.accountsmenubk = true;
        }

        if (menuitem === 'Contacts')
        {
            $scope.contactsmenubk = true;
        }

        if (menuitem === 'Dashboard')
        {
            $scope.dashboardmenubk = true;
        }

        if (menuitem === 'Config')
        {
            $scope.configmenubk = true;
        }

        if (menuitem === 'Products')
        {
            $scope.productsmenubk = true;
        }

        if (menuitem === 'Price')
        {
            $scope.pricemenubk = true;
        }

        if (menuitem === 'Quotations')
        {
            $scope.quotationsmenubk = true;
        }

        if (menuitem === 'Forecasting')
        {
            $scope.forecastingmenubk = true;
        }

        $scope.showMenu = !$scope.showMenu;

        //$scope.$apply();
        $route.reload();
    };
});