﻿

module.service('CalenderService', function ($http, $q) {

    //simply search contacts list for given id
    //and returns the contact object if found
    this.getall = function () {

        var deferred = $q.defer();
        var self = this;
        $http.get(Config.calenderUrl)
          .then(function (response) {
                if (!localStorage.Calenders) {
                self.setStorageCalenders(response.data);
                }
              return deferred.resolve(self.getStorageCalenders());

          }),
          function (response) {
              return response.Message;
          };

        return deferred.promise;

    }

    this.getStorageCalenders = function () {
        if (!localStorage.Calenders) {
            localStorage.Calenders = JSON.stringify([]);
        }

        return JSON.parse(localStorage.Calenders);
    }

    this.setStorageCalenders = function (calenders) {
        localStorage.Calenders = JSON.stringify(calenders);
    }

    

});