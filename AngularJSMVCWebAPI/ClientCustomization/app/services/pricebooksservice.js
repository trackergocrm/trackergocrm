﻿

module.service('PricebooksService', function ($http, $q) {
           
            //simply search contacts list for given id
            //and returns the contact object if found
            this.getall = function () {

                var deferred = $q.defer();
                var self = this;
                $http.get(Config.pricebooksUrl)
                  .then(function (response) {
                      if (!localStorage.Pricebooks) {
                          self.setStoragePricebooks(response.data);
                      }
                      return deferred.resolve(self.getStoragePricebooks());

                  }),
                  function (response) {
                     return response.Message;
                  };
                   
                 return deferred.promise;

            }
            this.getStoragePricebooks = function() {
                if (!localStorage.Pricebooks) {
                    localStorage.Pricebooks = JSON.stringify([]);
                }

                return JSON.parse(localStorage.Pricebooks);
            }

            this.setStoragePricebooks = function(pricebooks) {
                localStorage.Pricebooks = JSON.stringify(pricebooks);
            }

            this.getActivityType = function () {

                var deferred = $q.defer();

                $http.get(Config.activityTypeUrl)
                  .then(function (response) {
                      return deferred.resolve(response);
                  }),
                  function (response) {
                      return response.Message;
                  };

                return deferred.promise;

            }
            this.getAcountName = function () {

                var deferred = $q.defer();

                $http.get(Config.accountNameUrl)
                  .then(function (response) {
                      return deferred.resolve(response);
                  }),
                  function (response) {
                      return response.Message;
                  };

                return deferred.promise;

            }
            this.getLeadContact = function () {

                var deferred = $q.defer();

                $http.get(Config.leadContactUrl)
                  .then(function (response) {
                      return deferred.resolve(response);
                  }),
                  function (response) {
                      return response.Message;
                  };

                return deferred.promise;

            }
            this.getLeadSource = function () {

                var deferred = $q.defer();

                $http.get(Config.leadSourceUrl)
                  .then(function (response) {
                      return deferred.resolve(response);
                  }),
                  function (response) {
                      return response.Message;
                  };

                return deferred.promise;

            }
            this.getSalesStage = function () {

                var deferred = $q.defer();

                $http.get(Config.leadSalesUrl)
                  .then(function (response) {
                      return deferred.resolve(response);
                  }),
                  function (response) {
                      return response.Message;
                  };

                return deferred.promise;

            }
            this.getSalesman = function () {

                var deferred = $q.defer();

                $http.get(Config.SalesmanUrl)
                  .then(function (response) {
                      return deferred.resolve(response);
                  }),
                  function (response) {
                      return response.Message;
                  };

                return deferred.promise;

            }
            this.getReferredBy = function () {

                var deferred = $q.defer();

                $http.get(Config.ReferredByUrl)
                  .then(function (response) {
                      return deferred.resolve(response);
                  }),
                  function (response) {
                      return response.Message;
                  };

                return deferred.promise;

            }


            this.delete = function (id) {
              
               var deferred = $q.defer();

               var self = this;
               var config = { headers: { 'Content-Type': 'application/json; charset=UTF-8', 'accept': 'application/json' } }

               var deferred = $q.defer();

               var allLeads = this.getStoragePricebooks();

               $http.get(Config.addLeadsUrl)
                 .then(function (response) {
                     for (var i = 0; i < allLeads.length; i++) {
                         if ((allLeads[i] != null) && (allLeads[i].Id == id)) {
                             allLeads.splice(i, 1);

                             self.setStoragePricebooks(allLeads);
                             return deferred.resolve(allLeads);
                         }
                     }
                 });

                //$http.delete("/api/Pricebooks/" + id, config)
                // .then(function (response) {
                //     return deferred.resolve(response);
                // }),
                // function (response) {
                //     return response.Message;
                // };

                 return deferred.promise;

            }

           

            this.insert = function (instance) {
                var self = this;
                var jsonParse = JSON.parse(JSON.stringify(instance));
                var config = { headers: { 'Content-Type': 'application/json; charset=UTF-8', 'accept': 'application/json' } }
                var deferred = $q.defer();

                var allLeads = this.getStoragePricebooks();
                $http.get(Config.addLeadsUrl)
                  .then(function (response) {
                      var addLead = response;
                      addLead.data.Id = parseInt(allLeads[allLeads.length - 1].Id) + 1;
                      addLead.data.CompanyId = instance.CompanyId;
                      addLead.data.Company = instance.CompanyId;
                      addLead.data.CreationDate = instance.CreationDate;
                      addLead.data.LeadContactId = instance.LeadContactId;
                      addLead.data.LeadContact = instance.LeadContactId;
                      addLead.data.Subject = instance.Subject;
                      addLead.data.Description = instance.Description;
                      addLead.data.HotLead = instance.HotLead;
                      addLead.data.LeadSourceId = instance.LeadSourceId;
                      addLead.data.LeadSource = instance.LeadSourceId;
                      addLead.data.SalesStageId = instance.SalesStageId;
                      addLead.data.SalesStage = instance.SalesStageId;
                      addLead.data.SalesManId = instance.SalesManId;
                      addLead.data.Salesman = instance.SalesManId;
                      addLead.data.ReferredById = instance.ReferredById;
                      addLead.data.ReferredBy = instance.ReferredById;

                      allLeads.push(addLead.data);

                      self.setStoragePricebooks(allLeads);
                      return deferred.resolve(allLeads);
                  });

                

                //$http.post("/api/Pricebooks/", jsonParse, config)
                // .then(function (response) {
                //     return deferred.resolve(response);
                // }),
                // function (response) {
                //     alert(response.Message);
                // };

                return deferred.promise;

            }

            this.update = function (instance) {
                 
                var jsonParse = JSON.parse(JSON.stringify(instance));
                var self = this;
                var config = { headers: { 'Content-Type': 'application/json; charset=UTF-8', 'accept': 'application/json' } }

                var deferred = $q.defer();

                var allLeads = this.getStoragePricebooks();

                $http.get(Config.addLeadsUrl)
                  .then(function (response) {
                      for (var i = 0; i < allLeads.length; i++) {
                          if (allLeads[i].Id === instance.Id) {
                              allLeads[i].CompanyId = instance.CompanyId;
                              allLeads[i].Company = instance.CompanyId;
                              allLeads[i].CreationDate = instance.CreationDate;
                              allLeads[i].LeadContactId = instance.LeadContactId;
                              allLeads[i].LeadContact = instance.LeadContactId;
                              allLeads[i].Subject = instance.Subject;
                              allLeads[i].Description = instance.Description;
                              allLeads[i].HotLead = instance.HotLead;
                              allLeads[i].LeadSourceId = instance.LeadSourceId;
                              allLeads[i].LeadSource = instance.LeadSourceId;
                              allLeads[i].SalesStageId = instance.SalesStageId;
                              allLeads[i].SalesStage = instance.SalesStageId;
                              allLeads[i].SalesManId = instance.SalesManId;
                              allLeads[i].Salesman = instance.SalesManId;
                              allLeads[i].ReferredById = instance.ReferredById;
                              allLeads[i].ReferredBy = instance.ReferredById;

                             
                              allLeads[i].Activities = instance.Activities;
                              allLeads[i].Events = instance.Events;
                              allLeads[i].Notes = instance.Notes;
                              allLeads[i].Replies = instance.Replies;
                              self.setStoragePricebooks(allLeads);
                              return deferred.resolve(allLeads);

                          }
                      }
                  });


                //$http.put("/api/Pricebooks/", jsonParse, config)
                // .then(function (response) {
                //     return deferred.resolve(response);
                // }),
                // function (response)
                // {
                //     alert(response.Message);
                // };

                 return deferred.promise;

           }
           
        });