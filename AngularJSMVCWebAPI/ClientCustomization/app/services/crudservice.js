﻿module.service('CRUDService', function ($http, $q, $timeout) {

    this.getStorageProducts = function () {
        if (!localStorage.Products) {
            localStorage.Products = JSON.stringify([]);
        }

        return JSON.parse(localStorage.Products);
    }

    this.setStorageProducts = function (products) {
        localStorage.Products = JSON.stringify(products);
    }
    this.getStorageAccounts = function () {
        if (!localStorage.Accounts) {
            localStorage.Accounts = JSON.stringify([]);
        }

        return JSON.parse(localStorage.Accounts);
    }

    this.setStorageAccounts = function (accounts) {
        localStorage.Accounts = JSON.stringify(accounts);
    }

    this.getStorageLeads = function () {
        if (!localStorage.Leads) {
            localStorage.Leads = JSON.stringify([]);
        }

        return JSON.parse(localStorage.Leads);
    }

    this.setStorageLeads = function (leads) {
        localStorage.Leads = JSON.stringify(leads);
    }

    this.getStorageContacts = function () {
        if (!localStorage.Contacts) {
            localStorage.Contacts = JSON.stringify([]);
        }

        return JSON.parse(localStorage.Contacts);
    }

    this.setStorageContacts = function (contacts) {
        localStorage.Contacts = JSON.stringify(contacts);
    }
    this.getStoragePricebooks = function () {
        if (!localStorage.Pricebooks) {
            localStorage.Pricebooks = JSON.stringify([]);
        }

        return JSON.parse(localStorage.Pricebooks);
    }

    this.setStoragePricebooks = function (pricebooks) {
        localStorage.Pricebooks = JSON.stringify(pricebooks);
    }
    this.getStorageQuotations = function () {
        if (!localStorage.Quotations) {
            localStorage.Quotations = JSON.stringify([]);
        }

        return JSON.parse(localStorage.Quotations);
    }

    this.setStorageQuotations = function (leads) {
        localStorage.Quotations = JSON.stringify(leads);
    }

    this.getStorageForecasts = function () {
        if (!localStorage.Forecasts) {
            localStorage.Forecasts = JSON.stringify([]);
        }

        return JSON.parse(localStorage.Forecasts);
    }

    this.setStorageForecasts = function (leads) {
        localStorage.Forecasts = JSON.stringify(leads);
    }


    this.delete = function (id,type) {

        var deferred = $q.defer();

        var self = this;
        var config = { headers: { 'Content-Type': 'application/json; charset=UTF-8', 'accept': 'application/json' } }

        var deferred = $q.defer();
       
        switch (type) {
            case "Products":
                var allProducts = this.getStorageProducts();
                break;
            case "Accounts":
                var allProducts = this.getStorageAccounts();
                break;
            case "Leads":
                var allProducts = this.getStorageLeads();
                break;
            case "Contacts":
                var allProducts = this.getStorageContacts();
                break;
            case "Pricebooks":
                var allProducts = this.getStoragePricebooks();
                break;
            case "Quotations":
                var allProducts = this.getStorageQuotations();
                break;
            case "Forecasts":
                var allProducts = this.getStorageForecasts();
                break;
                
        }
        //console.log("allProducts", allProducts);
        for (var i = 0; i < allProducts.length; i++) {
            if ((allProducts[i] != null) && (allProducts[i].Id == id)) {
                //console.log("allAccounts.splice(i, 1)", allProducts.splice(i, 1));
                allProducts.splice(i, 1);
                switch (type) {
                    case "Products":
                        self.setStorageProducts(allProducts);
                        break;
                    case "Accounts":
                        self.setStorageAccounts(allProducts);
                        break;
                    case "Leads":
                        self.setStorageLeads(allProducts);
                        break;
                    case "Contacts":
                        self.setStorageContacts(allProducts);
                        break;
                    case "Pricebooks":
                        self.setStoragePricebooks(allProducts);
                        break;
                    case "Quotations":
                        self.setStorageQuotations(allProducts);
                        break;
                    case "Forecasts":
                        self.setStorageForecasts(allProducts);
                        break;
                }
            }
        }
        $timeout(function () {
            return deferred.resolve(allProducts);
        });
        return deferred.promise;
    }



    this.insert = function (instance, type) {
        var self = this;
        var jsonParse = JSON.parse(JSON.stringify(instance));
        var config = { headers: { 'Content-Type': 'application/json; charset=UTF-8', 'accept': 'application/json' } }
        var deferred = $q.defer();
        //console.log("instance", instance);
        //console.log("type", type);
        switch (type) {
            case "Products":
                var allProducts = this.getStorageProducts();
                break;
            case "Accounts":
                var allProducts = this.getStorageAccounts();
                break;
            case "Leads":
                var allProducts = this.getStorageLeads();
                break;
            case "Contacts":
                var allProducts = this.getStorageContacts();
                break;
            case "Pricebooks":
                var allProducts = this.getStoragePricebooks();
                break;
            case "Quotations":
                var allProducts = this.getStorageQuotations();
                break;
            case "Forecasts":
                var allProducts = this.getStorageForecasts();
                break;
        }

        allProducts.push(instance);
        //console.log("allProducts", allProducts);
        switch (type) {
            case "Products":
                this.setStorageProducts(allProducts);
                break;
            case "Accounts":
                this.setStorageAccounts(allProducts);
                break;
            case "Leads":
                this.setStorageLeads(allProducts);
                break;
            case "Contacts":
                this.setStorageContacts(allProducts);
                break;
            case "Pricebooks":
                this.setStoragePricebooks(allProducts);
                break;
            case "Quotations":
                this.setStorageQuotations(allProducts);
                break;
            case "Forecasts":
                this.setStorageForecasts(allProducts);
                break;
        }
        $timeout(function () {
            return deferred.resolve(allProducts);
        });

        return deferred.promise;

    }

    this.update = function (instance, type) {
        //console.log("instance", instance);
       // console.log("type", type);
        var jsonParse = JSON.parse(JSON.stringify(instance));
        var self = this;
        var config = { headers: { 'Content-Type': 'application/json; charset=UTF-8', 'accept': 'application/json' } }

        var deferred = $q.defer();
       
        switch (type) {
            case "Products":
                var allProducts = this.getStorageProducts();
                break;
            case "Accounts":
                var allProducts = this.getStorageAccounts();
                break;
            case "Leads":
                var allProducts = this.getStorageLeads();
                break;
            case "Contacts":
                var allProducts = this.getStorageContacts();
                break;
            case "Pricebooks":
                var allProducts = this.getStoragePricebooks();
                break;
            case "Quotations":
                var allProducts = this.getStorageQuotations();
                break;
            case "Forecasts":
                var allProducts = this.getStorageForecasts();
                break;
        }

        self.id = instance.Id;
        var particularlead = {};
        for (var i = 0; i < allProducts.length; i++) {
            if (allProducts[i].Id === self.id) {
                particularlead = instance;
                allProducts[i] = particularlead;
            }
        }
        //console.log("allProducts", allProducts);
       
        switch (type) {
            case "Products":
                this.setStorageProducts(allProducts);
                break;
            case "Accounts":
                this.setStorageAccounts(allProducts);
                break;
            case "Leads":
                this.setStorageLeads(allProducts);
                break;
            case "Contacts":
                this.setStorageContacts(allProducts);
                break;
            case "Pricebooks":
                this.setStoragePricebooks(allProducts);
                break;
            case "Quotations":
                this.setStorageQuotations(allProducts);
                break;
            case "Forecasts":
                this.setStorageForecasts(allProducts);
                break;
        }

        $timeout(function () {
            return deferred.resolve(allProducts);
        });
        return deferred.promise;
    }


    this.addNewActivity = function (newActivity_Text, Contact,type) {
        var newActivity = {};
        var deferred = $q.defer();
        if (!Contact.Activities || (Contact.Activities && Contact.Activities.length == 0)) {
            newActivity.ActivityID = 1;
            Contact.Activities = [];
        }else {
            newActivity.ActivityID = Contact.Activities[Contact.Activities.length - 1].ActivityID + 1;
        }
        newActivity.Name = 'Manish G';
        newActivity.Text = newActivity_Text;
        newActivity.ProfilePic = 'img/profileimg.png';
        newActivity.EditActivities = false;
        Contact.Activities.push(newActivity);
        switch (type) {
            case "Products":
                this.update(Contact, "Products").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Accounts":
                this.update(Contact, "Accounts").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Leads":
                this.update(Contact, "Leads").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Contacts":
                this.update(Contact, "Contacts").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Pricebooks":
                this.update(Contact, "Pricebooks").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Quotations":
                this.update(Contact, "Quotations").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Forecasts":
                this.update(Contact, "Forecasts").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
        }
       
        return deferred.promise;
        
    };

    this.addNewNotes = function (newNotes_Text, Contact,type) {
        var newNotes = {};
        var deferred = $q.defer();
        if (!Contact.Notes || (Contact.Notes && Contact.Notes.length == 0)) {
            newNotes.NotesID = 1;
            Contact.Notes = [];
        } else {
            newNotes.NotesID =Contact.Notes[Contact.Notes.length - 1].NotesID + 1;
        }
        newNotes.Name = 'Manish G';
        newNotes.Text = newNotes_Text;
        newNotes.ProfilePic = 'img/profileimg.png';
        newNotes.EditNotes = false;
        Contact.Notes.push(newNotes);

        switch (type) {
            case "Products":
                this.update(Contact, "Products").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Accounts":
                this.update(Contact, "Accounts").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Leads":
                this.update(Contact, "Leads").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Contacts":
                this.update(Contact, "Contacts").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Pricebooks":
                this.update(Contact, "Pricebooks").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Quotations":
                this.update(Contact, "Quotations").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Forecasts":
                this.update(Contact, "Forecasts").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
        }

        return deferred.promise;
    };

    this.addNewReply = function (reply, Contact,type) {
        var newReplyAdd = {};
        var deferred = $q.defer();
        if (!Contact.Replies || (Contact.Replies && Contact.Replies.length == 0)) {
            newReplyAdd.RepliesID = 1;
            Contact.Replies = [];
        }
        else {
            newReplyAdd.RepliesID = Contact.Replies[Contact.Replies.length - 1].RepliesID + 1;
        }
        newReplyAdd.Comment = reply.Text;
        newReplyAdd.ProfilePic = "img/profileimg.png";
        newReplyAdd.Recepients = reply.Recepients;
        newReplyAdd.CreationDate = "2016-08-16";
        newReplyAdd.ExcludeProspect = reply.excludeProspect;
        newReplyAdd.OtherReplies = [];
        Contact.Replies.push(newReplyAdd);

          switch (type) {
            case "Products":
                this.update(Contact, "Products").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Accounts":
                this.update(Contact, "Accounts").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Leads":
                this.update(Contact, "Leads").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Contacts":
                this.update(Contact, "Contacts").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
              case "Pricebooks":
                  this.update(Contact, "Pricebooks").then(function (response) {
                      $timeout(function () {
                          return deferred.resolve(response);
                      });
                  });
                  break;
              case "Quotations":
                  this.update(Contact, "Quotations").then(function (response) {
                      $timeout(function () {
                          return deferred.resolve(response);
                      });
                  });
                  break;
              case "Forecasts":
                  this.update(Contact, "Forecasts").then(function (response) {
                      $timeout(function () {
                          return deferred.resolve(response);
                      });
                  });
                  break;
        }

        return deferred.promise;
    };
    this.addsubreply = function (index, subreply, Contact, type) {
        var subReplyAdd = {};
        var deferred = $q.defer();
        if (Contact.Replies[index].OtherReplies.length == undefined || Contact.Replies[index].OtherReplies.length == 0) {
            subReplyAdd.OtherRepliesID = 1;
        }
        else {
            subReplyAdd.OtherRepliesID = Contact.Replies[index].OtherReplies[Contact.Replies[index].OtherReplies.length - 1].OtherRepliesID + 1;
        }
        subReplyAdd.Comment = subreply.Text
        subReplyAdd.ProfilePic = "img/profileimg.png";
        subReplyAdd.Recepients = subreply.Recepients;
        subReplyAdd.CreationDate = "2016-08-16";
        subReplyAdd.ExcludeProspect = false;
        Contact.Replies[index].OtherReplies.push(subReplyAdd);

        switch (type) {
            case "Products":
                this.update(Contact, "Products").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Accounts":
                this.update(Contact, "Accounts").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Leads":
                this.update(Contact, "Leads").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Contacts":
                this.update(Contact, "Contacts").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Pricebooks":
                this.update(Contact, "Pricebooks").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Quotations":
                this.update(Contact, "Quotations").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Forecasts":
                this.update(Contact, "Forecasts").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
        }

        return deferred.promise;

    };


    this.addNewEvents = function (newEvent, isEditEventForm, Contact, type) {
        var deferred = $q.defer();
        var newEvents = newEvent;
        if (isEditEventForm == true) {
            switch (type) {
                case "Products":
                    this.update(Contact, "Products").then(function (response) {
                        $timeout(function () {
                            return deferred.resolve(response);
                        });
                    });
                    break;
                case "Accounts":
                    this.update(Contact, "Accounts").then(function (response) {
                        $timeout(function () {
                            return deferred.resolve(response);
                        });
                    });
                    break;
                case "Leads":
                    this.update(Contact, "Leads").then(function (response) {
                        $timeout(function () {
                            return deferred.resolve(response);
                        });
                    });
                    break;
                case "Contacts":
                    this.update(Contact, "Contacts").then(function (response) {
                        $timeout(function () {
                            return deferred.resolve(response);
                        });
                    });
                    break;
                case "Pricebooks":
                    this.update(Contact, "Pricebooks").then(function (response) {
                        $timeout(function () {
                            return deferred.resolve(response);
                        });
                    });
                    break;
                case "Quotations":
                    this.update(Contact, "Quotations").then(function (response) {
                        $timeout(function () {
                            return deferred.resolve(response);
                        });
                    });
                    break;
                case "Forecasts":
                    this.update(Contact, "Forecasts").then(function (response) {
                        $timeout(function () {
                            return deferred.resolve(response);
                        });
                    });
                    break;
            }

        }
        else {
            if (!Contact.Events || (Contact.Events && Contact.Events.length == 0)) {
                newEvents.EventsID = 1;
                Contact.Events = [];
            }
            else {
                newEvents.EventsID = Contact.Events[Contact.Events.length - 1].EventsID + 1;
            }
            //console.log("newEvents", newEvents);
            newEvents.EditEvents = false;
            Contact.Events.push(newEvents);

            switch (type) {
                case "Products":
                    this.update(Contact, "Products").then(function (response) {
                        $timeout(function () {
                            return deferred.resolve(response);
                        });
                    });
                    break;
                case "Accounts":
                    this.update(Contact, "Accounts").then(function (response) {
                        $timeout(function () {
                            return deferred.resolve(response);
                        });
                    });
                    break;
                case "Leads":
                    this.update(Contact, "Leads").then(function (response) {
                        $timeout(function () {
                            return deferred.resolve(response);
                        });
                    });
                    break;
                case "Contacts":
                    this.update(Contact, "Contacts").then(function (response) {
                        $timeout(function () {
                            return deferred.resolve(response);
                        });
                    });
                    break;
                case "Pricebooks":
                    this.update(Contact, "Pricebooks").then(function (response) {
                        $timeout(function () {
                            return deferred.resolve(response);
                        });
                    });
                    break;
                case "Quotations":
                    this.update(Contact, "Quotations").then(function (response) {
                        $timeout(function () {
                            return deferred.resolve(response);
                        });
                    });
                    break;
                case "Forecasts":
                    this.update(Contact, "Forecasts").then(function (response) {
                        $timeout(function () {
                            return deferred.resolve(response);
                        });
                    });
                    break;
            }
        }
        return deferred.promise;
    };
    this.addUpload = function (Contact, type, file) {
        //console.log("Contact", Contact, "type", type, "file", file);
        //console.log("file[0]", file[0]);
        var newUpload = {};
        var deferred = $q.defer();
        if (!Contact.Uploads || (Contact.Uploads && Contact.Uploads.length == 0)) {
            newUpload.UploadsID = 1;
            Contact.Uploads = [];
        } else {
            newUpload.UploadsID = Contact.Uploads[Contact.Uploads.length - 1].UploadsID + 1;
        }
        newUpload.Name = 'Manish G';
        newUpload.ProfilePic = 'img/profileimg.png';
        Contact.Uploads.push(newUpload);
        switch (type) {
            case "Products":
                this.update(Contact, "Products").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Accounts":
                this.update(Contact, "Accounts").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Leads":
                this.update(Contact, "Leads").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Contacts":
                this.update(Contact, "Contacts").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Pricebooks":
                this.update(Contact, "Pricebooks").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Quotations":
                this.update(Contact, "Quotations").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Forecasts":
                this.update(Contact, "Forecasts").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
        }
        
        return deferred.promise;

    };
    this.searchvalue = function (Contact, type, model) {
        //console.log("Contact", Contact, "model", model);
        //console.log("type", type);
        var deferred = $q.defer();
        switch (type) {
            case "Products":
                this.update(Contact, "Products").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Accounts":
                this.update(Contact, "Accounts").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Leads":
                this.update(Contact, "Leads").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Contacts":
                this.update(Contact, "Contacts").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Pricebooks":
                this.update(Contact, "Pricebooks").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Quotations":
                this.update(Contact, "Quotations").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
            case "Forecasts":
                this.update(Contact, "Forecasts").then(function (response) {
                    $timeout(function () {
                        return deferred.resolve(response);
                    });
                });
                break;
        }

        return deferred.promise;

    };
});