﻿// script.js

// create the module and name it  
// also include ngRoute for all our routing needs
// var scotchApp = angular.module('scotchApp', ['ngRoute']);

// configure our routes
module.config(function ($routeProvider) {
    $routeProvider
        // route for the home page
        .when('/login', {
            templateUrl: '../login/login.html',
            controller: 'LoginController'
        })
        .when('/home', {
            templateUrl: '../home/index.html',
            controller: ''
        })
        .when('/config', {
            templateUrl: '../config/index.html',
            controller: ''
        })
        .when('/dashboard', {
            templateUrl: '../dashboard/index.html',
            controller: 'dashboardController'
        })
        // route for the about page
        .when('/leads', {
            templateUrl: '../leads/lead_container.html',
            controller: 'LeadsController',
			controllerAs:'vm'
        })
        .when('/accounts', {
            templateUrl: '../accounts/account_container.html',
            controller:'AccountsController',
			controllerAs:'vm'
        })
        .when('/calendar', {
            templateUrl: '../calendar/index.html',
            controller: 'CalendarController',
            controllerAs:'vm'
        })
        .when('/campaigns', {
            templateUrl: '../campaigns/campaign_feed.html',
            controller: 'vm'
        })
        .when('/contacts', {
            templateUrl: '../contacts/contact_container.html',
            controller: 'ContactsController',
            controllerAs:'vm'
        })
        .when('/insights', {
            templateUrl: '../insights/index.html',
            controller: ''
        })
        .when('/reports', {
            templateUrl: '../reports/index.html',
            controller: ''
        })
        .when('/opportunities', {
            templateUrl: '../opportunities/index.html',
            controller: ''
        })
        .when('/quotations', {
            templateUrl: '../quotations/quotation_container.html',
            controller: 'QuotationsController',
			controllerAs:'vm'
        })
        .when('/forecasting', {
            templateUrl: '../forecastings/forecasting_container.html',
            controller: 'forecastingController',
            controllerAs:'vm'
        })
        .when('/products', {
            templateUrl: '../products/product_container.html',
            controller: 'ProductsController',
            controllerAs:'vm'
        })
        .when('/pricebooks', {
            templateUrl: '../pricebooks/pricebook_container.html',
            controller: 'PricebookController',
            controllerAs:'vm'
        })
        .otherwise
        ({
            redirectTo: '/home'
        });

});

 