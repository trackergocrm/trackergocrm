﻿
var module = angular.module('app', ['ngRoute', 'angularMoment', 'angular-advanced-searchbox', 'ngFileUpload', 'ngCookies', 'nvd3', '720kb.datepicker']);

module.constant('serviceURL', 'http://localhost:56418/');

module.run(function ($rootScope, $location, $cookieStore, $http, $templateCache) {
    // keep user logged in after page refresh
    $rootScope.globals = $cookieStore.get('globals') || {};
    if ($rootScope.globals.currentUser) {
        $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
    }

    $rootScope.$on('$locationChangeStart', function (event, next, current) {
        // redirect to login page if not logged in and trying to access a restricted page
        var restrictedPage = (['/home', '/result'].indexOf($location.path()));
        var loggedIn = $rootScope.globals.currentUser;
        if (restrictedPage > -1 && !loggedIn) {
            $location.path('/login');
        }
    });
});
