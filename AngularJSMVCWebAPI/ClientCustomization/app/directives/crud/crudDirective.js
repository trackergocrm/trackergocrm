﻿module.directive('crudControl', [function () {

    var controller = function () {

        var vm = this;

        vm.showadd = false;
        vm.showedit = true;
        vm.showsave = false;
        vm.showdelete = true;
        vm.showcancel = false;
        vm.showback = false;

        vm.addl = function (event) {

            alert("addl");

            console.log($scope.subActiveTab);

            buttonvisible(false, false, true, false, true, false);

            event.preventDefault();

        }

        vm.editl = function (event) {

            alert("editl");

            buttonvisible(false, false, true, false, true, false);

            event.preventDefault();

        }

        vm.savel = function (event) {

            alert("savel");

            buttonvisible(false, true, false, true, false, false);

            event.preventDefault();

        }

        vm.deletel = function (event) {
            buttonvisible(false, true, false, true, false, false);
            event.preventDefault();

        }

        vm.cancell = function (event) {

            alert("cancell");

            buttonvisible(false, true, false, true, false, false);

            event.preventDefault();

        }

        vm.backl = function (event) {

            alert("backl");

            event.preventDefault();

        }

        buttonvisible = function (showadd, showedit, showsave, showdelete, showcancel, showback) {

            vm.showadd = showadd;
            vm.showedit = showedit;
            vm.showsave = showsave;
            vm.showdelete = showdelete;
            vm.showcancel = showcancel;
            vm.showback = showback;

        }


    };

    return {
        scope: {
            add: "&",
            edit: "&",
            save: "&",
            delete: "&",
            cancel: "&",
            back: "&"
        },

        controller: controller,
        templateUrl: '/ClientCustomization/usercontrols/crud/crud.htm',
        controllerAs: 'vm'
    };

}]);