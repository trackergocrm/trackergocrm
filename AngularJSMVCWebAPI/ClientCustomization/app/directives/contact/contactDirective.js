﻿module.directive('contactControl', [function () {
    return {
        scope: {
            label: '@', // optional
            changeCallback: '&',
            email: "&",
            call: "&",
            options: '='
        },
        restrict: 'E',
        replace: true, // optional 
        templateUrl: '/ClientCustomization/app/directives/contact/contact.htm',
        link: function (scope, element, attr) { }
    };
}]);