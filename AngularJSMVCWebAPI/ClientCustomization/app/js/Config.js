﻿    'use strict';
    var Config = (function () {
        function Config() {
        }
        Config.serverUrl = 'http://localhost:56418/ClientCustomization/app/';
        Config.loginUrl = Config.serverUrl + 'mockdata/login.json';
        Config.leadsUrl = Config.serverUrl + 'mockdata/leads.json';
        Config.contactsUrl = Config.serverUrl + 'mockdata/Contacts.json';
        Config.productsUrl = Config.serverUrl + 'mockdata/products.json';
        Config.quotationsUrl = Config.serverUrl + 'mockdata/quotations.json';
        Config.pricebooksUrl = Config.serverUrl + 'mockdata/pricebooks.json';
        Config.forecastingsUrl = Config.serverUrl + 'mockdata/forecastings.json';
        Config.addLeadsUrl = Config.serverUrl + 'mockdata/addLead.json';
        Config.activityTypeUrl = Config.serverUrl + 'mockdata/activities.json';
        Config.accountsUrl = Config.serverUrl + 'mockdata/accounts.json';
		Config.accountNameUrl = Config.serverUrl + 'mockdata/accountName.json';
        Config.leadContactUrl = Config.serverUrl + 'mockdata/leadContact.json';
        Config.leadSourceUrl = Config.serverUrl + 'mockdata/leadSource.json';
        Config.leadSalesUrl = Config.serverUrl + 'mockdata/salesStage.json';
        Config.SalesmanUrl = Config.serverUrl + 'mockdata/salesman.json';
        Config.ReferredByUrl = Config.serverUrl + 'mockdata/referredBy.json';
        Config.calenderUrl = Config.serverUrl + 'mockdata/calender.json';
        return Config;
    })();


