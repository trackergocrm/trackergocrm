﻿module.controller('ProductsController', function ($scope, $rootScope, Upload, $timeout, ProductsService, FlashService, CRUDService) {

    var vm = this;
    $scope.showiconadd = true;
    $scope.showiconsearch = true;
    $scope.activeiconadd = false;
    $scope.activeiconsearch = false;
    $scope.activeTab = 'productsFeed';
    $scope.subActiveTab = 'details';
    vm.Product = null;
    vm.LastProduct;
    vm.showAddForm = false;
    vm.isEditForm = false;
    vm.currentleadid = -1;
    vm.event_detail = true;
    vm.message = null;
    vm.showadd = true;
    vm.showedit = true;
    vm.showsave = false;
    vm.showdelete = true;
    vm.showcancel = false;
    vm.showback = false;
    vm.addNewEvent = false;
    vm.newActivity_Text = "";
    vm.newNotes_Text = "";
	vm.searchVisible = false;
    $rootScope.dashboardTitle = "Products Feed";
    $rootScope.dashboardMsg = "Overview of all the Products";
    vm.newEvent = {
        isRecurrence: false
    };
    $scope.availableSearchParams = [
        { key: "Subject", name: "Subject", placeholder: "Subject..." },
        { key: "Contact", name: "Contact", placeholder: "Contact..." },
        { key: "Phone", name: "Phone", placeholder: "Phone..." },
        { key: "emailAddress", name: "E-Mail", placeholder: "E-Mail..." },
        { key: "Date", name: "Date", placeholder: "Date...", type: "date" }
       //{ key: "SelectBox", name: "SelectBox", placeholder: "SelectBox...", type: "select", suggestedValues: ['test1', 'test2', 'test3'] }
    ];

    $scope.eventsSearchParams = [
        { key: "Search Param 1", name: "Search Param 1", placeholder: "Search Param 1..." },
        { key: "Search Param 2", name: "Search Param 2", placeholder: "Search Param 2..." }
    ];

    $scope.notesSearchParams = [
        { key: "Search Param 1", name: "Search Param 1", placeholder: "Search Param 1..." },
        { key: "Search Param 2", name: "Search Param 2", placeholder: "Search Param 2..." }
    ];
	
	$scope.startSearch = function ()
    {

        $scope.searchVisible = true;
    }

    $scope.hideSearch = function () {

        $scope.searchVisible = false;
    }

    vm.buttonvisible = function (showadd, showedit, showsave, showdelete, showcancel, showback) {

        vm.showadd = showadd;
        vm.showedit = showedit;
        vm.showsave = showsave;
        vm.showdelete = showdelete;
        vm.showcancel = showcancel;
        vm.showback = showback;
    };

    $scope.changeWithOutSubTabs = function (iconName) {

        $scope.activeiconadd = false;
        $scope.activeiconsearch = false;

        if (iconName === 'add') {
            $scope.activeiconadd = true;
        }

        if (iconName === 'search') {
            $scope.activeiconsearch = true;
        }

    };

    $scope.changeSubTabs = function (tabName) {
        $scope.subActiveTab = tabName;
        if (tabName === 'details') {
            $rootScope.dashboardTitle = "Products Details";
            //vm.buttonvisible(true, true, false, true, false, false);
            vm.buttonvisible(false, true, false, true, false, false);
        }
        else if (tabName === 'events') {
            vm.isEditForm = true;
            vm.buttonvisible(true, false, false, false, false, false);
        }
        else {
            vm.isEditForm = true;
            vm.buttonvisible(false, false, false, false, false, false);

        }

        if (tabName === 'posts') {
            $rootScope.dashboardTitle = "Activities Details";
        }
        if (tabName === 'events') {
            vm.showCalendarReadOnly = false;
            vm.showCalendarEdit = false;
            vm.event_detail = true;
            $rootScope.dashboardTitle = "Events Details";
        }
        if (tabName === 'notes') {
            $rootScope.dashboardTitle = "Notes Details";
        }
        if (tabName === 'uploads') {
            $rootScope.dashboardTitle = "Uploads Details";
        }
        if (tabName === 'replies') {
            $rootScope.dashboardTitle = "Replies Details";
        }
    };

    vm.getall = function () {
        ProductsService.getall()
        .then(function (response) {
            vm.Products = response;
            // console.log(JSON.stringify(response));
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getall();

    vm.getActivities = function () {
        ProductsService.getActivityType()
        .then(function (response) {
            vm.ActivityType = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getActivities();

    vm.getAcountName = function () {
        ProductsService.getAcountName()
        .then(function (response) {
            vm.AcountName = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getAcountName();

    vm.getLeadContact = function () {
        ProductsService.getLeadContact()
        .then(function (response) {
            vm.LeadContact = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getLeadContact();

    vm.getLeadSource = function () {
        ProductsService.getLeadSource()
        .then(function (response) {
            vm.LeadSource = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getLeadSource();

    vm.getSalesStage = function () {
        ProductsService.getSalesStage()
        .then(function (response) {
            vm.SalesStage = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getSalesStage();

    vm.getSalesman = function () {
        ProductsService.getSalesman()
        .then(function (response) {
            vm.Salesman = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getSalesman();

    vm.getReferredBy = function () {
        ProductsService.getReferredBy()
        .then(function (response) {
            vm.ReferredBy = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getReferredBy();



    $scope.$watch('files', function () {
        // $scope.upload($scope.files);
    });
    $scope.$watch('file', function () {
        if ($scope.file != null) {
            $scope.files = [$scope.file];
        }
    });
    vm.deleteActivities = function (id) {
        for (var i = 0; i < vm.Product.Activities.length; i++) {
            if ((vm.Product.Activities != null) && (vm.Product.Activities[i].ActivityID == id)) {
                vm.Product.Activities.splice(i, 1);
                vm.save();
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
    };
    vm.deleteEvents = function (id) {
        for (var i = 0; i < vm.Product.Events.length; i++) {
            if ((vm.Product.Events != null) && (vm.Product.Events[i].EventsID == id)) {
                vm.Product.Events.splice(i, 1);
                vm.save();
                break;
            }
        }
        vm.showCalendarReadOnly = false;
        vm.showCalendarEdit = false;
        vm.event_detail = true;
        FlashService.Success("Deleted Successfully");
    };
    vm.deleteNotes = function (id) {
        for (var i = 0; i < vm.Product.Notes.length; i++) {
            if ((vm.Product.Notes != null) && (vm.Product.Notes[i].NotesID == id)) {
                vm.Product.Notes.splice(i, 1);
                vm.save();
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
    };
    vm.deleteUploads = function (id) {
        for (var i = 0; i < vm.Product.Uploads.length; i++) {
            if ((vm.Product.Uploads != null) && (vm.Product.Uploads[i].UploadsID == id)) {
                vm.Product.Uploads.splice(i, 1);
                vm.save();
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
    };
    vm.editEvents = function (id) {
        vm.addNewEvent = !vm.addNewEvent;
        for (var i = 0; i < vm.Product.Events.length; i++) {
            if ((vm.Product.Events != null) && (vm.Product.Events[i].EventsID == id)) {
                vm.newEvent = vm.Product.Events[i];
                break;
            }
        }
    };
    // upload on file select or drop
    vm.upload = function (file) {
        CRUDService.addUpload(vm.Product, "Products", file).then(function (response) {
            //console.log("response", response);
            vm.Products = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.files = [];
            //console.log("vm.files", vm.files);
            FlashService.Success("Upload Saved Successfully!");
        });
        /*Upload.upload({
            url: 'upload/url',
            data: { file: file, 'username': $scope.username }
        }).then(function (resp) {
            // console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
        }, function (resp) {
            // console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            // console.log('progress: ' + progressPercentage + '% ');
       });*/
    };


    vm.email = function () {
        alert('email called');
    };
    vm.call = function () {
        alert('Call function called');
    };



    vm.cancel = function () {
        vm.showAddForm = false;
        if (vm.currentleadid) {
            vm.filterproducts(vm.currentleadid);
            vm.buttonvisible(true, true, false, true, false, false);
        } else {
            $scope.activeTab = "productsFeed";
        }
        
    }

    vm.addNewActivity = function () {
       /* var newActivity = {};
        newActivity.ActivityID = vm.Product.Activities[vm.Product.Activities.length - 1].ActivityID + 1;
        newActivity.Name = 'Manish G';
        newActivity.Text = vm.newActivity_Text;
        newActivity.ProfilePic = 'img/profileimg.png';
        newActivity.EditActivities = false;
        vm.Product.Activities.push(newActivity);

        ProductsService.update(vm.Product).then(function (response) {
            vm.Products = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newActivity_Text = "";
            FlashService.Success("Activity Saved Successfully!");
        });*/
        //console.log("vm.newActivity_Text", vm.newActivity_Text);
        //console.log("vm.Product", vm.Product);
        CRUDService.addNewActivity(vm.newActivity_Text, vm.Product, "Products").then(function (response) {
            //console.log("response", response);
            vm.Products = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newActivity_Text = "";
            FlashService.Success("Activity Saved Successfully!");
        });
    };
    vm.addNewReply = function () {
        /*var newReplyAdd = {};
        newReplyAdd.RepliesID = vm.Product.Replies[vm.Product.Replies.length - 1].RepliesID + 1;
        newReplyAdd.Comment = vm.reply.Text
        newReplyAdd.ProfilePic = "img/profileimg.png";
        newReplyAdd.Recepients = vm.reply.Recepients;
        newReplyAdd.CreationDate = "2016-08-16";
        newReplyAdd.ExcludeProspect = vm.reply.excludeProspect;
        newReplyAdd.OtherReplies = [];
        vm.Product.Replies.push(newReplyAdd);

        ProductsService.update(vm.Product).then(function (response) {
            vm.Products = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.reply = {};
            FlashService.Success("Replies Saved Successfully!");
        });*/
        CRUDService.addNewReply(vm.reply, vm.Product, "Products").then(function (response) {
            //console.log("response", response);
            vm.Products = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.reply = {};
            FlashService.Success("Replies Saved Successfully!");
        });
    };
    vm.addsubreply = function (index) {
       /* var subReplyAdd = {};
        if (vm.Product.Replies[index].OtherReplies.length == undefined || vm.Product.Replies[index].OtherReplies.length == 0) {
            subReplyAdd.OtherRepliesID = 1;
        }
        else {
            subReplyAdd.OtherRepliesID = vm.Product.Replies[index].OtherReplies[vm.Product.Replies[index].OtherReplies.length - 1].OtherRepliesID + 1;
        }
        subReplyAdd.Comment = vm.subreply.Text
        subReplyAdd.ProfilePic = "img/profileimg.png";
        subReplyAdd.Recepients = vm.subreply.Recepients;
        subReplyAdd.CreationDate = "2016-08-16";
        subReplyAdd.ExcludeProspect = false;
        vm.Product.Replies[index].OtherReplies.push(subReplyAdd);


        ProductsService.update(vm.Product).then(function (response) {
            vm.Products = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.subreply = {};
            vm.subreply.Recepients = '';
            FlashService.Success("Replies Saved Successfully!");
        });*/
        CRUDService.addsubreply(index, vm.subreply, vm.Product, "Products").then(function (response) {
            //console.log("response", response);
            vm.Products = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.subreply = {};
            vm.subreply.Recepients = '';
            FlashService.Success("Replies Saved Successfully!");
        });
    };
    vm.addNewEvents = function () {
        /*var newEvents = vm.newEvent;
        if (vm.newEvent.EventsID == undefined) {
            newEvents.EventsID = vm.Product.Events[vm.Product.Events.length - 1].EventsID + 1;
        }

        newEvents.EditActivities = false;
        vm.Product.Events.push(newEvents);

        ProductsService.update(vm.Product).then(function (response) {
            vm.Products = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newEvent = {};
            FlashService.Success("Events Saved Successfully!");
        });*/
        CRUDService.addNewEvents(vm.newEvent, vm.isEditEventForm, vm.Product, "Products").then(function (response) {
            //console.log("response", response);
            vm.Products = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newEvent = {};
            FlashService.Success("Events Saved Successfully!");
        });
        /*vm.showCalendarEdit = false;
        vm.event_detail = true;
        vm.isEditEventForm == false;
        vm.buttonvisible(true, false, false, false, false, false);*/
        if (vm.isEditEventForm == true) {
            vm.showCalendarReadOnly = true;
            vm.event_detail = false;
            vm.isEditEventForm = false;
            vm.buttonvisible(true, false, true, true, false, false);
        } else {
            vm.event_detail = true;
            vm.buttonvisible(true, false, false, false, false, false);
        }

        vm.showCalendarEdit = false;
        //vm.buttonvisible(true, false, true, true, false, false);

    };
    vm.addNewNotes = function () {
       /* var newNotes = {};
        newNotes.NotesID = vm.Product.Notes[vm.Product.Notes.length - 1].NotesID + 1;
        newNotes.Name = 'Manish G';
        newNotes.Text = vm.newNotes_Text;
        newNotes.ProfilePic = 'img/profileimg.png';
        newNotes.EditActivities = false;
        vm.Product.Notes.push(newNotes);

        ProductsService.update(vm.Product).then(function (response) {
            vm.Products = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newNotes_Text = "";
            FlashService.Success("Notes Saved Successfully!");
        });*/
        CRUDService.addNewNotes(vm.newNotes_Text, vm.Product, "Products").then(function (response) {
            //console.log("response", response);
            vm.Products = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newNotes_Text = "";
            FlashService.Success("Notes Saved Successfully!");
        });
    };
    vm.save = function () {
       /* vm.showAddForm = false;

        if (vm.isEditForm == true) {
            ProductsService.update(vm.Product).then(function (response) {
                vm.Products = response;
                if ($scope.subActiveTab == 'details') {
                    vm.buttonvisible(false, true, false, true, false, false);
                }
                FlashService.Success("Saved Successfully!");
            });
        }
        else {
            ProductsService.insert(vm.Product).then(function (response) {
                vm.Products = response;
                if ($scope.subActiveTab == 'details') {
                    vm.buttonvisible(false, true, false, true, false, false);
                    $scope.activeTab = 'productsFeed';
                }
                FlashService.Success("Saved Successfully!");
            });
        }*/
        vm.showAddForm = false;
        vm.Product.TaxableId = vm.Product.Tax;
        vm.Product.PriceBookId = vm.Product.PriceBook;
        if (vm.isEditForm == true) {
            CRUDService.update(vm.Product, "Products").then(function (response) {
                vm.Products = response;
                if ($scope.subActiveTab == 'details') {
                    //vm.buttonvisible(false, true, false, true, false, false);
                    vm.buttonvisible(true, true, false, true, false, false);
                }
                FlashService.Success("Saved Successfully!");
            });
        }
        else {
            vm.Product.Id = vm.Products.length + 1;
            vm.Product.ManufacturerId = "";
            vm.Product.UsageUnitId = "";
            vm.Product.CategoryId = "";
            vm.Product.Active = "";
            vm.Product.Replies = [];
            vm.Product.Uploads = [];
            vm.Product.Notes = [];
            vm.Product.Events = [];
            vm.Product.Activities = [];
            CRUDService.insert(vm.Product, "Products").then(function (response) {
                vm.Products = response;
                if ($scope.subActiveTab == 'details') {
                    //vm.buttonvisible(false, true, false, true, false, false);
                    vm.buttonvisible(true, true, false, true, false, false);
                    $scope.activeTab = 'productsFeed';
                }
                vm.Products = [];
                vm.Products = CRUDService.getStorageProducts();
                //console.log("vm.Products", vm.Products);
                FlashService.Success("Saved Successfully!");
            });
        }
    }

    vm.add = function () {
        vm.LastProduct = vm.Product;
        vm.Product = {};
        vm.showAddForm = true;
        //vm.buttonvisible(false, true, true, false, true, false);
        vm.buttonvisible(false, false, true, false, true, false);
    }
    vm.addevent = function () {
        //console.log(" vm.Account", vm.Account);
        vm.newEvent = {
            isRecurrence: false
        };
        vm.showCalendarEdit = true;
        vm.event_detail = false;
        vm.buttonvisible(false, false, true, true, false, false);
    }
    vm.delete = function () {
        if (confirm("Delete this Product?")) {

            ProductsService.delete(vm.currentleadid).then(function (response) {
                vm.Products = response;
                if ($scope.subActiveTab == 'details') {
                    vm.buttonvisible(false, true, false, true, false, false);
                }
            });
            FlashService.Success("Deleted Successfully!");
        }
        //$scope.activeTab = 'productsFeed';
        $scope.activeTab = 'productsDetail';
        vm.buttonvisible(false, true, false, true, false, false);
    };

    vm.edit = function () {
        vm.showAddForm = true;
        vm.isEditForm = true;
        vm.filterproducts(vm.currentleadid);
        vm.buttonvisible(false, false, true, false, true, false);
    }
    vm.editevent = function () {
        vm.showCalendarReadOnly = false;
        vm.showCalendarEdit = true;
        vm.isEditEventForm = true;
        vm.filterevents(vm.currenteventid);
       // console.log("vm.newEvent", vm.newEvent);
        vm.buttonvisible(false, false, true, true, false, false);
    }
    vm.filterproducts = function (id) {
        vm.Product = vm.Products.filter(function (v) {
            return v.Id === id; // Filter out the appropriate one
        })[0];
        if (vm.Product && vm.Product.hasOwnProperty('CreationDate') && vm.Product.CreationDate != "") {
            vm.Product.CreationDate = new Date(vm.Product.CreationDate);
        }
        if (vm.Product && vm.Product.hasOwnProperty('SalesStartDate') && vm.Product.SalesStartDate != "") {
            vm.Product.SalesStartDate = new Date(vm.Product.SalesStartDate);
        }
        if (vm.Product && vm.Product.hasOwnProperty('SalesEndDate') && vm.Product.SalesEndDate != "") {
            vm.Product.SalesEndDate = new Date(vm.Product.SalesEndDate);
        }
        if (vm.Product && vm.Product.hasOwnProperty('Phone') && vm.Product.Phone != "") {
            vm.Product.Phone = parseInt(vm.Product.Phone);
        }
        if (vm.Product && vm.Product.hasOwnProperty('QtyOrdered') && vm.Product.QtyOrdered != "") {
            vm.Product.QtyOrdered = parseInt(vm.Product.QtyOrdered);
        }
        if (vm.Product && vm.Product.hasOwnProperty('QtyInstock') && vm.Product.QtyInstock != "") {
            vm.Product.QtyInstock = parseInt(vm.Product.QtyInstock);
        }
        if (vm.Product && vm.Product.hasOwnProperty('ReorderLevel') && vm.Product.ReorderLevel != "") {
            vm.Product.ReorderLevel = parseInt(vm.Product.ReorderLevel);
        }
        if (vm.Product && vm.Product.hasOwnProperty('QtyInDemand') && vm.Product.QtyInDemand != "") {
            vm.Product.QtyInDemand = parseInt(vm.Product.QtyInDemand);
        }
        
        
    }
    vm.filterevents = function (id) {
        //console.log("vm.Product", vm.Product);
        //console.log("vm.Product.Events", vm.Product.Events);
        vm.Calendar = vm.Product.Events.filter(function (v) {
            return v.EventsID === id; // Filter out the appropriate one
        })[0];
        vm.newEvent = vm.Calendar;
    }
    vm.showproductdetail = function (id, event) {
        //console.log("getting")
        $scope.activeTab = 'productsDetail';
        $scope.subActiveTab = 'details'
        vm.filterproducts(id);
        vm.currentleadid = id;
        vm.showAddForm = false;

        //vm.buttonvisible(true, true, false, true, false, false);
        vm.buttonvisible(false, true, false, true, false, false);
    }
    vm.showeventdetail = function (id, event) {
        vm.filterevents(id);
        vm.currenteventid = id;
        //console.log(" vm.filterevents(id)", vm.filterevents(id));
        //console.log(" vm.currenteventid", vm.currenteventid);
        vm.showCalendarReadOnly = true;
        vm.event_detail = false;
        vm.buttonvisible(true, true, false, true, false, false);
    }
    vm.close = function () {
        vm.showCalendarReadOnly = true;
        vm.showCalendarEdit = false;
        vm.event_detail = false;
        vm.buttonvisible(true, true, false, true, false, false);
    }
    vm.addNewProduct = function (id, event) {
        $scope.activeTab = 'productsDetail';
        $scope.subActiveTab = 'details'
        vm.filterproducts(id);
        vm.currentleadid = id;
        vm.LastProduct = vm.Product;
        vm.Product = {};
        vm.showAddForm = true;
        //vm.buttonvisible(false, true, true, false, true, false);
        vm.buttonvisible(false, false, true, false, true, false);
    }

    $scope.searchObject = {};
    $scope.$on('advanced-searchbox:addedSearchParam', function (event, searchParameter) {
        ///
    });

    $scope.$on('advanced-searchbox:removedSearchParam', function (event, searchParameter) {
        /// Need to call ajax if tags is removed
        //console.log(searchParameter.key, " is removed", searchParameter);
    });

    $scope.$on('advanced-searchbox:removedAllSearchParam', function (event) {
        ///
    });

    $scope.$on('advanced-searchbox:enteredEditMode', function (event, searchParameter) {
        ///
    });

    $scope.$on('advanced-searchbox:leavedEditMode', function (event, searchParameter) {
        ///
    });

    $scope.$on('advanced-searchbox:modelUpdated', function (event, model) {
        ///
    });

    $scope.$on('advanced-searchbox:searchButtonClicked', function (event, model) {
        ///Get results with filters after user clicking submit button
        $scope.searchObject = model;
        model.type = "Products";
        model.object = vm.Products;
        //console.log("model", model);
        CRUDService.searchvalue(vm.Products, model.type, model).then(function (response) {
            //console.log("response", response);
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
        });

    });

});