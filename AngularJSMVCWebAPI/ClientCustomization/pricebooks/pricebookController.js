﻿module.controller('PricebookController', function ($scope, $rootScope, Upload, $timeout, PricebooksService, FlashService, CRUDService) {

    var vm = this;
    $scope.showiconadd = true;
    $scope.showiconsearch = true;
    $scope.activeiconadd = false;
    $scope.activeiconsearch = false;
    $scope.activeTab = 'pricebooksFeed';
    $scope.subActiveTab = 'details';
    vm.Pricebook = null;
    vm.LastPricebook;
    vm.showAddForm = false;
    vm.isEditForm = false;
    vm.currentpricebookid = -1;
    vm.event_detail = true;
    vm.message = null;
    vm.deleteItem = false;
    vm.showadd = true;
    vm.showedit = true;
    vm.showsave = false;
    vm.showdelete = true;
    vm.showcancel = false;
    vm.showback = false;
    vm.addNewEvent = false;
    vm.showitemsave = false;
    vm.showitemcancel = false;
    
    vm.newActivity_Text = "";
    vm.newNotes_Text = "";
	vm.searchVisible = false;
    $rootScope.dashboardTitle = "Pricebooks Feed";
    $rootScope.dashboardMsg = "Overview of all the Pricebooks";
    vm.newEvent = {
        isRecurrence: false
    };
   
    $scope.availableSearchParams = [
        { key: "Subject", name: "Subject", placeholder: "Subject..." },
        { key: "Contact", name: "Contact", placeholder: "Contact..." },
        { key: "Phone", name: "Phone", placeholder: "Phone..." },
        { key: "emailAddress", name: "E-Mail", placeholder: "E-Mail..." },
        { key: "Date", name: "Date", placeholder: "Date...", type: "date" }
       //{ key: "SelectBox", name: "SelectBox", placeholder: "SelectBox...", type: "select", suggestedValues: ['test1', 'test2', 'test3'] }
    ];

    $scope.eventsSearchParams = [
        { key: "Search Param 1", name: "Search Param 1", placeholder: "Search Param 1..." },
        { key: "Search Param 2", name: "Search Param 2", placeholder: "Search Param 2..." }
    ];

    $scope.notesSearchParams = [
        { key: "Search Param 1", name: "Search Param 1", placeholder: "Search Param 1..." },
        { key: "Search Param 2", name: "Search Param 2", placeholder: "Search Param 2..." }
    ];

	$scope.startSearch = function ()
    {

        $scope.searchVisible = true;
    }

    $scope.hideSearch = function () {

        $scope.searchVisible = false;
    }
	
	
    vm.buttonvisible = function (showadd, showedit, showsave, showdelete, showcancel, showback) {

        vm.showadd = showadd;
        vm.showedit = showedit;
        vm.showsave = showsave;
        vm.showdelete = showdelete;
        vm.showcancel = showcancel;
        vm.showback = showback;
    };

    $scope.changeWithOutSubTabs = function (iconName) {

        $scope.activeiconadd = false;
        $scope.activeiconsearch = false;

        if (iconName === 'add') {
            $scope.activeiconadd = true;
        }

        if (iconName === 'search') {
            $scope.activeiconsearch = true;
        }

    };

    $scope.changeSubTabs = function (tabName) {
        $scope.subActiveTab = tabName;
        if (tabName === 'details') {
            $rootScope.dashboardTitle = "Pricebooks Details";
            //vm.buttonvisible(true, true, false, true, false, false);
            vm.buttonvisible(false, true, false, true, false, false);
        }
        else if (tabName === 'events') {
            vm.isEditForm = true;
            vm.buttonvisible(true, false, false, false, false, false);
        }
        else {
            vm.isEditForm = true;
            vm.buttonvisible(false, false, false, false, false, false);
           
        }

        if (tabName === 'posts') {
            $rootScope.dashboardTitle = "Activities Details";
        }
        if (tabName === 'events') {
            vm.showCalendarReadOnly = false;
            vm.showCalendarEdit = false;
            vm.event_detail = true;
            $rootScope.dashboardTitle = "Events Details";
        }
        if (tabName === 'notes') {
            $rootScope.dashboardTitle = "Notes Details";
        }
        if (tabName === 'uploads') {
            $rootScope.dashboardTitle = "Uploads Details";
        }
        if (tabName === 'replies') {
            $rootScope.dashboardTitle = "Replies Details";
        }
    };

    vm.getall = function () {
        PricebooksService.getall()
        .then(function (response) {
            vm.Pricebooks = response;
            // console.log(JSON.stringify(response));
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getall();

    vm.getActivities = function () {
        PricebooksService.getActivityType()
        .then(function (response) {
            vm.ActivityType = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getActivities();

    vm.getAcountName = function () {
        PricebooksService.getAcountName()
        .then(function (response) {
            vm.AcountName = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getAcountName();

    vm.getLeadContact = function () {
        PricebooksService.getLeadContact()
        .then(function (response) {
            vm.LeadContact = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getLeadContact();

    vm.getLeadSource = function () {
        PricebooksService.getLeadSource()
        .then(function (response) {
            vm.LeadSource = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getLeadSource();

    vm.getSalesStage = function () {
        PricebooksService.getSalesStage()
        .then(function (response) {
            vm.SalesStage = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getSalesStage();

    vm.getSalesman = function () {
        PricebooksService.getSalesman()
        .then(function (response) {
            vm.Salesman = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getSalesman();

    vm.getReferredBy = function () {
        PricebooksService.getReferredBy()
        .then(function (response) {
            vm.ReferredBy = response.data;
        }),
        function (response) {
            FlashService.Error(response.message);
        };
    };
    vm.getReferredBy();



    $scope.$watch('files', function () {
        // $scope.upload($scope.files);
    });
    $scope.$watch('file', function () {
        if ($scope.file != null) {
            $scope.files = [$scope.file];
        }
    });
    vm.deleteItems = function (id) {

        for (var i = 0; i < vm.Pricebook.Items.length; i++) {
            if ((vm.Pricebook.Items != null) && (vm.Pricebook.Items[i].PricebookItemId == id)) {
                vm.Pricebook.Items.splice(i, 1);
                CRUDService.update(vm.Pricebook, "Pricebooks").then(function (response) {
                    vm.Pricebooks = response;
                    if ($scope.subActiveTab == 'details') {
                        vm.buttonvisible(true, true, false, true, false, false);
                    }
                    FlashService.Success("Saved Successfully!");
                });
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
        $scope.activeTab = 'pricebooksDetail';
        vm.buttonvisible(false, true, false, true, false, false);
    };
    vm.deleteActivities = function (id) {
        for (var i = 0; i < vm.Pricebook.Activities.length; i++) {
            if ((vm.Pricebook.Activities != null) && (vm.Pricebook.Activities[i].ActivityID == id)) {
                vm.Pricebook.Activities.splice(i, 1);
                vm.save();
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
    };
    vm.deleteEvents = function (id) {
        for (var i = 0; i < vm.Pricebook.Events.length; i++) {
            if ((vm.Pricebook.Events != null) && (vm.Pricebook.Events[i].EventsID == id)) {
                vm.Pricebook.Events.splice(i, 1);
                vm.save();
                break;
            }
        }
        vm.showCalendarReadOnly = false;
        vm.showCalendarEdit = false;
        vm.event_detail = true;
        FlashService.Success("Deleted Successfully");
    };
    vm.deleteNotes = function (id) {
        for (var i = 0; i < vm.Pricebook.Notes.length; i++) {
            if ((vm.Pricebook.Notes != null) && (vm.Pricebook.Notes[i].NotesID == id)) {
                vm.Pricebook.Notes.splice(i, 1);
                vm.save();
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
    };
    vm.deleteUploads = function (id) {
        for (var i = 0; i < vm.Pricebook.Uploads.length; i++) {
            if ((vm.Pricebook.Uploads != null) && (vm.Pricebook.Uploads[i].UploadsID == id)) {
                vm.Pricebook.Uploads.splice(i, 1);
                vm.save();
                break;
            }
        }
        FlashService.Success("Deleted Successfully");
    };
    vm.editEvents = function (id) {
        vm.addNewEvent = !vm.addNewEvent;
        for (var i = 0; i < vm.Pricebook.Events.length; i++) {
            if ((vm.Pricebook.Events != null) && (vm.Pricebook.Events[i].EventsID == id)) {
                vm.newEvent = vm.Pricebook.Events[i];
                break;
            }
        }
    };
    // upload on file select or drop
    vm.upload = function (file) {
        CRUDService.addUpload(vm.Pricebook, "Pricebooks", file).then(function (response) {
            //console.log("response", response);
            vm.Pricebooks = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.files = [];
            //console.log("vm.files", vm.files);
            FlashService.Success("Upload Saved Successfully!");
        });
        /* Upload.upload({
            url: 'upload/url',
            data: { file: file, 'username': $scope.username }
        }).then(function (resp) {
            // console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
        }, function (resp) {
            // console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            // console.log('progress: ' + progressPercentage + '% ');
       });*/
    };


    vm.email = function () {
        alert('email called');
    };
    vm.call = function () {
        alert('Call function called');
    };



    vm.cancel = function () {
        vm.showAddForm = false;
        if (vm.currentpricebookid) {
            vm.filterpricebooks(vm.currentpricebookid);
            vm.buttonvisible(true, true, false, true, false, false);
        } else {
            $scope.activeTab = 'pricebooksFeed';
        }
        
    }
    vm.cancelItem = function (index) {
        vm.Pricebook.Items[index].readvalue = true;
        vm.showcancel = true;
        //vm.buttonvisible(true, true, false, true, false, false);
    }
    vm.undocancelItem = function (index) {
        vm.Pricebook.Items[index].readvalue = false;
        //vm.buttonvisible(true, true, false, true, false, false);
    }
    vm.addNewActivity = function () {
        /*var newActivity = {};
        newActivity.ActivityID = vm.Pricebook.Activities[vm.Pricebook.Activities.length - 1].ActivityID + 1;
        newActivity.Name = 'Manish G';
        newActivity.Text = vm.newActivity_Text;
        newActivity.ProfilePic = 'img/profileimg.png';
        newActivity.EditActivities = false;
        vm.Pricebook.Activities.push(newActivity);

        PricebooksService.update(vm.Pricebook).then(function (response) {
            vm.Pricebooks = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newActivity_Text = "";
            FlashService.Success("Activity Saved Successfully!");
        });*/
        //console.log("vm.newActivity_Text", vm.newActivity_Text);
        //console.log("vm.Pricebook", vm.Pricebook);
        CRUDService.addNewActivity(vm.newActivity_Text, vm.Pricebook, "Pricebooks").then(function (response) {
            //console.log("response", response);
            vm.Pricebooks = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newActivity_Text = "";
            FlashService.Success("Activity Saved Successfully!");
        });
    };
    vm.addNewReply = function () {
       /* var newReplyAdd = {};
        newReplyAdd.RepliesID = vm.Pricebook.Replies[vm.Pricebook.Replies.length - 1].RepliesID + 1;
        newReplyAdd.Comment = vm.reply.Text
        newReplyAdd.ProfilePic = "img/profileimg.png";
        newReplyAdd.Recepients = vm.reply.Recepients;
        newReplyAdd.CreationDate = "2016-08-16";
        newReplyAdd.ExcludeProspect = vm.reply.excludeProspect;
        newReplyAdd.OtherReplies = [];
        vm.Pricebook.Replies.push(newReplyAdd);

        PricebooksService.update(vm.Pricebook).then(function (response) {
            vm.Pricebooks = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.reply = {};
            FlashService.Success("Replies Saved Successfully!");
        });*/
        CRUDService.addNewReply(vm.reply, vm.Pricebook, "Pricebooks").then(function (response) {
            //console.log("response", response);
            vm.Pricebooks = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.reply = {};
            FlashService.Success("Replies Saved Successfully!");
        });
    };
    vm.addsubreply = function (index) {
        /*var subReplyAdd = {};
        if (vm.Pricebook.Replies[index].OtherReplies.length == undefined || vm.Pricebook.Replies[index].OtherReplies.length == 0) {
            subReplyAdd.OtherRepliesID = 1;
        }
        else {
            subReplyAdd.OtherRepliesID = vm.Pricebook.Replies[index].OtherReplies[vm.Pricebook.Replies[index].OtherReplies.length - 1].OtherRepliesID + 1;
        }
        subReplyAdd.Comment = vm.subreply.Text
        subReplyAdd.ProfilePic = "img/profileimg.png";
        subReplyAdd.Recepients = vm.subreply.Recepients;
        subReplyAdd.CreationDate = "2016-08-16";
        subReplyAdd.ExcludeProspect = false;
        vm.Pricebook.Replies[index].OtherReplies.push(subReplyAdd);
        

        PricebooksService.update(vm.Pricebook).then(function (response) {
            vm.Pricebooks = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.subreply = {};
            vm.subreply.Recepients = '';
            FlashService.Success("Replies Saved Successfully!");
        });*/
        CRUDService.addsubreply(index, vm.subreply, vm.Pricebook, "Pricebooks").then(function (response) {
            //console.log("response", response);
            vm.Pricebooks = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.subreply = {};
            vm.subreply.Recepients = '';
            FlashService.Success("Replies Saved Successfully!");
        });
    };
    vm.addNewEvents = function () {
        /*var newEvents = vm.newEvent;
        if (vm.newEvent.EventsID == undefined) {
            newEvents.EventsID = vm.Pricebook.Events[vm.Pricebook.Events.length - 1].EventsID + 1;
        }

        newEvents.EditActivities = false;
        vm.Pricebook.Events.push(newEvents);

        PricebooksService.update(vm.Pricebook).then(function (response) {
            vm.Pricebooks = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newEvent = {};
            FlashService.Success("Events Saved Successfully!");
        });*/
        CRUDService.addNewEvents(vm.newEvent, vm.isEditEventForm, vm.Pricebook, "Pricebooks").then(function (response) {
            //console.log("response", response);
            vm.Pricebooks = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newEvent = {};
            FlashService.Success("Events Saved Successfully!");
        });
        /*vm.showCalendarEdit = false;
        vm.event_detail = true;
        vm.isEditEventForm == false;
        vm.buttonvisible(true, false, false, false, false, false);*/
        if (vm.isEditEventForm == true) {
            vm.showCalendarReadOnly = true;
            vm.event_detail = false;
            vm.isEditEventForm = false;
            vm.buttonvisible(true, false, true, true, false, false);
        } else {
            vm.event_detail = true;
            vm.buttonvisible(true, false, false, false, false, false);
        }

        vm.showCalendarEdit = false;
        vm.buttonvisible(true, false, true, true, false, false);

    };
    vm.addNewNotes = function () {
       /*var newNotes = {};
        newNotes.NotesID = vm.Pricebook.Notes[vm.Pricebook.Notes.length - 1].NotesID + 1;
        newNotes.Name = 'Manish G';
        newNotes.Text = vm.newNotes_Text;
        newNotes.ProfilePic = 'img/profileimg.png';
        newNotes.EditActivities = false;
        vm.Pricebook.Notes.push(newNotes);

        PricebooksService.update(vm.Pricebook).then(function (response) {
            vm.Pricebooks = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newNotes_Text = "";
            FlashService.Success("Notes Saved Successfully!");
        });*/
        CRUDService.addNewNotes(vm.newNotes_Text, vm.Pricebook, "Pricebooks").then(function (response) {
            //console.log("response", response);
            vm.Pricebooks = response;
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
            vm.newNotes_Text = "";
            FlashService.Success("Notes Saved Successfully!");
        });
    };
    vm.save = function () {
       /* vm.showAddForm = false;
        
        if (vm.isEditForm == true) {
            PricebooksService.update(vm.Pricebook).then(function (response) {
                vm.Pricebooks = response;
                if ($scope.subActiveTab == 'details') {
                    vm.buttonvisible(false, true, false, true, false, false);
                }
                FlashService.Success("Saved Successfully!");
            });
        }
        else {
            PricebooksService.insert(vm.Pricebook).then(function (response) {
                vm.Pricebooks = response;
                if ($scope.subActiveTab == 'details') {
                    vm.buttonvisible(false, true, false, true, false, false);
                    $scope.activeTab = 'leadsFeed';
                }
                FlashService.Success("Saved Successfully!");
            });
        }*/
        vm.showAddForm = false;
        if (vm.isEditForm == true) {
            CRUDService.update(vm.Pricebook, "Pricebooks").then(function (response) {
                vm.Pricebooks = response;
                if ($scope.subActiveTab == 'details') {
                    //vm.buttonvisible(false, true, false, true, false, false);
                    vm.buttonvisible(true, true, false, true, false, false);
                }
                FlashService.Success("Saved Successfully!");
            });
        }
        else {
            vm.Pricebook.Id = vm.Pricebooks.length + 1;
            vm.Pricebook.PriceBookName = "";
            vm.Pricebook.Replies = [];
            vm.Pricebook.Uploads = [];
            vm.Pricebook.Notes = [];
            vm.Pricebook.Events = [];
            vm.Pricebook.Activities = [];
            //console.log("vm.Pricebook", vm.Pricebook);
            CRUDService.insert(vm.Pricebook, "Pricebooks").then(function (response) {
                vm.Pricebooks = response;
                //console.log("vm.Pricebooks", vm.Pricebooks);
                if ($scope.subActiveTab == 'details') {
                    //vm.buttonvisible(false, true, false, true, false, false);
                    vm.buttonvisible(true, true, false, true, false, false);
                    $scope.activeTab = 'pricebooksFeed';
                }
                vm.Pricebooks = [];
                vm.Pricebooks = CRUDService.getStoragePricebooks();
                //console.log("vm.Pricebooks", vm.Pricebooks);
                FlashService.Success("Saved Successfully!");
            });
        }
        
    }

    vm.add = function () {
       
            vm.LastPricebook = vm.Pricebook;
            vm.Pricebook = {};
            vm.showAddForm = true;
        //vm.buttonvisible(false, true, true, false, true, false);
            vm.buttonvisible(false, false, true, false, false, true);
    }
    vm.addevent = function () {
        //console.log(" vm.Account", vm.Account);
        vm.newEvent = {
            isRecurrence: false
        };
        vm.showCalendarEdit = true;
        vm.event_detail = false;
        vm.buttonvisible(false, false, true, true, false, false);
    }
    vm.delete = function () {
        if (confirm("Delete this pricebooks?")) {

            PricebooksService.delete(vm.currentpricebookid).then(function (response) {
                vm.Pricebooks = response;
                if ($scope.subActiveTab == 'details') {
                    vm.buttonvisible(false, true, false, true, false, false);
                }
            });
            FlashService.Success("Deleted Successfully!");
        }
        //$scope.activeTab = 'pricebooksFeed';
        $scope.activeTab = 'pricebooksDetail';
        vm.buttonvisible(false, true, false, true, false, false);
    };

    vm.edit = function () {
        
        vm.showAddForm = true;
        vm.isEditForm = true;
        vm.filterpricebooks(vm.currentpricebookid);
        vm.showitemsave = true;
        vm.showitemcancel = true;
        vm.firstvalue = 1;
        //console.log("vm.showitemcancel", vm.showitemcancel, "vm.showitemsave", vm.showitemsave);
        vm.buttonvisible(false, false, true, false, true, false);
    }
    vm.editevent = function () {
        vm.showCalendarReadOnly = false;
        vm.showCalendarEdit = true;
        vm.isEditEventForm = true;
        vm.filterevents(vm.currenteventid);
        //console.log("vm.newEvent", vm.newEvent);
        vm.buttonvisible(false, false, true, true, false, false);
    }
    vm.filterpricebooks = function (id) {
        vm.Pricebook = vm.Pricebooks.filter(function (v) {
            return v.Id === id; // Filter out the appropriate one
        })[0];
        if (vm.Pricebook && vm.Pricebook.hasOwnProperty('CreationDate') && vm.Pricebook.CreationDate != "") {
            vm.Pricebook.CreationDate = new Date(vm.Pricebook.CreationDate);
        }
        if (vm.Pricebook && vm.Pricebook.hasOwnProperty('Phone') && vm.Pricebook.Phone != "") {
            vm.Pricebook.Phone = parseInt(vm.Pricebook.Phone);
        }
        if (vm.Pricebook && vm.Pricebook.hasOwnProperty('PricebookItemId') && vm.Pricebook.PricebookItemId != "") {
            vm.Pricebook.PricebookItemId = parseInt(vm.Pricebook.PricebookItemId);
        }
        if (vm.Pricebook && vm.Pricebook.hasOwnProperty('From') && vm.Pricebook.From != "") {
            vm.Pricebook.From = parseInt(vm.Pricebook.From);
        }
        if (vm.Pricebook && vm.Pricebook.hasOwnProperty('To') && vm.Pricebook.To != "") {
            vm.Pricebook.To = parseInt(vm.Pricebook.To);
        }
        //vm.Pricebook.Items.readvalue = false;
        if (vm.Pricebook && vm.Pricebook.hasOwnProperty('Items') && vm.Pricebook.Items != "") {
            //console.log("vm.Pricebook.Items.length", vm.Pricebook.Items.length);
            for (i = 0; i < vm.Pricebook.Items.length; i++) {
                vm.Pricebook.Items[i].readvalue = false;
            }
        }
       
    }
    vm.filterevents = function (id) {
        //console.log("vm.Pricebook", vm.Pricebook);
        //console.log("vm.Pricebook.Events", vm.Pricebook.Events);
        vm.Calendar = vm.Pricebook.Events.filter(function (v) {
            return v.EventsID === id; // Filter out the appropriate one
        })[0];
        vm.newEvent = vm.Calendar;
    }
    vm.showpricebookdetail = function (id, event) {
        $scope.activeTab = 'pricebooksDetail';
        $scope.subActiveTab = 'details'
        vm.filterpricebooks(id);
        vm.currentpricebookid = id;
        vm.showAddForm = false;

        //vm.buttonvisible(true, true, false, true, false, false);
        vm.buttonvisible(false, true, false, true, false, false);
    }
    vm.showeventdetail = function (id, event) {
        vm.filterevents(id);
        vm.currenteventid = id;
        //console.log(" vm.filterevents(id)", vm.filterevents(id));
        //console.log(" vm.currenteventid", vm.currenteventid);
        vm.showCalendarReadOnly = true;
        vm.event_detail = false;
        vm.buttonvisible(true, true, false, true, false, false);
    }
    vm.close = function () {
        vm.showCalendarReadOnly = true;
        vm.showCalendarEdit = false;
        vm.event_detail = false;
        vm.buttonvisible(true, true, false, true, false, false);
    }
    vm.addNewPricebook = function (id, event) {
        $scope.activeTab = 'pricebooksDetail';
        $scope.subActiveTab = 'details'
        vm.filterpricebooks(id);
        vm.currentpricebookid = id;
        vm.LastPricebook = vm.Pricebook;
        vm.Pricebook = {};
        vm.showAddForm = true;
        vm.buttonvisible(false, false, true, false, true, false);
        //vm.buttonvisible(false, false, true, false, false, true);
    }

    $scope.searchObject = {};
    $scope.$on('advanced-searchbox:addedSearchParam', function (event, searchParameter) {
        ///
    });

    $scope.$on('advanced-searchbox:removedSearchParam', function (event, searchParameter) {
        /// Need to call ajax if tags is removed
        //console.log(searchParameter.key, " is removed", searchParameter);
    });

    $scope.$on('advanced-searchbox:removedAllSearchParam', function (event) {
        ///
    });

    $scope.$on('advanced-searchbox:enteredEditMode', function (event, searchParameter) {
        ///
    });

    $scope.$on('advanced-searchbox:leavedEditMode', function (event, searchParameter) {
        ///
    });

    $scope.$on('advanced-searchbox:modelUpdated', function (event, model) {
        ///
    });

    $scope.$on('advanced-searchbox:searchButtonClicked', function (event, model) {
        ///Get results with filters after user clicking submit button
        $scope.searchObject = model;
        model.type = "Pricebooks";
        model.object = vm.Pricebooks;
        //console.log("model", model);
        CRUDService.searchvalue(vm.Pricebooks, model.type, model).then(function (response) {
            //console.log("response", response);
            if ($scope.subActiveTab == 'details') {
                vm.buttonvisible(false, true, false, true, false, false);
            }
        });

    });

});