﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AngularJSMVCWebAPI.Models;
using System.Reflection;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Xml;
using System.Web;
using System.IO;
using System.Data;
using AngularJSMVCWebAPI.ExtensionMethods;
using System.Xml.Linq;
using System.Web.Http.Cors;

namespace AngularJSMVCWebAPI.Controllers
{
    [EnableCors(origins: "http://localhost:53218/", headers: "*", methods: "*")]
    public class LeadsController : ApiController
    {

        [HttpGet]
        public List<Lead> Get()
        {
            var lead= new Lead();
            var leads = new List<Lead>();

            try
            {
                string xmlfile = HttpContext.Current.Server.MapPath("/App_Data/leads.xml");

                using (DataSet ds = new DataSet())
                {
                    ds.ReadXml(xmlfile);
                    DataTable dt = ds.Tables[0];

                    dynamic query = from c in dt.AsEnumerable() select c; //where c["id"].Equals(id.ToString()) select c;

                    foreach (DataRow q in query)
                    {
                        lead = new Lead();

                        lead.Id = Convert.ToInt32(q["Id"]);
                        lead.Description = (string)q["Description"];
                        lead.Title = (string)q["Title"];
                        lead.Salesman = (string)q["Salesman"];
                        lead.CreateDate = (string)q["CreateDate"];
                        //Uploads
                        lead.Account = (string)q["Account"];
                        lead.From = (string)q["From"];
                        lead.To = (string)q["To"];
                        lead.AccountId = Convert.ToInt32(q["AccountId"]);
                        lead.FromId = Convert.ToInt32(q["FromId"]);
                        lead.Read = Convert.ToBoolean(q["Read"]);
                        lead.Subject = (string)q["Subject"];
                        lead.Status = (string)q["Status"];
                        lead.Saleman = (string)q["Saleman"];
                        lead.ContactPhone = (string)q["ContactPhone"];
                        lead.PONum = (string)q["PONum"];
                        lead.Amount = Convert.ToDecimal(q["Amount"]);
                        lead.SalesStageId = Convert.ToInt32(q["SalesStageId"]);
                        lead.SalesStage = (string)q["SalesStage"];
                        lead.Topics = (string)q["Topics"];
                        lead.UnRead = Convert.ToBoolean(q["UnRead"]);
                        lead.LeadSourceId = Convert.ToInt32(q["LeadSourceId"]);
                        lead.LeadSource = (string)q["LeadSource"];
                        lead.SalesManId = Convert.ToInt32(q["SalesManId"]);
                        lead.InvoiceNum = (string)q["InvoiceNum"];
                        lead.InvoiceDate = (string)q["InvoiceDate"];
                        lead.LostDate = (string)q["LostDate"];
                        lead.PotentialRevenue = Convert.ToDecimal(q["PotentialRevenue"]);
                        lead.ReferredById = Convert.ToInt32(q["ReferredById"]);
                        lead.Units = Convert.ToInt32(q["Units"]);
                        lead.UnitType = Convert.ToInt32(q["UnitType"]);
                        lead.TemplateId = Convert.ToInt32(q["TemplateId"]);
                        lead.HotLead = Convert.ToBoolean(q["HotLead"]);
                        lead.InActive = Convert.ToBoolean(q["InActive"]);

                        leads.Add(lead);

                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            //string json = "" + JsonConvert.SerializeObject(leads);

            //return json;
            return leads;
        }

        // POST api/login
        [HttpPost]
        public string Post([FromBody]dynamic lead)
        {
            try
            {
 
                var s = new System.Web.Script.Serialization.JavaScriptSerializer();
                Lead leadD = s.Deserialize<Lead>(Convert.ToString(lead));
               
                string xmlfile = HttpContext.Current.Server.MapPath("/App_Data/leads.xml");
                 
                XDocument xdoc = XDocument.Load(xmlfile);
              
                leadD.Id = getNewId();
                    
                XElement el = new XElement("lead");

                Type type = typeof(Lead);
                PropertyInfo[] properties = type.GetProperties();

                foreach (PropertyInfo property in properties)
                {
                     
                    string propertyName = property.Name;

                    string propertyType = property.PropertyType.ToString();

                    XAttribute at = new XAttribute(property.Name, ""); 

                     switch (propertyType)
                     {
                         case "System.String":
                         {
                             if (leadD.GetType().GetProperty(propertyName).GetValue(leadD, null) == null)
                             {
                                 at = new XAttribute(property.Name, ""); 
                             }else
                             {
                                 at = new XAttribute(property.Name, leadD.GetType().GetProperty(propertyName).GetValue(leadD, null));
                             }
                             break;
                         }
                         case "System.Boolean":
                         {
                             if (leadD.GetType().GetProperty(propertyName).GetValue(leadD, null) == null)
                             {
                                at = new XAttribute(property.Name, "false");
                             }
                             else
                             {
                                 at = new XAttribute(property.Name, leadD.GetType().GetProperty(propertyName).GetValue(leadD, null));
                             }
                             break;
                         }
                         case "System.Decimal":
                         {
                             if (leadD.GetType().GetProperty(propertyName).GetValue(leadD, null) == null)
                             {
                                 at = new XAttribute(property.Name, "0.00");
                             }
                             else
                             {
                                 at = new XAttribute(property.Name, leadD.GetType().GetProperty(propertyName).GetValue(leadD, null));
                             }
                             break;
                         }
                         case "System.Int32":
                         {
                             if (leadD.GetType().GetProperty(propertyName).GetValue(leadD, null) == null)
                             {
                                 at = new XAttribute(property.Name, "0");
                             }
                             else
                             {
                                 at = new XAttribute(property.Name, leadD.GetType().GetProperty(propertyName).GetValue(leadD, null));
                             }
                             break;
                         }

                     }
                    //var tempString = "";
                    // var tempInt = 0;
                    // var tempBoolean = false;
                    // var tempDecimal = 0.00;

                    // if (leadD.GetType().GetProperty(propertyName).GetValue(leadD, null) == null)
                    // {
                    //      tempString = "";
                    //     var tempInt = 0;
                    //     var tempBoolean = false;
                    //     var tempDecimal = 0.00;
                    // }
                    // else
                    // {
                    //     temp = (string)leadD.GetType().GetProperty(property.Name).GetValue(leadD, null);
                    // }
                    
                    //XAttribute at = new XAttribute(property.Name, temp); 

                     el.Add(at);
                }

                //xdoc.Element("contacts").Add(
                //           new XElement("contact",
                //               new XAttribute("id", Convert.ToString(contactD.id)),
                //               new XAttribute("name", Convert.ToString(contactD.name)),
                //               new XAttribute("email", Convert.ToString(contactD.email)),
                //               new XAttribute("phone", Convert.ToString(contactD.phone))
                //               ));

                xdoc.Element("leads").Add(el);
                xdoc.Save(xmlfile);

                return "Success! Record has been updated.";
            }
            catch (Exception ex)
            {
                return ex.Message;

            }

        }

        // PUT api/login/5
        [HttpPut]
        public string Put([FromBody]dynamic lead)
        {
            try
            {

                var s = new System.Web.Script.Serialization.JavaScriptSerializer();
                Lead leadD = s.Deserialize<Lead>(Convert.ToString(lead));

                string xmlfile = HttpContext.Current.Server.MapPath("/App_Data/leads.xml");

                XDocument xdoc = XDocument.Load(xmlfile);
  
                XElement ld = xdoc.Root.Elements("lead").Where(r => (int)r.Attribute("Id") == leadD.Id).FirstOrDefault();
                ld.Attribute("Description").Value = leadD.Description;
                ld.Attribute("Title").Value = leadD.Title;

                xdoc.Save(xmlfile);

                return "Success! Record has been updated.";
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }

        // DELETE api/login/5
        [HttpDelete]
        public string Delete(int id)
        {

            try
            {

                var s = new System.Web.Script.Serialization.JavaScriptSerializer(); 
                string xmlfile = HttpContext.Current.Server.MapPath("/App_Data/leads.xml");

                XDocument xdoc = XDocument.Load(xmlfile);

                xdoc.Root.Elements("lead").Where(r => (int)r.Attribute("Id") == id).FirstOrDefault().Remove();
                 
                xdoc.Save(xmlfile);

                return "Success! Record has been deleted.";

            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }

        private int getNewId()
        {
            var contact = new Contact();
            var contacts = new List<Contact>();

            try
            {
                string xmlfile = HttpContext.Current.Server.MapPath("/App_Data/leads.xml");
                int max = -1;
                using (DataSet ds = new DataSet())
                {
                    ds.ReadXml(xmlfile);
                    DataTable dt = ds.Tables[0];
                    max = dt.AsEnumerable().Max(row => Convert.ToInt32(row["Id"]));

                }

                return  Convert.ToInt32(max) + 1;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }


}
