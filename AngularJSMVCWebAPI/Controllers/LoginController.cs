﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AngularJSMVCWebAPI.Models;

namespace AngularJSMVCWebAPI.Controllers
{
    public class LoginController : ApiController
    {
        // GET api/login
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET api/login/5
        [HttpGet]
        public string Get(dynamic login)
        {
            try
            {
                var s = new System.Web.Script.Serialization.JavaScriptSerializer();
                Login loginD = s.Deserialize<Login>(Convert.ToString(login));

                return "Success!";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        // POST api/login
        [HttpPost]
        public string Post([FromBody]dynamic login)
        {
            try
            {
                var s = new System.Web.Script.Serialization.JavaScriptSerializer();
                Login loginD = s.Deserialize<Login>(Convert.ToString(login));

                return "Success!";
            }
           catch(Exception ex)
            {
                return ex.Message;
            }

        }

        // PUT api/login/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/login/5
        public void Delete(int id)
        {
        }
    }
}
