﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AngularJSMVCWebAPI.Models;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Xml;
using System.Web;
using System.IO;
using System.Data;
using AngularJSMVCWebAPI.ExtensionMethods;
using System.Xml.Linq;


namespace AngularJSMVCWebAPI.Controllers
{
    public class ContactsController : ApiController
    {
        // GET api/contacts
        [HttpGet]
        public String Get()
        {
            var contact = new Contact();
            var contacts = new List<Contact>();

            try
            {
                string xmlfile = HttpContext.Current.Server.MapPath("/App_Data/contacts.xml");
                
                using (DataSet ds = new DataSet()) 
                {
	                    ds.ReadXml(xmlfile);
	                    DataTable dt = ds.Tables[0];
	         
	                    dynamic query = from c in dt.AsEnumerable() select c;
                       
	                    foreach (DataRow q in query) 
                        {
                              contact = new Contact();

                              contact.id = (string)q["id"];  
                              contact.name = (string)q["name"];
                              contact.email = (string)q["email"];
                              contact.phone = (string)q["phone"];

                              contacts.Add(contact);

	                 }
                }
            }

            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }

            string json = "" + JsonConvert.SerializeObject(contacts);

            return json;
        }

        // GET api/contacts/5
        [HttpGet]
        public string Get(int id)
        {
            {
                var contact = new Contact();
                var contacts = new List<Contact>();

                try
                {
                    string xmlfile = HttpContext.Current.Server.MapPath("/App_Data/contacts.xml");

                    using (DataSet ds = new DataSet())
                    {
                        ds.ReadXml(xmlfile);
                        DataTable dt = ds.Tables[0];

                        dynamic query = from c in dt.AsEnumerable() where c["id"].Equals(id.ToString()) select c;

                        foreach (DataRow q in query)
                        {
                            contact = new Contact();

                            contact.id = (string)q["id"];
                            contact.name = (string)q["name"];
                            contact.email = (string)q["email"];
                            contact.phone = (string)q["phone"];

                            contacts.Add(contact);

                        }
                    }
                }

                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                string json = "" + JsonConvert.SerializeObject(contacts);

                return json;
            }
        }


        // POST api/contacts
        [HttpPost]
        public String Post([FromBody] dynamic contact)
        {
                      
            try
            {
                
                //List<Contact> contacts = new List<Contact>
                //   {
                //        new Contact{id = "1", name = "Viral", phone = "123-1231232", email="viral@mail.xxx"}               
                //   };
                //string contact1 = contacts.FirstOrDefault().ToJSON();
 
                var s = new System.Web.Script.Serialization.JavaScriptSerializer();                
                Contact contactD = s.Deserialize<Contact>(Convert.ToString(contact));
                contactD.id = getNewId();
                
                string xmlfile = HttpContext.Current.Server.MapPath("/App_Data/contacts.xml");

                XDocument xdoc = XDocument.Load(xmlfile);
                xdoc.Element("contacts").Add(
                    new XElement("contact",
                        new XAttribute("id", Convert.ToString(contactD.id)),
                        new XAttribute("name", Convert.ToString(contactD.name)),
                        new XAttribute("email", Convert.ToString(contactD.email)),
                        new XAttribute("phone", Convert.ToString(contactD.phone))
                        ));
                xdoc.Save(xmlfile);

                return "Success! Record has been added.";
            }
            catch (Exception ex)
            {
                return ex.Message;
   
            }
            

        }

        // PUT api/contacts/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/contacts/5
        [HttpDelete]
        public string Delete(int id)
        {

            try
            {

                string xmlfile = HttpContext.Current.Server.MapPath("/App_Data/contacts.xml");

                XDocument xdoc = XDocument.Load(xmlfile);
                var query = from q in xdoc.Descendants("contact") select q;
                
                foreach (XElement xe in query)
                {
                    if (xe.Attribute("id").Value.Equals(id.ToString()))
                    {
                        xe.Remove();
                        break;
                    } 
                    
                }
                
                xdoc.Save(xmlfile);

                return "Success! Record has been deleted.";
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
            
        }

        private string getNewId()
        {
            var contact = new Contact();
            var contacts = new List<Contact>();

            try
            { 
                string xmlfile = HttpContext.Current.Server.MapPath("/App_Data/contacts.xml");
                int max = -1;
                 using (DataSet ds = new DataSet()) 
                 {
	                    ds.ReadXml(xmlfile);
	                    DataTable dt = ds.Tables[0];
                        max = dt.AsEnumerable().Max(row => Convert.ToInt32(row["id"]));
 
                }

                 return Convert.ToString(Convert.ToInt64(max) + 1);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
